package cn.lzh.cqhg1.controller;
import cn.lzh.cqhg1.Bean.*;
import cn.lzh.cqhg1.dao.*;
import cn.lzh.cqhg1.utils.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Controller
public class DaochuController extends BaseController{
    @Autowired
    LuruDao luruDao;
    @Autowired
    DClogDao dClogDao;
    @Autowired
    TestJsonDao testJsonDao;
    @Autowired
    RulesDao rulesDao;
    @Autowired
    TestCenterDao testCenterDao;


    //列表数据翻译
    public static List<String> fun(List<String> list1){
        List<String> listE = new ArrayList<>();
        for(int i = 0 ; i<list1.size();i++){
            JSONObject jsonObject3 = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(list1.get(i)));
            if(jsonObject3.getString("errorCode").equals("0")){

                listE.add(jsonObject3.getJSONArray("translation").getString(0));
            }else{
                listE.add("请手动翻译");
            }
        }
        return listE;
    }
    @GetMapping("/Daochuleixing")
    @ResponseBody
    public AjaxResult daochuleixing(String name){
        //导出日志
            DClog dClog = new DClog();
            dClog.setUsername(getSession().getAttribute("username").toString());
            dClog.setMiaosu(name);
            dClogDao.save(dClog);
            return AjaxResult.success();
    }

    @PostMapping("/daochu")
    @ResponseBody
    public AjaxResult daochu(Long id,String name) {
//        name="铝膜清洗剂";
//        id=330l;
        Map mapall = new HashMap();

        TestJson testJson = testJsonDao.findByName(name);

       //第一部分数据封装 没有翻译 测试 需要发送样信息id
        Luru luru=luruDao.findId(id);

        //测试有无网络
        if(FanyiV3DemoInternalTest.fanyi(luru.getTuijianyongtu()).equals("无网络")){
            return AjaxResult.error();
        }

        //第一部分翻译 未测试
        Map<String,String> mapyibufen = new HashMap<>();
        JSONObject jsontuijian =JSON.parseObject(FanyiV3DemoInternalTest.fanyi(luru.getTuijianyongtu()));



        if(jsontuijian.getString("errorCode").equals("0")){
            mapyibufen.put("tuijianyongtuE",jsontuijian.getJSONArray("translation").getString(0));
        }else{
            mapyibufen.put("tuijianyongtuE","请手动翻译");
        }

        JSONObject jsonxianzhi = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(luru.getXianzhiyongtu()));
        if(jsonxianzhi.getString("errorCode").equals("0")){
            mapyibufen.put("xianzhiyongtuE",jsonxianzhi.getJSONArray("translation").getString(0));
        }else{
            mapyibufen.put("xianzhiyongtuE","请手动翻译");
        }

        JSONObject jsonSqname = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(luru.getSqname()));
        if(jsonSqname.getString("errorCode").equals("0")){
            mapyibufen.put("SqnameE",jsonSqname.getJSONArray("translation").getString(0));
        }else{
            mapyibufen.put("SqnameE","请手动翻译");
        }

        JSONObject jsonSqadd = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(luru.getSqadd()));
        if(jsonSqadd.getString("errorCode").equals("0")){
            mapyibufen.put("SqaddE",jsonSqadd.getJSONArray("translation").getString(0));
        }else{
            mapyibufen.put("SqaddE","请手动翻译");
        }

        JSONObject jsonGysname = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(luru.getGysname()));
        if(jsonGysname.getString("errorCode").equals("0")){
            mapyibufen.put("GysnameE",jsonGysname.getJSONArray("translation").getString(0));
        }else{
            mapyibufen.put("GysnameE","请手动翻译");
        }

        JSONObject jsonGysadd = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(luru.getGysadd()));
        if(jsonGysadd.getString("errorCode").equals("0")){
            mapyibufen.put("GysaddE",jsonGysadd.getJSONArray("translation").getString(0));
        }else{
            mapyibufen.put("GysaddE","请手动翻译");
        }
          Map<String,Object> mapyibufenall = new HashMap<>();
          mapyibufenall.put("CN",luru);
          mapyibufenall.put("EN",mapyibufen);

          mapall.put("diyibufen",mapyibufenall);

        //大部分封装
        Field[] field = testJson.getClass().getDeclaredFields();
        String str2 = null;
        try{
            for(int i = 0;i<field.length;i++){
                str2 = field[i].getName();
                if(str2.equals("id")||str2.equals("name")||str2.equals("zhufen")||str2.equals("weixian")||str2.equals("usname")||str2.equals("dulixue")||str2.equals("jiechukongzhi")){
                    continue;
                }
                String str1 = "get"+str2.substring(0,1).toUpperCase()+str2.substring(1);
                Method method = testJson.getClass().getMethod(str1, new Class[] {});
                if(method.invoke(testJson, new Object[] {})==null){
                    mapall.put(str2,null);
                }else {
                    Map mapall4 = new HashMap();
                    JSONObject jsonObject4 = JSON.parseObject(method.invoke(testJson, new Object[] {}).toString());

                    for(String string:jsonObject4.keySet()){
                        Map map4 = new HashMap();
                        JSONObject jsonObject = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(jsonObject4.getString(string)));
                        if(jsonObject.getString("errorCode").equals("0")){
                            map4.put(string.trim()+"E",jsonObject.getJSONArray("translation").getString(0));
                            map4.put(string.trim(),jsonObject4.getString(string));
                            mapall4.put(string,map4);
                        }else{
                            map4.put(string.trim()+"E","请手动翻译");
                            map4.put(string.trim(),jsonObject4.getString(string));
                            mapall4.put(string,map4);
                        }

                    }
                    mapall.put(str2,mapall4);
                }


            }

        }catch (Exception e){
            System.out.println("出错了~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+str2);
            e.printStackTrace();
        }

        //第二部分封装 完成
        if(testJson.getWeixian()==null){
            mapall.put("weixianbiaoshi",null);

        }else {
            Map mapall2 = new HashMap();
            JSONObject jsonObject1 = JSON.parseObject(testJson.getWeixian());

            for(String string:jsonObject1.keySet()){
                JSONObject jsonObject=jsonObject1.getJSONObject(string);
                Map map2  = new HashMap();
                for(String str:jsonObject.keySet()){
                    JSONObject jsonObject2 = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(jsonObject.getString(str)));
                    if(jsonObject2.getString("errorCode").equals("0")){
                        map2.put(str.trim()+"E",jsonObject2.getJSONArray("translation").getString(0));
                        map2.put(str.trim(),jsonObject.getString(str));
                    }else{
                        map2.put(str.trim()+"E","请手动翻译");
                        map2.put(str.trim(),jsonObject.getString(str));
                    }

                }
                mapall2.put(string,map2);
            }
            mapall.put("weixianbiaoshi",mapall2);
        }



       //第三部分封装和法规 只有组分名翻译 完成
        if(testJson.getZhufen()==null){
            mapall.put("zufen",null);

        }else{
            Map<String,List<String>> map3 = JSON.parseObject(testJson.getZhufen(),Map.class);
            List<String> listZhufenE  = new ArrayList<>();
            for(int i = 0;i<map3.get("zufen").size();i++){
                JSONObject jsonObject = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(map3.get("zufen").get(i)));
                if(jsonObject.getString("errorCode").equals("0")){
                    listZhufenE.add(jsonObject.getJSONArray("translation").getString(0));
                }else{
                    listZhufenE.add("请手动翻译");
                }
            }
            map3.put("zufenE",listZhufenE);
            //提取法规名称
            List<Rules> list1=rulesDao.findAll();
            List list2 = new ArrayList();
            List list6 = new ArrayList();
            List list4 = new ArrayList();
            List list5 = new ArrayList();
            for(int i = 0;i<list1.size();i++){
                String string= list1.get(i).getRegion();
                if(string.equals("us")){
                    list2.add(list1.get(i).getUsname());
                    list6.add(list1.get(i).getCnname());
                }
                else if(string.equals("china")){
                    list4.add(list1.get(i).getUsname());
                    list5.add(list1.get(i).getCnname());
                }
            }

            List list7 = new ArrayList();
            for(int q = 0;q<list5.size();q++){
                JSONObject jsonObject = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(list5.get(q).toString()));
                if(jsonObject.getString("errorCode").equals("0")){
                    list7.add(jsonObject.getJSONArray("translation").getString(0));
                }else{
                    list7.add("请手动翻译");
                }
            }

            map3.put("fagui",list2);
            map3.put("faguiD",list6);
            map3.put("faguiDE",list2);
            map3.put("faguiC",list4);
            map3.put("faguiCD",list5);
            map3.put("faguiCDE",list7);
            mapall.put("zufen",map3);
        }

        //第十一部分封装
        if(testJson.getDulixue()==null){
            mapall.put("dulixue",null);

        }else {
            Map mapall4 = new HashMap();
            JSONObject jsonObject1 = JSON.parseObject(testJson.getDulixue());

            //翻译other
            Map map2 = new HashMap();
            JSONObject jsonObject2=jsonObject1.getJSONObject("other");
            for(String str:jsonObject2.keySet()){
                JSONObject jsonObject3 = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(jsonObject2.getString(str)));
                if(jsonObject3.getString("errorCode").equals("0")){
                    map2.put(str.trim()+"E",jsonObject3.getJSONArray("translation").getString(0));
                    map2.put(str.trim(),jsonObject2.getString(str));
                }else{
                    map2.put(str.trim()+"E","请手动翻译");
                    map2.put(str.trim(),jsonObject2.getString(str));
                }
            }
            mapall4.put("other",map2);

            //翻译致癌性IARC和NTP
            JSONObject jsonObject4=jsonObject1.getJSONObject("zhiaixing");
            Map<String,Object> map3 = JSON.parseObject(jsonObject4.toJSONString());

            List<String> list1=JSON.parseArray(JSON.toJSONString(jsonObject4.getJSONArray("iarc")),String.class);
            map3.put("iarcE",fun(list1));

            List<String> list2=JSON.parseArray(JSON.toJSONString(jsonObject4.getJSONArray("ntp")),String.class);

            map3.put("ntpE",fun(list2));
            mapall4.put("zhiaixing",map3);

            //翻译急性毒性jk jp xiru
            JSONObject jsonObject5=jsonObject1.getJSONObject("jixingdu");
            Map<String,Object> map4 = JSON.parseObject(jsonObject5.toJSONString());
            List<String> list3=JSON.parseArray(JSON.toJSONString(jsonObject5.getJSONArray("jinkou")),String.class);
            map4.put("jinkouE",fun(list3));

            List<String> list4=JSON.parseArray(JSON.toJSONString(jsonObject5.getJSONArray("jinpi")),String.class);
            map4.put("jinpiE",fun(list4));

            List<String> list5=JSON.parseArray(JSON.toJSONString(jsonObject5.getJSONArray("lc")),String.class);
            map4.put("lcE",fun(list5));

            List<String> list6=JSON.parseArray(JSON.toJSONString(jsonObject5.getJSONArray("jzufen")),String.class);
            map4.put("jzufenE",fun(list6));


            mapall4.put("jixingdu",map4);
            mapall.put("dulixue",mapall4);
        }

        //8部分封装
        if(testJson.getJiechukongzhi()==null){
            mapall.put("jiechukongzhi",null);

        }else {
            Map mapall4 = new HashMap();
            JSONObject jsonObject1 = JSON.parseObject(testJson.getJiechukongzhi());

            //翻译puto
            Map map2 = new HashMap();
            JSONObject jsonObject2=jsonObject1.getJSONObject("puto");
            for(String str:jsonObject2.keySet()){
                JSONObject jsonObject3 = JSON.parseObject(FanyiV3DemoInternalTest.fanyi(jsonObject2.getString(str)));
                if(jsonObject3.getString("errorCode").equals("0")){
                    map2.put(str.trim()+"E",jsonObject3.getJSONArray("translation").getString(0));
                    map2.put(str.trim(),jsonObject2.getString(str));
                }else{
                    map2.put(str.trim()+"E","请手动翻译");
                    map2.put(str.trim(),jsonObject2.getString(str));
                }
            }
            mapall4.put("puto",map2);


            //翻译zhiyejie
            JSONObject jsonObject5=jsonObject1.getJSONObject("zhiyejie");
            Map<String,Object> map4 = JSON.parseObject(jsonObject5.toJSONString());
            List<String> list3=JSON.parseArray(JSON.toJSONString(jsonObject5.getJSONArray("Jzufen")),String.class);
            map4.put("JzufenE",fun(list3));
            map4.put("Jzufen",list3);

            List<String> list4=JSON.parseArray(JSON.toJSONString(jsonObject5.getJSONArray("beizhu")),String.class);
            map4.put("beizhuE",fun(list4));
            map4.put("beizhu",list4);

            List<String> list5=JSON.parseArray(JSON.toJSONString(jsonObject5.getJSONArray("biaozhunlai")),String.class);
            map4.put("biaozhunlaiE",fun(list5));
            map4.put("biaozhunlai",list5);

            List<String> list6=JSON.parseArray(JSON.toJSONString(jsonObject5.getJSONArray("biaozhunzhi")),String.class);
            map4.put("biaozhunzhiE",fun(list6));
            map4.put("biaozhunzhi",list6);
            List<String> list7=JSON.parseArray(JSON.toJSONString(jsonObject5.getJSONArray("leixin")),String.class);
            map4.put("leixinE",fun(list7));
            map4.put("leixin",list7);

            mapall4.put("zhiyejie",map4);
            mapall.put("jiechukongzhi",mapall4);
        }

        //检测中心封装
        TestCenter testCenter = testCenterDao.find();
        mapall.put("testCenter",testCenter);

        return AjaxResult.success(mapall);

    }
}
