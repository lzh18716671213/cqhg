package cn.lzh.cqhg1.controller;


import cn.lzh.cqhg1.Bean.TestCenter;

import cn.lzh.cqhg1.dao.TestCenterDao;
import cn.lzh.cqhg1.utils.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class TestCenterController extends BaseController{
    @Autowired
    TestCenterDao testCenterDao;
    @GetMapping("/test-center-info")
    public String test(){
        return "admin/test-center-info";
    }

    //中心展示
    @GetMapping("/findTestCenter")
    @ResponseBody
    public AjaxResult findTestCenter(int page, int limit){

        List<TestCenter> list = testCenterDao.findAll();

        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        Page<TestCenter> p=testCenterDao.findAll(pageable);

        return AjaxResult.success(p.getContent());

    }

    //修改
    @PostMapping("/updateTestCenter")
    @ResponseBody
    public AjaxResult updateTestCenter(TestCenter testCenter){
        testCenterDao.save(testCenter);
        return AjaxResult.success();
    }

//    //添加
//    @PostMapping("/insertTestCenter")
//    @ResponseBody
//    public AjaxResult insertTestCenter(TestCenter testCenter){
//        testCenterDao.save(testCenter);
//        return AjaxResult.success();
//    }





}
