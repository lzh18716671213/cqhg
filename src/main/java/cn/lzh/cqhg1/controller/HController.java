package cn.lzh.cqhg1.controller;

import cn.lzh.cqhg1.Bean.*;
import cn.lzh.cqhg1.dao.HDao;
import cn.lzh.cqhg1.dao.LuruDao;
import cn.lzh.cqhg1.utils.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class HController extends BaseController{
    @Autowired
    HDao hDao;
    @Autowired
    LuruDao luruDao;



    //前台检验必填
    @ResponseBody
    @PostMapping("/H1")
    public AjaxResult findByHcode(@RequestBody Map<String,List<String>> map){

        List<String> list = map.get("data");

        List listAll =  new ArrayList<>();

        Map<String,String> GhsMap1 = new HashMap<>();
        Map<String,String> GhsMap2 = new HashMap<>();
        Map<String,String> weixianying = new HashMap<>();
        Map<String,String> yufang = new HashMap<>();
        Map<String,String> shigu = new HashMap<>();
        Map<String,String> anquan= new HashMap<>();
        Map<String,String> feiqi = new HashMap<>();

        Map<String,String> yunshu= new HashMap<>();


        for(int i = 0;i<list.size();i++){
            H h=hDao.finid(Long.parseLong(list.get(i)));


            //ghs危险性类别
            GhsMap1.put(h.getLeibie(),h.getFenlei());
            //ghs标签要素
            GhsMap2.put("象形图"+i,h.getQqxiangxintu());
            GhsMap2.put("信号词"+i,h.getXinhaoci());

            //运输标签
            yunshu.put("运输标签和标记"+i,h.getLhgxiangxintu());

            //危险性说明
            weixianying.put(h.getHcode(),h.getShuoming());
            //预防措施
            List<P> yufangP =h.getSsyufangcuoshiP();

            for(int j = 0;j<yufangP.size();j++){
                yufang.put(yufangP.get(j).getPcode(),yufangP.get(j).getYufangcuoshishuoming());
            }
            //事故响应
            List<P> shiguP = h.getSsyingduicuoshiP();
            for(int q = 0;q<shiguP.size();q++){
                shigu.put(shiguP.get(q).getPcode(),shiguP.get(q).getYufangcuoshishuoming());
            }
            //安全存储
            List<P> anquanP = h.getSscunfangP();
            for(int p = 0;p<anquanP.size();p++){
                anquan.put(anquanP.get(p).getPcode(),anquanP.get(p).getYufangcuoshishuoming());
            }
            //废弃处置
            List<P> feiqiP = h.getSschuzhiP();

            for(int e = 0;e<feiqiP.size();e++){
                feiqi.put(feiqiP.get(e).getPcode(),feiqiP.get(e).getYufangcuoshishuoming());
            }

        }



        Map<String,Object> M1 = new HashMap<>();
       // M1.put("name",luruDao.findName().getCnname());
        M1.put("GHSweixianxingleibie",GhsMap1);
        M1.put("GHSbiaoqianyaoshu",GhsMap2);
        M1.put("weixianyingshuo",weixianying);
        M1.put("yufangcuoshi",yufang);
        M1.put("shiguxiangying",shigu);
        M1.put("anquanchunchu",anquan);
        M1.put("feiqichuzhi",feiqi);
        M1.put("yunshubiaoqian",yunshu);


        listAll.add(M1);
        return AjaxResult.success(listAll);
    }
}
