package cn.lzh.cqhg1.controller;


import cn.lzh.cqhg1.Bean.Rules;
import cn.lzh.cqhg1.Bean.TestJson;
import cn.lzh.cqhg1.dao.RulesDao;
import cn.lzh.cqhg1.dao.TestJsonDao;
import cn.lzh.cqhg1.utils.AjaxResult;
import cn.lzh.cqhg1.utils.FanyiV3DemoInternalTest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class chemicalsController extends BaseController{
    @Autowired
    TestJsonDao testJsonDao;
    @Autowired
    RulesDao rulesDao;
    @GetMapping("/chemicals")
    public String test(){
        return "public/chemicals";
    }

    @GetMapping("/dasisEdit")
    public String test7(){
        return "public/dasisEdit";
    }



    //所有化学品信息
    @GetMapping("/findAllChemicals")
    @ResponseBody
    public AjaxResult test3(int page,int limit){
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        Page<TestJson> p=testJsonDao.findAll(pageable);
        return AjaxResult.success(p.getTotalElements(),p.getContent());
    }

    //查找化学品信息
    @GetMapping("/findChemical")
    @ResponseBody
    public AjaxResult test4(String tick,int page,int limit){
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        Page<TestJson> p=testJsonDao.findSql(tick,pageable);
        return AjaxResult.success(p.getTotalElements(),p.getContent());
    }

    //化学品管理 测试
    @PostMapping("/guangli")
    @ResponseBody
    public AjaxResult test1(String name){
//        //测试
//        name="铝膜清洗剂";

        getSession().setAttribute("huaxuepingname",name);
        getSession().setMaxInactiveInterval(86400);
        TestJson testJson = testJsonDao.findByName(name);
        Map mapall = new HashMap();
        Field[] field = testJson.getClass().getDeclaredFields();
        String str2 = null;
        try{
            for(int i = 0;i<field.length;i++){
                str2 = field[i].getName();
                if(str2.equals("id")||str2.equals("name")||str2.equals("zhufen")||str2.equals("weixian")||str2.equals("usname")||str2.equals("dulixue")||str2.equals("jiechukongzhi")){
                    continue;
                }
                String str1 = "get"+str2.substring(0,1).toUpperCase()+str2.substring(1);
                Method method = testJson.getClass().getMethod(str1, new Class[] {});

                if(method.invoke(testJson, new Object[] {})==null){
                    mapall.put(str2,null);
                }else {
                    JSONObject jsonObject4 = JSON.parseObject(method.invoke(testJson, new Object[] {}).toString());

                    Map mapall4 = new HashMap();
                    for(String string:jsonObject4.keySet()){
                        Map map4 = new HashMap();

                        map4.put(string.trim(),jsonObject4.getString(string));
                        mapall4.put(string,map4);
                    }
                    mapall.put(str2,mapall4);
                }
                }





        }catch (Exception e){
            System.out.println("出错了~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+str2);
            e.printStackTrace();
        }

        //第二部分封装  完成
        if(testJson.getWeixian()==null){
            mapall.put("weixianbiaoshi",null);
        }else{
            Map mapall2 = new HashMap();
            JSONObject jsonObject1 = JSON.parseObject(testJson.getWeixian());

            for(String string:jsonObject1.keySet()){
                JSONObject jsonObject=jsonObject1.getJSONObject(string);
                Map map2  = new HashMap();
                for(String str:jsonObject.keySet()){

                    map2.put(str.trim(),jsonObject.getString(str));
                }
                mapall2.put(string,map2);
            }
            mapall.put("weixianbiaoshi",mapall2);
        }



        //第三部分封装和法规 只有组分名翻译 完成
        if(testJson.getZhufen()==null){
            mapall.put("zufen",null);
        }else{
            Map<String, List<String>> map3 = JSON.parseObject(testJson.getZhufen(),Map.class);

            //提取法规名称
            List<Rules> list1=rulesDao.findAll();
            List list2 = new ArrayList();
            List list6 = new ArrayList();
            List list4 = new ArrayList();
            List list5 = new ArrayList();
            for(int i = 0;i<list1.size();i++){
                String string= list1.get(i).getRegion();
                if(string.equals("us")){
                    list6.add(list1.get(i).getCnname());
                    list2.add(list1.get(i).getUsname());
                }
                else if(string.equals("china")){
                    list4.add(list1.get(i).getUsname());
                    list5.add(list1.get(i).getCnname());
                }
            }
            map3.put("fagui",list2);
            map3.put("faguiD",list6);

            map3.put("faguiC",list4);
            map3.put("faguiCD",list5);
            mapall.put("zufen",map3);
        }

        //第十一部分封装
        if(testJson.getDulixue()==null){
            mapall.put("dulixue",null);
        }else {
            Map<String, List<String>> map11 = JSON.parseObject(testJson.getDulixue(),Map.class);
            mapall.put("dulixue",map11);
        }

        //第8部分封装
        if(testJson.getJiechukongzhi()==null){
            mapall.put("jiechukongzhi",null);
        }else {
            Map<String, List<String>> map11 = JSON.parseObject(testJson.getJiechukongzhi(),Map.class);
            mapall.put("jiechukongzhi",map11);
        }





        return  AjaxResult.success(mapall);
    }
}
