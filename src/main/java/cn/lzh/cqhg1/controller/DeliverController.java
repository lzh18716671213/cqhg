package cn.lzh.cqhg1.controller;

import cn.lzh.cqhg1.Bean.Luru;
import cn.lzh.cqhg1.Bean.TestJson;
import cn.lzh.cqhg1.dao.LuruDao;
import cn.lzh.cqhg1.dao.TestJsonDao;
import cn.lzh.cqhg1.utils.AjaxResult;
import cn.lzh.cqhg1.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Enumeration;


@Controller
public class DeliverController extends BaseController{
    @Autowired
    LuruDao luruDao;

    @Autowired
    TestJsonDao testJsonDao;
    @GetMapping("/Deliver")
    public String test(){
        return "public/Deliver";
    }


    //存入送样信息
    @PostMapping("/submitDeliver")
    @ResponseBody
    public AjaxResult test2(Luru luru){
        if(Utils.isNull(luru)||Utils.isEmpty(luru.getCnname())){
           return AjaxResult.error();
        }
        TestJson testJson1 = testJsonDao.findByName(luru.getCnname());

        luruDao.save(luru);
        if(Utils.isNull(testJson1)){
            //化学品名字存入session
            getSession().setAttribute("huaxuepingname",luru.getCnname());
            //session 消失时间24小时
            getSession().setMaxInactiveInterval(86400);
            //生成大表
            TestJson testJson = new TestJson();
            testJson.setName(luru.getCnname());
            testJson.setUsname(luru.getUsname());
            testJsonDao.save(testJson);
            return AjaxResult.error();
        }else{
            return AjaxResult.success(luru);
        }

    }

    //所有送样信息
    @GetMapping("/findAllDeliver")
    @ResponseBody
    public AjaxResult test3(int page,int limit){
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        Page<Luru> p=luruDao.findAll(pageable);
        return AjaxResult.success(p.getTotalElements(),p.getContent());
    }

    //查找送样信息
    @GetMapping("/findDeliver")
    @ResponseBody
    public AjaxResult test4(String tick,int page,int limit){
            Sort sort = new Sort(Sort.Direction.DESC,"id");
            Pageable pageable = PageRequest.of(page-1,limit,sort);
            Page<Luru> p=luruDao.findSql(tick,pageable);
            return AjaxResult.success(p.getTotalElements(),p.getContent());
    }

    //查看详情
    @GetMapping("/xiangQing")
    @ResponseBody
    public AjaxResult test4(Long id){
        Luru luru = luruDao.findId(id);
        return AjaxResult.success(luru);
    }

    //删除详情
    @GetMapping("/deleteDeliver")
    @ResponseBody
    public AjaxResult test5(Long id){
        luruDao.deleteById(id);
        return AjaxResult.success();
    }
    //修改
    @PostMapping("/updateDeliver")
    @ResponseBody
    public AjaxResult test5(Luru luru){
        luruDao.save(luru);
        return AjaxResult.success();
    }
}
