package cn.lzh.cqhg1.controller;


import cn.lzh.cqhg1.Bean.User;
import cn.lzh.cqhg1.dao.UserDao;
import cn.lzh.cqhg1.utils.AjaxResult;
import cn.lzh.cqhg1.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.criteria.*;


@Controller
public class UserController extends BaseController{
    @Autowired
    UserDao userDao;

    @GetMapping("/userM")
    public String userM(){
        return "admin/userM";
    }

    //用户管理
    @ResponseBody
    @GetMapping("/findAllUser")
    public AjaxResult findAll(int page,int limit){


        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        User user=(User)getSession().getAttribute("user");
        if(user.getUsername().equals("admin")){
            Page<User> p=userDao.findAllRemove(pageable,getSession().getAttribute("username").toString());
            return AjaxResult.success(p.getTotalElements(),p.getContent());

        }else {
            Page<User> p=userDao.findAllRemoveUser(pageable,"user");
            return AjaxResult.success(p.getTotalElements(),p.getContent());
        }


    }

    //搜索用户
    @ResponseBody
    @GetMapping("/searchUser")
    public AjaxResult searchUser(String tick,int page,int limit){

        if(tick.equals("admin")||getSession().getAttribute("username").equals(tick)){
            return AjaxResult.success(0l,null);
        }
        Specification<User> spec = new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                  Path<Object> name=root.get("username");
                  Predicate pre=criteriaBuilder.equal(name,tick);
                  return pre;
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);


        Page<User> p=userDao.findAll(spec,pageable);
        return AjaxResult.success(p.getTotalElements(),p.getContent());
    }


    //注册用户
    @PostMapping("/registerUser")
    @ResponseBody
    public AjaxResult registerUser(User user){
        User us = userDao.findByUsername(user.getUsername());
        if(Utils.isNull(us)){
            user.setPassword(Utils.getMD5String(user.getPassword()));
            userDao.save(user);
            return AjaxResult.success();
        }else{
            //用户名重复
            return AjaxResult.error();
        }

    }

    //管理员重置密码
    @PostMapping("/rePassword")
    @ResponseBody
    public AjaxResult updateUser(Long id,String passwordNew,String passwordOld){
        passwordNew=Utils.getMD5String(passwordNew);
        userDao.updatePassword(passwordNew,id);
        return AjaxResult.success();
    }

    //更新status
    @GetMapping("/updateStatus")
    @ResponseBody

    public AjaxResult updateStatus(int status,Long id){

        userDao.updateStatus(status,id);
        return AjaxResult.success();
    }
    //更新type
    @GetMapping("/updateType")
    @ResponseBody
    public AjaxResult updateType(String type,Long id){


        userDao.updateType(type,id);
        return AjaxResult.success();
    }

    //用户修改密码
    @PostMapping("/updatePass")
    @ResponseBody
    public AjaxResult updatePass(String passwordNew,String passwordOld){
        String name=getSession().getAttribute("username").toString();
        User user = userDao.findByUsernameAndPassword(name,Utils.getMD5String(passwordOld));


        if(Utils.isNull(user)){
            return AjaxResult.error();
        }else{
            passwordNew=Utils.getMD5String(passwordNew);
            user.setPassword(passwordNew);
            userDao.save(user);
            return AjaxResult.success();
        }

    }
}
