package cn.lzh.cqhg1.controller;
import org.springframework.beans.factory.annotation.Autowired;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



public class BaseController {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpSession session;

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpSession getSession() {
        return session;
    }



}
