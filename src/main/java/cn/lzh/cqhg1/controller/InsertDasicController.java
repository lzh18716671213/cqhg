package cn.lzh.cqhg1.controller;



import cn.lzh.cqhg1.Bean.TestJson;
import cn.lzh.cqhg1.Bean.bufen.*;
import cn.lzh.cqhg1.dao.*;
import cn.lzh.cqhg1.utils.AjaxResult;
import cn.lzh.cqhg1.utils.Utils;
import com.alibaba.fastjson.JSON;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class InsertDasicController extends BaseController{

    @Autowired
    RulesDao rulesDao;
    @Autowired
    TestJsonDao testJsonDao;

    @PersistenceContext
    private EntityManager em;

    @GetMapping("/insertDasic")
    public String insertDasic(){
        return "public/insert-dasic";
    }

    //联想
    @PostMapping("/lianxiang")
    @ResponseBody
    public AjaxResult lianxiang(String val,String zdName,String conName) throws UnsupportedEncodingException {
        String sql = "select "+ conName+" from test";
        Query query=em.createNativeQuery(sql);
        List<String> list = query.getResultList();
        List<String> list1 = new ArrayList<>();
        for(int i = 0;i<list.size();i++){
            if(list.get(i)!=null){
               String string = JSON.parseObject(list.get(i)).getString(zdName);

               if(string.indexOf(val)!=-1){
                   list1.add(string);
               }
            }
        }
        return AjaxResult.success(list1);

    }

    @PostMapping("/weixian")
    @ResponseBody
    @Transactional
    public  AjaxResult weixian(@RequestBody Map<String,Object> map){

        testJsonDao.updateweixian(JSON.toJSONString(map),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }



    @PostMapping("/chengFen")
    @ResponseBody
    @Transactional
    public AjaxResult chengFen(@RequestBody Map<String,List<String>> map){

        TestJson testJson = testJsonDao.findByName(getSession().getAttribute("huaxuepingname").toString());
        Map<String,List<String>> map3 = JSON.parseObject(testJson.getZhufen(),Map.class);
        if(Utils.isNull(map3)){
            testJsonDao.updatezhufen(JSON.toJSONString(map),getSession().getAttribute("huaxuepingname").toString());
            return AjaxResult.success();
        }else{
            map3.putAll(map);
            testJsonDao.updatezhufen(JSON.toJSONString(map3),getSession().getAttribute("huaxuepingname").toString());
            return AjaxResult.success();
        }

    }


    //急救措施提交
    @PostMapping("/jijiucuoshi")
    @ResponseBody
    @Transactional
    public  AjaxResult jijiuchushi(Jijiucuoshi jijiucuoshi){
        testJsonDao.updatejijiucuoshi(JSON.toJSONString(jijiucuoshi),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }

    //消防措施提交
    @PostMapping("/xiaofangcuoshi")
    @ResponseBody
    @Transactional
    public  AjaxResult xiaofangcuoshi(Xiaofangcuoshi xiaofangcuoshi){
        testJsonDao.updatexiaofangcuoshi(JSON.toJSONString(xiaofangcuoshi),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }

    //泄露应急处理提交
    @PostMapping("/xielouyingji")
    @ResponseBody
    @Transactional
    public  AjaxResult xielouyingji(Xielouyingjichuli xielouyingjichuli){
        testJsonDao.updatexielouyingji(JSON.toJSONString(xielouyingjichuli),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }

    //操作与存储提交
    @PostMapping("/caozhuoyucunchu")
    @ResponseBody
    @Transactional
    public  AjaxResult caozhuoyucunchu(Caozuoyucunchu caozuoyucunchu){
        testJsonDao.updatecaozhuoyucunchu(JSON.toJSONString(caozuoyucunchu),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }


    //接触控制提交
    @PostMapping("/jiechukongzhi")
    @ResponseBody
    @Transactional
    public  AjaxResult jiechukongzhi(@RequestBody Map<String,Object> map){
        testJsonDao.updatejiechukongzhi(JSON.toJSONString(map),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }

    //物化特性提交
    @PostMapping("/wuhuatexing")
    @ResponseBody
    @Transactional
    public  AjaxResult wuhuatexing(Wuhuatexing wuhuatexing){
        testJsonDao.updatewuhuatexing(JSON.toJSONString(wuhuatexing),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }

    //稳定性提交
    @PostMapping("/wendingying")
    @ResponseBody
    @Transactional
    public  AjaxResult wendingying(Wendingying wendingying){
        testJsonDao.updatewendingying(JSON.toJSONString(wendingying),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }


    //毒理学提交
    @PostMapping("/dulixue")
    @ResponseBody
    @Transactional
    public  AjaxResult dulixue(@RequestBody Map<String,Object> map){
        testJsonDao.updatedulixue(JSON.toJSONString(map),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }

    //生态学提交
    @PostMapping("/shengtaixue")
    @ResponseBody
    @Transactional
    public  AjaxResult shengtaixue(Shengtaixue shengtaixue){
        testJsonDao.updateshengtaixue(JSON.toJSONString(shengtaixue),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }

    //废弃处置提交
    @PostMapping("/feiqichuzhi")
    @ResponseBody
    @Transactional
    public  AjaxResult feiqichuzhi(Feiqichuzhi feiqichuzhi){
        testJsonDao.updatefeiqichuzhi(JSON.toJSONString(feiqichuzhi),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }
    //运输信息提交
    @PostMapping("/yunshuxingxi")
    @ResponseBody
    @Transactional
    public  AjaxResult yunshuxingxi(Yunshuxingxi yunshuxingxi){

        testJsonDao.updateyunshuxingxi(JSON.toJSONString(yunshuxingxi),getSession().getAttribute("huaxuepingname").toString());
        return  AjaxResult.success();
    }




}
