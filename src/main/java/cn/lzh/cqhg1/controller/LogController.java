package cn.lzh.cqhg1.controller;

import cn.lzh.cqhg1.Bean.DClog;
import cn.lzh.cqhg1.Bean.DLlog;

import cn.lzh.cqhg1.dao.DClogDao;
import cn.lzh.cqhg1.dao.DLlogDao;
import cn.lzh.cqhg1.utils.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.criteria.*;
import java.util.List;

@Controller
public class LogController extends BaseController{
    @Autowired
    DLlogDao dLlogDao;
    @Autowired
    DClogDao dClogDao;

    @GetMapping("/loginLog")
    public String loginLog(){
        return "admin/loginLog";
    }


    @GetMapping("/exportLog")
    public String DCLog(){
        return "admin/exportLog";
    }


    //全部登录日志展示
    @GetMapping("/findAllLog")
    @ResponseBody
    public AjaxResult findAllLog(int page,int limit){

        List<DLlog> list = dLlogDao.findAll();

        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        Page<DLlog> p=dLlogDao.findAll(pageable);

        return AjaxResult.success(p.getTotalElements(),p.getContent());

    }
    //全部导出日志展示
    @GetMapping("/findAllDcLog")
    @ResponseBody
    public AjaxResult findAllDcLog(int page,int limit){

        List<DClog> list = dClogDao.findAll();

        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        Page<DClog> p=dClogDao.findAll(pageable);
        return AjaxResult.success(p.getTotalElements(),p.getContent());

    }



    //搜索登录日志
    @GetMapping("/findLog")
    @ResponseBody
    public AjaxResult findLog(String tick,int page,int limit){
        Specification<DLlog> spec = new Specification<DLlog>() {
            @Override
            public Predicate toPredicate(Root<DLlog> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path<Object> name=root.get("username");
                Predicate pre=criteriaBuilder.equal(name,tick);
                return pre;
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);

        Page<DLlog> p=dLlogDao.findAll(spec,pageable);
        return AjaxResult.success(p.getTotalElements(),p.getContent());

    }

    //搜索导出日志
    @GetMapping("/findDcLog")
    @ResponseBody
    public AjaxResult findDcLog(String tick,int page,int limit){
        Specification<DClog> spec = new Specification<DClog>() {
            @Override
            public Predicate toPredicate(Root<DClog> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Path<Object> name=root.get("username");
                Predicate pre=criteriaBuilder.equal(name,tick);
                return pre;
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);

        Page<DClog> p=dClogDao.findAll(spec,pageable);
        return AjaxResult.success(p.getTotalElements(),p.getContent());

    }





}
