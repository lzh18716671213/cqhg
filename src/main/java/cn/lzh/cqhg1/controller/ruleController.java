package cn.lzh.cqhg1.controller;

import cn.lzh.cqhg1.Bean.Rules;
import cn.lzh.cqhg1.Bean.TestJson;

import cn.lzh.cqhg1.dao.RulesDao;
import cn.lzh.cqhg1.dao.TestJsonDao;
import cn.lzh.cqhg1.utils.AjaxResult;
import cn.lzh.cqhg1.utils.Utils;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class ruleController extends BaseController{
    @Autowired
    RulesDao rulesDao;


    @Autowired
    TestJsonDao testJsonDao;

    @GetMapping("/rulesF")
    public String test1(){
        return "admin/rulesF";
    }

    //修改法规
    @PostMapping("/updateRules")
    @ResponseBody
    public AjaxResult test9(Rules rules){
        rulesDao.save(rules);
        return AjaxResult.success();
    }
    //找所有法规
    @GetMapping("/findRules")
    @ResponseBody
    public AjaxResult test2(int page, int limit){


        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        Page<Rules> p=rulesDao.findAll(pageable);

        return AjaxResult.success(p.getTotalElements(),p.getContent());
    }

    //添加法规 有大问题
    @PostMapping("/insertRules")
    @ResponseBody
    public AjaxResult test3(Rules rules){
        List<TestJson> testJson=testJsonDao.findAll();
        //没有化学品 不能添加法规
        if(Utils.isNull(testJson)){
            return AjaxResult.error();
        }
        rulesDao.save(rules);
        String string = rules.getRegion();

        int a =0;
        int b =0;
        if(string.equals("us")){
            for(int i =0 ;i<testJson.size();i++){
                Map<String,List<String>> mapZhufen=JSON.parseObject(testJson.get(i).getZhufen(),Map.class);
                if(Utils.isNull(mapZhufen)||!mapZhufen.containsKey("n")||!mapZhufen.containsKey("zufen0")){
                    b++;
                    continue;
                }
                for(int j = 0;j<Integer.parseInt(mapZhufen.get("n").get(0));j++){
                    mapZhufen.get("zufen"+j).add(null);
                }
                testJson.get(i).setZhufen(JSON.toJSONString(mapZhufen));
                testJsonDao.save(testJson.get(i));
            }
        }
        else if(string.equals("china")){
            for(int i =0 ;i<testJson.size();i++){
                Map<String,List<String>> mapZhufen=JSON.parseObject(testJson.get(i).getZhufen(),Map.class);

                if(Utils.isNull(mapZhufen)||!mapZhufen.containsKey("n")||!mapZhufen.containsKey("zufenC0")){
                    b++;
                    continue;
                }
                for(int j = 0;j<Integer.parseInt(mapZhufen.get("n").get(0));j++){
                    mapZhufen.get("zufenC"+j).add(null);
                }
                testJson.get(i).setZhufen(JSON.toJSONString(mapZhufen));
                testJsonDao.save(testJson.get(i));
            }
        }
//        //没有输入组分和是否 其他化学品都更改 测试
//        if(a!=0||b!=0){
//            return AjaxResult.error();
//        }
        return AjaxResult.success();
    }

    //返回组分和CAS
    @PostMapping("/returnZhufenAndCas")
    @ResponseBody
    public AjaxResult test6(){
        Map<String,Object> map = new HashMap<>();
        TestJson testJson = testJsonDao.findByName(getSession().getAttribute("huaxuepingname").toString());

        //测试
        if(Utils.isNull(testJson.getZhufen())||!testJson.getZhufen().contains("zufen")||!testJson.getZhufen().contains("CAS")){
            return AjaxResult.error();
        }
        map.put("zufen",JSON.parseObject(testJson.getZhufen()).get("zufen"));
        map.put("cas",JSON.parseObject(testJson.getZhufen()).get("CAS"));
        return AjaxResult.success(map);
    }

    //返回所有法规名称和组分名称
    @PostMapping("/returnRuleAndZhufen")
    @ResponseBody
    public AjaxResult test4(){
        Map<String,Object> map = new HashMap<>();
        List<Map<String,Object>> listAll = new ArrayList<>();
        //提取法规名称
        List<Rules> list1=rulesDao.findAll();
        //根据化学名提取组分名称
        TestJson testJson = testJsonDao.findByName(getSession().getAttribute("huaxuepingname").toString());

        //测试
        if(Utils.isNull(list1)||Utils.isNull(testJson)){
            return AjaxResult.error();
        }
        List list2 = new ArrayList();
        List list3 = new ArrayList();
        List list4 = new ArrayList();
        List list5 = new ArrayList();
        for(int i = 0;i<list1.size();i++){
            String string= list1.get(i).getRegion();
            if(string.equals("us")){
                list2.add(list1.get(i).getUsname());
                list3.add(list1.get(i).getCnname());
            }
            else if(string.equals("china")){
                list4.add(list1.get(i).getUsname());
                list5.add(list1.get(i).getCnname());
            }
        }


        map.put("fagui",list2);
        map.put("faguiD",list3);
        map.put("faguiC",list4);
        map.put("faguiCD",list5);
        map.put("zufen",JSON.parseObject(testJson.getZhufen()).get("zufen"));

        listAll.add(map);
        return AjaxResult.success(listAll);
    }



    @GetMapping("/FaguiAndZhufen")
    @ResponseBody
    public  AjaxResult test5(){
        return AjaxResult.success();
    }


    //接收法规和成分的是否 测试
    @PostMapping("/Submitxuanzhe")
    @ResponseBody
    public AjaxResult test6(@RequestBody Map<String,List<String>> map){

        //{zufen0=[是, 是, 是, 是, 是], zufenC0=[是, 是, 是], zufen1=[是, 是, 是, 是, 是], zufenC1=[是, 是, 是], n=[2]}



        int n = Integer.parseInt(map.get("n").get(0));

        TestJson testJson=testJsonDao.findByName(getSession().getAttribute("huaxuepingname").toString());
        Map mapZhufen = JSON.parseObject(testJson.getZhufen(),Map.class);

        for(int i = 0;i<n;i++){
            mapZhufen.put("zufen"+i,map.get("zufen"+i));
            mapZhufen.put("zufenC"+i,map.get("zufenC"+i));
        }
        testJson.setZhufen(JSON.toJSONString(mapZhufen));

        testJsonDao.save(testJson);
        return AjaxResult.success();
    }

}

