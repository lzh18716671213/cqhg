package cn.lzh.cqhg1.dao;


import cn.lzh.cqhg1.Bean.Lianxiang;
import cn.lzh.cqhg1.Bean.TestJson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TestJsonDao extends BaseRepository<TestJson> {

    @Query(value = "select jijiucuoshi from test",nativeQuery = true)
    public List<String> lianxiang(String conName);

    @Query(value = "select * from test where id=?",nativeQuery = true)
    public TestJson findId(Long id);

    @Query(value = "select * from test where name=?1",nativeQuery = true)
    public Page<TestJson> findSql(String tick, Pageable pageable);


    @Query(value = "update test set weixian=? where name=?",nativeQuery = true)
    @Modifying
    public void updateweixian(String weixian,String name);



    @Query(value = "update test set zhufen=? where name=?",nativeQuery = true)
    @Modifying
    public void updatezhufen(String weixian,String name);


    @Query(value = "update test set jijiucuoshi=? where name=?",nativeQuery = true)
    @Modifying
    public void updatejijiucuoshi(String weixian,String name);

    @Query(value = "update test set xiaofangcuoshi=? where name=?",nativeQuery = true)
    @Modifying
    public void updatexiaofangcuoshi(String weixian,String name);


    @Query(value = "update test set xielouyingji=? where name=?",nativeQuery = true)
    @Modifying
    public void updatexielouyingji(String weixian,String name);


    @Query(value = "update test set caozhuoyucunchu=? where name=?",nativeQuery = true)
    @Modifying
    public void updatecaozhuoyucunchu(String weixian,String name);


    @Query(value = "update test set jiechukongzhi=? where name=?",nativeQuery = true)
    @Modifying
    public void updatejiechukongzhi(String weixian,String name);

    @Query(value = "update test set wuhuatexing=? where name=?",nativeQuery = true)
    @Modifying
    public void updatewuhuatexing(String weixian,String name);



    @Query(value = "update test set wendingying=? where name=?",nativeQuery = true)
    @Modifying
    public void updatewendingying(String weixian,String name);



    @Query(value = "update test set dulixue=? where name=?",nativeQuery = true)
    @Modifying
    public void updatedulixue(String weixian,String name);


    @Query(value = "update test set shengtaixue=? where name=?",nativeQuery = true)
    @Modifying
    public void updateshengtaixue(String weixian,String name);



    @Query(value = "update test set feiqichuzhi=? where name=?",nativeQuery = true)
    @Modifying
    public void updatefeiqichuzhi(String weixian,String name);

    @Query(value = "update test set yunshuxingxi=? where name=?",nativeQuery = true)
    @Modifying
    public void updateyunshuxingxi(String weixian,String name);

    public TestJson findByName(String name);

    @Query(value = "select zhufen from test ",nativeQuery = true)
    public List<String> findZhufen();







}
