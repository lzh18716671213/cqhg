package cn.lzh.cqhg1.dao;

import cn.lzh.cqhg1.Bean.H;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface HDao extends BaseRepository<H> {

    //public List<H> findByHcode(String hcode);
    public H findByHcode(String hcode);
    public List<H> findAll();

    @Query(value = "select * from h_copy where id=?",nativeQuery = true)
    public H finid(Long id);

}
