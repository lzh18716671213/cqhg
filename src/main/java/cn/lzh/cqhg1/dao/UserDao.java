package cn.lzh.cqhg1.dao;

import cn.lzh.cqhg1.Bean.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;


public interface UserDao extends BaseRepository<User> {


      public User findByUsername(String username);
      public User findByUsernameAndPassword(String username,String password);
      public User findByIdAndPassword(Long id,String password);

      @Query(value = "select * from user where username!=?",nativeQuery = true)
      public Page<User> findAllRemove(Pageable pageable,String username);

      @Query(value = "select * from user where type=?",nativeQuery = true)
      public Page<User> findAllRemoveUser(Pageable pageable,String type);

      //根据id更新status
      @Query(value = "update user set status=? where id =?",nativeQuery = true)
      @Modifying
      @Transactional
      public void updateStatus(int status,Long id);

      //根据id更新type
      @Query(value = "update user set type=? where id =?",nativeQuery = true)
      @Modifying
      @Transactional
      public void updateType(String type,Long id);

      //根据id更新password
      @Query(value = "update user set password=? where id =?",nativeQuery = true)
      @Modifying
      @Transactional
      public void updatePassword(String password,Long id);
}
