package cn.lzh.cqhg1.dao;

import cn.lzh.cqhg1.Bean.Luru;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LuruDao extends BaseRepository<Luru> {
    public List<Luru> findByCnname(String tick);

    @Query(value = "select * from luru where id=?",nativeQuery = true)
    public Luru findId(Long id);

    @Query(value = "select * from luru where cnname=?1 or sqname=?1 or sqtel=?1 or gysname=?1 or gystel=?1",nativeQuery = true)
    public Page<Luru> findSql(String tick,Pageable pageable);


}
