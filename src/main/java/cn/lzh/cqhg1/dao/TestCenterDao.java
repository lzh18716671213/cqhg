package cn.lzh.cqhg1.dao;

import cn.lzh.cqhg1.Bean.TestCenter;
import org.springframework.data.jpa.repository.Query;

public interface TestCenterDao extends BaseRepository<TestCenter> {
    @Query(value = "SELECT * FROM jianchezhongxin order by id desc limit 1",nativeQuery = true)
    public TestCenter find();
}
