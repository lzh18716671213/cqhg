package cn.lzh.cqhg1.Bean;

import cn.lzh.cqhg1.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "luru")
public class Luru {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String cnname;
    private String usname;
    private String biename;
    private String casno;
    private String ecno;
    private String fenzisi;
    private String tuijianyongtu;
    private String xianzhiyongtu;
    private String sqname;
    private String sqadd;
    private String sqcode;
    private String sqtel;
    private String sqfax;
    private String sqyouxiang;
    private String gysname;
    private String gysadd;
    private String gyscode;
    private String gystel;
    private String gysfax;
    private String gysyouxiang;
    private String qiyetel;
    private String bianhao;
    private String shuliang;
    private String jiancheyiju;
    private String yangpingbiaoshi;
    private String yangpingxingzhuang;

    private String jianguanren;
    private String yewulei;
    private String jianceri;
    private String yangpinxin;
    private String jishuming;
    private String shuyu;
    private String fuhe;


    public String getJianguanren() {
        return jianguanren;
    }

    public void setJianguanren(String jianguanren) {
        this.jianguanren = jianguanren;
    }

    public String getYewulei() {
        return yewulei;
    }

    public void setYewulei(String yewulei) {
        this.yewulei = yewulei;
    }

    public String getJianceri() {
        return jianceri;
    }

    public void setJianceri(String jianceri) {
        this.jianceri = jianceri;
    }

    public String getYangpinxin() {
        return yangpinxin;
    }

    public void setYangpinxin(String yangpinxin) {
        this.yangpinxin = yangpinxin;
    }

    public String getJishuming() {
        return jishuming;
    }

    public void setJishuming(String jishuming) {
        this.jishuming = jishuming;
    }

    public String getShuyu() {
        return shuyu;
    }

    public void setShuyu(String shuyu) {
        this.shuyu = shuyu;
    }

    public String getFuhe() {
        return fuhe;
    }

    public void setFuhe(String fuhe) {
        this.fuhe = fuhe;
    }

    public String getUsname() {
        return usname;
    }

    public void setUsname(String usname) {
        this.usname = usname;
    }


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "createtime", nullable = false, updatable = false)
    private Date createtime;

    public Luru() {
        this.createtime = new Date();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public String getBiename() {
        return biename;
    }

    public void setBiename(String biename) {
        this.biename = biename;
    }

    public String getCasno() {
        return casno;
    }

    public void setCasno(String casno) {
        this.casno = casno;
    }

    public String getEcno() {
        return ecno;
    }

    public void setEcno(String ecno) {
        this.ecno = ecno;
    }

    public String getFenzisi() {
        return fenzisi;
    }

    public void setFenzisi(String fenzisi) {
        this.fenzisi = fenzisi;
    }

    public String getTuijianyongtu() {
        return tuijianyongtu;
    }

    public void setTuijianyongtu(String tuijianyongtu) {
        this.tuijianyongtu = tuijianyongtu;
    }

    public String getXianzhiyongtu() {
        return xianzhiyongtu;
    }

    public void setXianzhiyongtu(String xianzhiyongtu) {
        this.xianzhiyongtu = xianzhiyongtu;
    }

    public String getSqname() {
        return sqname;
    }

    public void setSqname(String sqname) {
        this.sqname = sqname;
    }

    public String getSqadd() {
        return sqadd;
    }

    public void setSqadd(String sqadd) {
        this.sqadd = sqadd;
    }

    public String getSqcode() {
        return sqcode;
    }

    public void setSqcode(String sqcode) {
        this.sqcode = sqcode;
    }

    public String getSqtel() {
        return sqtel;
    }

    public void setSqtel(String sqtel) {
        this.sqtel = sqtel;
    }

    public String getSqfax() {
        return sqfax;
    }

    public void setSqfax(String sqfax) {
        this.sqfax = sqfax;
    }

    public String getSqyouxiang() {
        return sqyouxiang;
    }

    public void setSqyouxiang(String sqyouxiang) {
        this.sqyouxiang = sqyouxiang;
    }

    public String getGysname() {
        return gysname;
    }

    public void setGysname(String gysname) {
        this.gysname = gysname;
    }

    public String getGysadd() {
        return gysadd;
    }

    public void setGysadd(String gysadd) {
        this.gysadd = gysadd;
    }

    public String getGyscode() {
        return gyscode;
    }

    public void setGyscode(String gyscode) {
        this.gyscode = gyscode;
    }

    public String getGystel() {
        return gystel;
    }

    public void setGystel(String gystel) {
        this.gystel = gystel;
    }

    public String getGysfax() {
        return gysfax;
    }

    public void setGysfax(String gysfax) {
        this.gysfax = gysfax;
    }

    public String getGysyouxiang() {
        return gysyouxiang;
    }

    public void setGysyouxiang(String gysyouxiang) {
        this.gysyouxiang = gysyouxiang;
    }

    public String getQiyetel() {
        return qiyetel;
    }

    public void setQiyetel(String qiyetel) {
        this.qiyetel = qiyetel;
    }

    public String getBianhao() {
        return bianhao;
    }

    public void setBianhao(String bianhao) {
        this.bianhao = bianhao;
    }

    public String getShuliang() {
        return shuliang;
    }

    public void setShuliang(String shuliang) {
        this.shuliang = shuliang;
    }

    public String getJiancheyiju() {
        return jiancheyiju;
    }

    public void setJiancheyiju(String jiancheyiju) {
        this.jiancheyiju = jiancheyiju;
    }

    public String getYangpingbiaoshi() {
        return yangpingbiaoshi;
    }

    public void setYangpingbiaoshi(String yangpingbiaoshi) {
        this.yangpingbiaoshi = yangpingbiaoshi;
    }


    public String getYangpingxingzhuang() {
        return yangpingxingzhuang;
    }

    public void setYangpingxingzhuang(String yangpingxingzhuang) {
        this.yangpingxingzhuang = yangpingxingzhuang;
    }

    public Date getCreatetime() {
        if (Utils.isNull(createtime)) {
            return new Date();
        } else {
            DateTime date = new DateTime(createtime, DateTimeZone.UTC);
            return date.toDate();
        }
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}
