package cn.lzh.cqhg1.Bean;


import javax.persistence.*;

@Entity
@Table(name = "test")
public class TestJson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String weixian;
    private String jijiucuoshi;
    private String xiaofangcuoshi;
    private String xielouyingji;
    private String caozhuoyucunchu;
    private String jiechukongzhi;
    private String wuhuatexing;
    private String wendingying;
    private String dulixue;
    private String shengtaixue;
    private String feiqichuzhi;
    private String yunshuxingxi;
    private String name;
    private String zhufen;
    private String usname;



    public String getUsname() {
        return usname;
    }

    public void setUsname(String usname) {
        this.usname = usname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWeixian() {
        return weixian;
    }

    public void setWeixian(String weixian) {
        this.weixian = weixian;
    }

    public String getJijiucuoshi() {
        return jijiucuoshi;
    }

    public void setJijiucuoshi(String jijiucuoshi) {
        this.jijiucuoshi = jijiucuoshi;
    }

    public String getXiaofangcuoshi() {
        return xiaofangcuoshi;
    }

    public void setXiaofangcuoshi(String xiaofangcuoshi) {
        this.xiaofangcuoshi = xiaofangcuoshi;
    }

    public String getXielouyingji() {
        return xielouyingji;
    }

    public void setXielouyingji(String xielouyingji) {
        this.xielouyingji = xielouyingji;
    }

    public String getCaozhuoyucunchu() {
        return caozhuoyucunchu;
    }

    public void setCaozhuoyucunchu(String caozhuoyucunchu) {
        this.caozhuoyucunchu = caozhuoyucunchu;
    }

    public String getJiechukongzhi() {
        return jiechukongzhi;
    }

    public void setJiechukongzhi(String jiechukongzhi) {
        this.jiechukongzhi = jiechukongzhi;
    }

    public String getWuhuatexing() {
        return wuhuatexing;
    }

    public void setWuhuatexing(String wuhuatexing) {
        this.wuhuatexing = wuhuatexing;
    }

    public String getWendingying() {
        return wendingying;
    }

    public void setWendingying(String wendingying) {
        this.wendingying = wendingying;
    }

    public String getDulixue() {
        return dulixue;
    }

    public void setDulixue(String dulixue) {
        this.dulixue = dulixue;
    }

    public String getShengtaixue() {
        return shengtaixue;
    }

    public void setShengtaixue(String shengtaixue) {
        this.shengtaixue = shengtaixue;
    }

    public String getFeiqichuzhi() {
        return feiqichuzhi;
    }

    public void setFeiqichuzhi(String feiqichuzhi) {
        this.feiqichuzhi = feiqichuzhi;
    }

    public String getYunshuxingxi() {
        return yunshuxingxi;
    }

    public void setYunshuxingxi(String yunshuxingxi) {
        this.yunshuxingxi = yunshuxingxi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZhufen() {
        return zhufen;
    }

    public void setZhufen(String zhufen) {
        this.zhufen = zhufen;
    }



}
