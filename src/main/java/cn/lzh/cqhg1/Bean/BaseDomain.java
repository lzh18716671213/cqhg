package cn.lzh.cqhg1.Bean;

import cn.lzh.cqhg1.utils.Utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;


@MappedSuperclass
public abstract class BaseDomain implements BaseDomainInterface {
    private static final long serialVersionUID = 4302749702280530401L;

    /**
     * 通用状态
     */
    public enum Status {
        /**
         * 有效数据
         */
        AVAILABLE(1),

        /**
         * 无效数据
         */
        UNAVAILABLE(0),

        /**
         * 已删除数据
         */
        DELETED(-1);

        private Integer status;

        Status(Integer status) {
            this.status = status;
        }

        public Integer getStatus() {
            return this.status;
        }
    }

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    protected Long id;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "createtime", nullable = false, updatable = false)
    protected Date createtime;
//    /**
//     * 更新时间
//     */
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    @Column(name = "update_time", nullable = false)
//    protected Date updateTime;
    /**
     * 状态：-1.已删除；0.无效；1.有效
     */
    @Column(nullable = false)
    protected int status = 1;

    public BaseDomain() {
        this.createtime = new Date();
        //this.updateTime = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getCreateTime() {
        if (Utils.isNull(createtime)) {
            return new Date();
        } else {
            DateTime date = new DateTime(createtime, DateTimeZone.UTC);
            return date.toDate();
        }
    }

    public void setCreateTime(Date createTime) {
        this.createtime = createTime;
    }

//    public Date getUpdateTime() {
//        DateTime date = new DateTime(updateTime, DateTimeZone.UTC);
//        return date.toDate();
//    }
//
//    public void setUpdateTime(Date updateTime) {
//        this.updateTime = updateTime;
//    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public Map<String, Object> toMap() {
        return Utils.toMap(this, null, null);
    }

    @Override
    public Map<String, Object> toMapWithInclude(String[] fields) {
        return Utils.toMap(this, fields, null);
    }

    @Override
    public Map<String, Object> toMapWithExclude(String[] fields) {
        return Utils.toMap(this, null, fields);
    }

    /**
     * 取得实体对象对应的表名
     *
     * @return
     */
    public String getTableName() {
        Table ann = this.getClass().getAnnotation(Table.class);
        if (null != ann) {
            if (!Utils.isEmpty(ann.name())) {
                return ann.name();
            }
        }
        return "";
    }

    /**
     * 取得实体类名
     *
     * @return
     */
    public String getClsName() {
        return this.getClass().getSimpleName();
    }


}
