package cn.lzh.cqhg1.Bean;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "denglulog")
public class DLlog extends BaseDomain {
    private String username;
    private String miaoshu;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMiaoshu() {
        return miaoshu;
    }

    public void setMiaoshu(String miaoshu) {
        this.miaoshu = miaoshu;
    }
}
