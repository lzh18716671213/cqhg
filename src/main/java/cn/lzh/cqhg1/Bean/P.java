package cn.lzh.cqhg1.Bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "p_copy")
public class P {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String pcode;
    private String yufangcuoshishuoming;
    private String weixianzhonglei;
    private String weixianleibie;
    private String shiyongtiaojian;

    @ManyToMany(mappedBy = "ssyufangcuoshiP")
    @JsonIgnore
    private List<H> ssyufangcuoshiH= new ArrayList<>();

    @ManyToMany(mappedBy = "ssyingduicuoshiP")
    @JsonIgnore
    private List<H> ssyingduicuoshiH= new ArrayList<>();

    @ManyToMany(mappedBy = "sscunfangP")
    @JsonIgnore
    private List<H> sscunfangH= new ArrayList<>();

    @ManyToMany(mappedBy = "sschuzhiP")
    @JsonIgnore
    private List<H> sschuzhiH= new ArrayList<>();

    public List<H> getSsyufangcuoshiH() {
        return ssyufangcuoshiH;
    }

    public void setSsyufangcuoshiH(List<H> ssyufangcuoshiH) {
        this.ssyufangcuoshiH = ssyufangcuoshiH;
    }


    public List<H> getSsyingduicuoshiH() {
        return ssyingduicuoshiH;
    }

    public void setSsyingduicuoshiH(List<H> ssyingduicuoshiH) {
        this.ssyingduicuoshiH = ssyingduicuoshiH;
    }

    public List<H> getSscunfangH() {
        return sscunfangH;
    }

    public void setSscunfangH(List<H> sscunfangH) {
        this.sscunfangH = sscunfangH;
    }

    public List<H> getSschuzhiH() {
        return sschuzhiH;
    }

    public void setSschuzhiH(List<H> sschuzhiH) {
        this.sschuzhiH = sschuzhiH;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getYufangcuoshishuoming() {
        return yufangcuoshishuoming;
    }

    public void setYufangcuoshishuoming(String yufangcuoshishuoming) {
        this.yufangcuoshishuoming = yufangcuoshishuoming;
    }

    public String getWeixianzhonglei() {
        return weixianzhonglei;
    }

    public void setWeixianzhonglei(String weixianzhonglei) {
        this.weixianzhonglei = weixianzhonglei;
    }

    public String getWeixianleibie() {
        return weixianleibie;
    }

    public void setWeixianleibie(String weixianleibie) {
        this.weixianleibie = weixianleibie;
    }

    public String getShiyongtiaojian() {
        return shiyongtiaojian;
    }

    public void setShiyongtiaojian(String shiyongtiaojian) {
        this.shiyongtiaojian = shiyongtiaojian;
    }

}
