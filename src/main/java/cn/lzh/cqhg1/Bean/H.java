package cn.lzh.cqhg1.Bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "h_copy")
public class H {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String hcode;
    private String leibie;
    private String fenlei;
    private String qqxiangxintu;
    private String xinhaoci;
    private String shuoming;
    private String lhgxiangxintu;


    @ManyToMany(targetEntity = P.class)
    @JsonIgnore
    @JoinTable(name="ssyufangcuoshi",
            joinColumns = {@JoinColumn(name="hid",referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name="pid",referencedColumnName = "id")}
    )

    private List<P> ssyufangcuoshiP = new ArrayList<>();

    public List<P> getSsyufangcuoshiP() {
        return ssyufangcuoshiP;
    }

    public void setSsyufangcuoshiP(List<P> ssyufangcuoshiP) {
        this.ssyufangcuoshiP = ssyufangcuoshiP;
    }

    @ManyToMany(targetEntity = P.class)
    @JsonIgnore
    @JoinTable(name="ssyingduicuoshi",
            joinColumns = {@JoinColumn(name="hid",referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name="pid",referencedColumnName = "id")}
    )
    private List<P> ssyingduicuoshiP = new ArrayList<>();

    @ManyToMany(targetEntity = P.class)
    @JsonIgnore
    @JoinTable(name="sscunfang",
            joinColumns = {@JoinColumn(name="hid",referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name="pid",referencedColumnName = "id")}
    )
    private List<P> sscunfangP = new ArrayList<>();

    public List<P> getSscunfangP() {
        return sscunfangP;
    }

    public void setSscunfangP(List<P> sscunfangP) {
        this.sscunfangP = sscunfangP;
    }

    @ManyToMany(targetEntity = P.class)
    @JsonIgnore
    @JoinTable(name="sschuzhi",
            joinColumns = {@JoinColumn(name="hid",referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name="pid",referencedColumnName = "id")}
    )
    private List<P> sschuzhiP = new ArrayList<>();


    public List<P> getSschuzhiP() {
        return sschuzhiP;
    }

    public void setSschuzhiP(List<P> sschuzhiP) {
        this.sschuzhiP = sschuzhiP;
    }

    public List<P> getSsyingduicuoshiP() {
        return ssyingduicuoshiP;
    }

    public void setSsyingduicuoshiP(List<P> ssyingduicuoshiP) {
        this.ssyingduicuoshiP = ssyingduicuoshiP;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHcode() {
        return hcode;
    }

    public void setHcode(String hcode) {
        this.hcode = hcode;
    }

    public String getFenlei() {
        return fenlei;
    }

    public void setFenlei(String fenlei) {
        this.fenlei = fenlei;
    }

    public String getLeibie() {
        return leibie;
    }

    public void setLeibie(String leibie) {
        this.leibie = leibie;
    }

    public String getQqxiangxintu() {
        return qqxiangxintu;
    }

    public void setQqxiangxintu(String qqxiangxintu) {
        this.qqxiangxintu = qqxiangxintu;
    }

    public String getLhgxiangxintu() {
        return lhgxiangxintu;
    }

    public void setLhgxiangxintu(String lhgxiangxintu) {
        this.lhgxiangxintu = lhgxiangxintu;
    }

    public String getXinhaoci() {
        return xinhaoci;
    }

    public void setXinhaoci(String xinhaoci) {
        this.xinhaoci = xinhaoci;
    }

    public String getShuoming() {
        return shuoming;
    }

    public void setShuoming(String shuoming) {
        this.shuoming = shuoming;
    }


}
