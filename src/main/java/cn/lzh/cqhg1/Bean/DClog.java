package cn.lzh.cqhg1.Bean;

import cn.lzh.cqhg1.utils.Utils;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "daochulog")
public class DClog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createtime;

    public DClog(){
        this.createtime = new Date();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getCreateTime() {
        if (Utils.isNull(createtime)) {
            return new Date();
        } else {
            DateTime date = new DateTime(createtime, DateTimeZone.UTC);
            return date.toDate();
        }
    }
    public void setCreateTime(Date createTime) {
        this.createtime = createTime;
    }
    private String username;
    private String miaosu;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMiaosu() {
        return miaosu;
    }

    public void setMiaosu(String miaosu) {
        this.miaosu = miaosu;
    }
}
