package cn.lzh.cqhg1.Bean;

import javax.persistence.*;

@Entity
@Table(name = "jianchezhongxin")
public class TestCenter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String jcname;
    private String jctel;
    private String jcfax;
    private String jccode;
    private String jcadd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJcname() {
        return jcname;
    }

    public void setJcname(String jcname) {
        this.jcname = jcname;
    }

    public String getJctel() {
        return jctel;
    }

    public void setJctel(String jctel) {
        this.jctel = jctel;
    }

    public String getJcfax() {
        return jcfax;
    }

    public void setJcfax(String jcfax) {
        this.jcfax = jcfax;
    }

    public String getJccode() {
        return jccode;
    }

    public void setJccode(String jccode) {
        this.jccode = jccode;
    }

    public String getJcadd() {
        return jcadd;
    }

    public void setJcadd(String jcadd) {
        this.jcadd = jcadd;
    }
}
