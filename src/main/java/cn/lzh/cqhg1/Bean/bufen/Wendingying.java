package cn.lzh.cqhg1.Bean.bufen;


public class Wendingying {

    private String fanyingxing  ;
    private String huaxuewen  ;
    private String weixianfan  ;
    private String yingbimian  ;
    private String buxiangrong  ;
    private String weixiande  ;


    public String getFanyingxing() {
        return fanyingxing;
    }

    public void setFanyingxing(String fanyingxing) {
        this.fanyingxing = fanyingxing;
    }

    public String getHuaxuewen() {
        return huaxuewen;
    }

    public void setHuaxuewen(String huaxuewen) {
        this.huaxuewen = huaxuewen;
    }

    public String getWeixianfan() {
        return weixianfan;
    }

    public void setWeixianfan(String weixianfan) {
        this.weixianfan = weixianfan;
    }

    public String getYingbimian() {
        return yingbimian;
    }

    public void setYingbimian(String yingbimian) {
        this.yingbimian = yingbimian;
    }

    public String getBuxiangrong() {
        return buxiangrong;
    }

    public void setBuxiangrong(String buxiangrong) {
        this.buxiangrong = buxiangrong;
    }

    public String getWeixiande() {
        return weixiande;
    }

    public void setWeixiande(String weixiande) {
        this.weixiande = weixiande;
    }
}
