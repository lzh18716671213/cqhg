package cn.lzh.cqhg1.Bean.bufen;


public class Zhiaixing {
    private String name;
    private String casno;
    private String iarc;
    private String ntp;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCasno() {
        return casno;
    }

    public void setCasno(String casno) {
        this.casno = casno;
    }

    public String getIarc() {
        return iarc;
    }

    public void setIarc(String iarc) {
        this.iarc = iarc;
    }

    public String getNtp() {
        return ntp;
    }

    public void setNtp(String ntp) {
        this.ntp = ntp;
    }
}
