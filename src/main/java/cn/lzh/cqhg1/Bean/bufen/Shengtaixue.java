package cn.lzh.cqhg1.Bean.bufen;


public class Shengtaixue {

    private String jixingshui ;
    private String manxingshui ;
    private String chijiuxing ;
    private String shengwufu ;
    private String turangzhong ;
    private String pbt ;



    public String getJixingshui() {
        return jixingshui;
    }

    public void setJixingshui(String jixingshui) {
        this.jixingshui = jixingshui;
    }

    public String getManxingshui() {
        return manxingshui;
    }

    public void setManxingshui(String manxingshui) {
        this.manxingshui = manxingshui;
    }

    public String getChijiuxing() {
        return chijiuxing;
    }

    public void setChijiuxing(String chijiuxing) {
        this.chijiuxing = chijiuxing;
    }

    public String getShengwufu() {
        return shengwufu;
    }

    public void setShengwufu(String shengwufu) {
        this.shengwufu = shengwufu;
    }

    public String getTurangzhong() {
        return turangzhong;
    }

    public void setTurangzhong(String turangzhong) {
        this.turangzhong = turangzhong;
    }

    public String getPbt() {
        return pbt;
    }

    public void setPbt(String pbt) {
        this.pbt = pbt;
    }
}
