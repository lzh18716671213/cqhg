package cn.lzh.cqhg1.Bean.bufen;


public class Dulixue {

    private String jixingdu ;
    private String pifufu ;
    private String yanzhongyan ;
    private String pihuzhimin ;
    private String huxizhimin ;
    private String shengzhixi ;
    private String zhiaixing ;
    private String shengzhidu ;
    private String fujiawei ;
    private String dancijie ;
    private String fanfujie ;
    private String xiruwei;



    public String getJixingdu() {
        return jixingdu;
    }

    public void setJixingdu(String jixingdu) {
        this.jixingdu = jixingdu;
    }

    public String getPifufu() {
        return pifufu;
    }

    public void setPifufu(String pifufu) {
        this.pifufu = pifufu;
    }

    public String getYanzhongyan() {
        return yanzhongyan;
    }

    public void setYanzhongyan(String yanzhongyan) {
        this.yanzhongyan = yanzhongyan;
    }

    public String getPihuzhimin() {
        return pihuzhimin;
    }

    public void setPihuzhimin(String pihuzhimin) {
        this.pihuzhimin = pihuzhimin;
    }

    public String getHuxizhimin() {
        return huxizhimin;
    }

    public void setHuxizhimin(String huxizhimin) {
        this.huxizhimin = huxizhimin;
    }

    public String getShengzhixi() {
        return shengzhixi;
    }

    public void setShengzhixi(String shengzhixi) {
        this.shengzhixi = shengzhixi;
    }

    public String getZhiaixing() {
        return zhiaixing;
    }

    public void setZhiaixing(String zhiaixing) {
        this.zhiaixing = zhiaixing;
    }

    public String getShengzhidu() {
        return shengzhidu;
    }

    public void setShengzhidu(String shengzhidu) {
        this.shengzhidu = shengzhidu;
    }

    public String getFujiawei() {
        return fujiawei;
    }

    public void setFujiawei(String fujiawei) {
        this.fujiawei = fujiawei;
    }

    public String getDancijie() {
        return dancijie;
    }

    public void setDancijie(String dancijie) {
        this.dancijie = dancijie;
    }

    public String getFanfujie() {
        return fanfujie;
    }

    public void setFanfujie(String fanfujie) {
        this.fanfujie = fanfujie;
    }

    public String getXiruwei() {
        return xiruwei;
    }

    public void setXiruwei(String xiruwei) {
        this.xiruwei = xiruwei;
    }
}
