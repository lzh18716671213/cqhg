package cn.lzh.cqhg1.Bean.bufen;


public class Xiaofangcuoshi {

    private String heshi ;
    private String buheshi ;
    private String yuanyuci ;
    private String duixiaofang ;

    public String getHeshi() {
        return heshi;
    }

    public void setHeshi(String heshi) {
        this.heshi = heshi;
    }

    public String getBuheshi() {
        return buheshi;
    }

    public void setBuheshi(String buheshi) {
        this.buheshi = buheshi;
    }

    public String getYuanyuci() {
        return yuanyuci;
    }

    public void setYuanyuci(String yuanyuci) {
        this.yuanyuci = yuanyuci;
    }

    public String getDuixiaofang() {
        return duixiaofang;
    }

    public void setDuixiaofang(String duixiaofang) {
        this.duixiaofang = duixiaofang;
    }
}
