package cn.lzh.cqhg1.Bean.bufen;


public class Weixian {

    private String jinjiqing;
    private String wulihe;
    private String wxiru;
    private String wshiru;
    private String wpifujie;
    private String yanjing;
    private String huanjinwei;


    public String getJinjiqing() {
        return jinjiqing;
    }

    public void setJinjiqing(String jinjiqing) {
        this.jinjiqing = jinjiqing;
    }

    public String getWulihe() {
        return wulihe;
    }

    public void setWulihe(String wulihe) {
        this.wulihe = wulihe;
    }

    public String getWxiru() {
        return wxiru;
    }

    public void setWxiru(String wxiru) {
        this.wxiru = wxiru;
    }

    public String getWshiru() {
        return wshiru;
    }

    public void setWshiru(String wshiru) {
        this.wshiru = wshiru;
    }

    public String getWpifujie() {
        return wpifujie;
    }

    public void setWpifujie(String wpifujie) {
        this.wpifujie = wpifujie;
    }

    public String getYanjing() {
        return yanjing;
    }

    public void setYanjing(String yanjing) {
        this.yanjing = yanjing;
    }

    public String getHuanjinwei() {
        return huanjinwei;
    }

    public void setHuanjinwei(String huanjinwei) {
        this.huanjinwei = huanjinwei;
    }
}
