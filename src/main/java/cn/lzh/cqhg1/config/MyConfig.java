package cn.lzh.cqhg1.config;


import cn.lzh.cqhg1.component.LoginHandlerInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

//扩展springmvc的功能

@Configuration
public class MyConfig implements WebMvcConfigurer {
    @Autowired
    private LoginHandlerInterceptor loginHandlerInterceptor;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
          registry.addViewController("/").setViewName("public/login");

    }

    //注册拦截器 测试发get请求时 注释 发布打开注释
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
       registry.addInterceptor(loginHandlerInterceptor).addPathPatterns("/**").excludePathPatterns("/","/static/**","/userlogin");
    }
}
