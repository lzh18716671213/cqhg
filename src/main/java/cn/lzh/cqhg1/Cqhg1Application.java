package cn.lzh.cqhg1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
@SpringBootApplication
@EnableTransactionManagement // 开启事务
public class Cqhg1Application {

    public static void main(String[] args) {

        SpringApplication.run(Cqhg1Application.class, args);
    }


}
