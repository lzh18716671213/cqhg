layui.define(['layer','jquery'],function (exports) {
   var layer = layui.layer;
   var $ = layui.$;
    function window(){
        layer.open({
            type: 1,
            content:$("#search-info"),
            area: ['80%', '80%'],  //大小
            shift: 1,// 动画
            maxmin: true, //最大最小化按钮
            title: ['搜索详情', 'font-size:22px;']
        });
    }
    exports('customWindows',window);
});