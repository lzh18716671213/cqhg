layui.define(['jquery','layer','form','element','customAjax','setTpl','bar'],function (exports) {
    var  form = layui.form,
        layer = layui.layer,
        $ = layui.$,
        element = layui.element,
        customAjax = layui.customAjax,
        setTpl = layui.setTpl,
        bar = layui.bar;
        bar();



    //请求数据
    getAllData();
    function getAllData() {
        //化学品管理
        var d = {
            name:localStorage.getItem('name')
        };
        customAjax.http('/guangli',d,function (msg) {
            if (msg.code === 0){
                localStorage.setItem('hxpgl',JSON.stringify(msg.data));
                show();
            } else {
                layer.msg("服务器繁忙，请稍后重试！",{
                    icon:5
                })
            }
        });
    }


        function show() {
            //所有数据
            var allData = JSON.parse(localStorage.getItem('hxpgl'));

            //危险说明编码
            ((function () {
                $(".HcodeEdit").click(function () {
                    if (allData.weixianbiaoshi) {
                        layer.msg("已有相应数据请勿重复添加！",{
                            icon:5
                        });
                        return;
                    }
                    openView($("#insert-H"),'危险说明编码');
                });
                $(".formDemo-H-add").click(function () {
                    var index = $(this).parents("#addInsertH").children("#H-content").children().length;
                    $("#H-content").append(createElement(index+1));
                    form.render(); // 必须添加
                });
                function createElement(index){
                    var item = $(" <div class=\"layui-form-item\">\n" +
                        "                                <label class=\"layui-form-label\"><span class=\"layui-icon layui-icon-close close-btn\" title=\"删除一条\"></span>编号"+index+"</label>\n" +
                        "                                <div class=\"layui-input-block\">\n" +
                        "                                    <select name=\"H[]\" lay-search lay-verify=\"required\">\n" +
                        "                                        <option value=\"\">请搜索危险性说明编号</option>\n" +
                        "                                        <option value=\"1\">H200</option>\n" +
                        "                                        <option value=\"2\">H201</option>\n" +
                        "                                        <option value=\"3\">H202</option>\n" +
                        "                                        <option value=\"4\">H203</option>\n" +
                        "                                        <option value=\"5\">H204</option>\n" +
                        "                                        <option value=\"6\">H205</option>\n" +
                        "                                        <option value=\"7\">无(类别：爆炸物,危险分类：第1.6项)</option>\n" +
                        "                                        <option value=\"8\">H220</option>\n" +
                        "                                        <option value=\"9\">H220H232</option>\n" +
                        "                                        <option value=\"10\">H220H230</option>\n" +
                        "                                        <option value=\"11\">H220H231</option>\n" +
                        "                                        <option value=\"12\">H221(危险分类：1B,信号词：危险)</option>\n" +
                        "                                        <option value=\"13\">H221(危险分类：2,信号词：警告)</option>\n" +
                        "                                        <option value=\"14\">H222H229</option>\n" +
                        "                                        <option value=\"15\">H223H229</option>\n" +
                        "                                        <option value=\"16\">H229</option>\n" +
                        "                                        <option value=\"17\">H270</option>\n" +
                        "                                        <option value=\"18\">H280(危险分类：压缩气体)</option>\n" +
                        "                                        <option value=\"19\">H280(危险分类：液化气体)</option>\n" +
                        "                                        <option value=\"20\">H281</option>\n" +
                        "                                        <option value=\"21\">H280(危险分类：溶解气体)</option>\n" +
                        "                                        <option value=\"22\">H224</option>\n" +
                        "                                        <option value=\"23\">H225</option>\n" +
                        "                                        <option value=\"24\">H226</option>\n" +
                        "                                        <option value=\"25\">H227</option>\n" +
                        "                                        <option value=\"26\">H228(危险分类：1,信号词：危险)</option>\n" +
                        "                                        <option value=\"27\">H228(危险分类：2,信号词：警告)</option>\n" +
                        "                                        <option value=\"28\">H240(危险类别：自反应物质和混合物)</option>\n" +
                        "                                        <option value=\"29\">H241(危险类别：自反应物质和混合物)</option>\n" +
                        "                                        <option value=\"30\">H242(危险类别：自反应物质和混合物,危险分类：C型和D型,信号词：危险)</option>\n" +
                        "                                        <option value=\"31\">H242(危险类别：自反应物质和混合物,危险分类：E型和F型,信号词：警告)</option>\n" +
                        "                                        <option value=\"32\">无(危险类别：自反应物质和混合物,危险分类：G型)</option>\n" +
                        "                                        <option value=\"33\">H250(危险类别：发火液体)</option>\n" +
                        "                                        <option value=\"34\">H250(危险类别：发火固体)</option>\n" +
                        "                                        <option value=\"35\">H251</option>\n" +
                        "                                        <option value=\"36\">H252</option>\n" +
                        "                                        <option value=\"37\">H260</option>\n" +
                        "                                        <option value=\"38\">H261(危险分类：2,信号词：危险)</option>\n" +
                        "                                        <option value=\"39\">H261(危险分类：3,信号词：警告)</option>\n" +
                        "                                        <option value=\"40\">H271(危险类别：氧化性液体)</option>\n" +
                        "                                        <option value=\"41\">H272(危险分类：2,信号词：危险)</option>\n" +
                        "                                        <option value=\"42\">H272(危险分类：3,信号词：警告)</option>\n" +
                        "                                        <option value=\"43\">H271(危险类别：氧化性固体)</option>\n" +
                        "                                        <option value=\"44\">H272(危险分类：2,信号词：危险)</option>\n" +
                        "                                        <option value=\"45\">H272(危险分类：3,信号词：警告)</option>\n" +
                        "                                        <option value=\"46\">H240(危险类别：有机过氧化物)</option>\n" +
                        "                                        <option value=\"47\">H241(危险类别：有机过氧化物)</option>\n" +
                        "                                        <option value=\"48\">H242(危险类别：有机过氧化物,危险分类：C型和D型,信号词：危险)</option>\n" +
                        "                                        <option value=\"49\">H242(危险类别：有机过氧化物,危险分类：E型和F型,信号词：警告)</option>\n" +
                        "                                        <option value=\"50\">无(危险类别：有机过氧化物,危险分类：G型)</option>\n" +
                        "                                        <option value=\"51\">H290</option>\n" +
                        "                                        <option value=\"52\">H206</option>\n" +
                        "                                        <option value=\"53\">H207(危险分类：2,信号词：危险)</option>\n" +
                        "                                        <option value=\"54\">H207(危险分类：3,信号词：警告)</option>\n" +
                        "                                        <option value=\"55\">H208</option>\n" +
                        "                                        <option value=\"56\">H300+H310</option>\n" +
                        "                                        <option value=\"57\">H300+H330</option>\n" +
                        "                                        <option value=\"58\">H310+H330</option>\n" +
                        "                                        <option value=\"59\">H300+H310+H330</option>\n" +
                        "                                        <option value=\"60\">H301+H311</option>\n" +
                        "                                        <option value=\"61\">H301+H331</option>\n" +
                        "                                        <option value=\"62\">H311+H331</option>\n" +
                        "                                        <option value=\"63\">H301+H311+H331</option>\n" +
                        "                                        <option value=\"64\">H302+H312</option>\n" +
                        "                                        <option value=\"65\">H302+H332</option>\n" +
                        "                                        <option value=\"66\">H312+H332</option>\n" +
                        "                                        <option value=\"67\">H302+H312+H332</option>\n" +
                        "                                        <option value=\"68\">H303+H313</option>\n" +
                        "                                        <option value=\"69\">H303+H333</option>\n" +
                        "                                        <option value=\"70\">H313+H333</option>\n" +
                        "                                        <option value=\"71\">H303+H313+H333</option>\n" +
                        "                                        <option value=\"72\">H315+H320</option>\n" +
                        "                                        <option value=\"73\">H314</option>\n" +
                        "                                        <option value=\"74\">H315</option>\n" +
                        "                                        <option value=\"75\">H316</option>\n" +
                        "                                        <option value=\"76\">H318</option>\n" +
                        "                                        <option value=\"77\">H319</option>\n" +
                        "                                        <option value=\"78\">H320</option>\n" +
                        "                                        <option value=\"79\">H334(危险分类：1)</option>\n" +
                        "                                        <option value=\"80\">H334(危险分类：1Aa)</option>\n" +
                        "                                        <option value=\"81\">H334(危险分类：1Ba)</option>\n" +
                        "                                        <option value=\"82\">H317(危险分类：1)</option>\n" +
                        "                                        <option value=\"83\">H317(危险分类：1Aa)</option>\n" +
                        "                                        <option value=\"84\">H317(危险分类：1Ba)</option>\n" +
                        "                                        <option value=\"85\">H340</option>\n" +
                        "                                        <option value=\"86\">H341</option>\n" +
                        "                                        <option value=\"87\">H350</option>\n" +
                        "                                        <option value=\"88\">H351</option>\n" +
                        "                                        <option value=\"89\">H360</option>\n" +
                        "                                        <option value=\"90\">H361</option>\n" +
                        "                                        <option value=\"91\">H362</option>\n" +
                        "                                        <option value=\"92\">H370</option>\n" +
                        "                                        <option value=\"93\">H371</option>\n" +
                        "                                        <option value=\"94\">H335H336</option>\n" +
                        "                                        <option value=\"95\">H372</option>\n" +
                        "                                        <option value=\"96\">H373</option>\n" +
                        "                                        <option value=\"97\">H304</option>\n" +
                        "                                        <option value=\"98\">H305</option>\n" +
                        "                                        <option value=\"99\">H400</option>\n" +
                        "                                        <option value=\"100\">H401</option>\n" +
                        "                                        <option value=\"101\">H402</option>\n" +
                        "                                        <option value=\"102\">H410</option>\n" +
                        "                                        <option value=\"103\">H411</option>\n" +
                        "                                        <option value=\"104\">H412</option>\n" +
                        "                                        <option value=\"105\">H413</option>\n" +
                        "                                        <option value=\"106\">H420</option>\n" +
                        "                                    </select>\n" +
                        "                                </div>\n" +
                        "                            </div>");
                    return item;
                }
                //删除
                $("#insert-H").delegate(".close-btn","click",function () {
                    $(this).parents(".layui-form-item").remove();
                });
                form.on('submit(formDemo-H)', function (data) {
                    if (data.field) {
                        var d = {};
                        var ds = [];
                        for(k in data.field){
                            ds.push(data.field[k]);
                        }
                        d["data"] = ds;
                        var index = layer.load(0);
                        $.ajax({
                            url:'/H1',
                            type:"post",
                            dataType:"json",
                            contentType: "application/json; charset=utf-8",
                            data:JSON.stringify(d),
                            success:function (msg) {
                                if (msg.code === 0){
                                    allData.weixianbiaoshi = {};
                                    allData.weixianbiaoshi = msg.data[0];
                                    allData.weixianbiaoshi.userInsert = {};
                                    wxbs(); //危险标识
                                }else {
                                    layer.msg("数据接收失败！,请稍后重试",{
                                        icon:2
                                    });
                                }
                            },
                            error:function (err) {
                                layer.msg("服务器异常,请稍后重试");
                            }
                        });
                        layer.close(index);
                        layer.closeAll('page');
                    }
                    return false;
                });
            })());

            //危险标识
            wxbs();
            function wxbs() {
                var wxb = allData.weixianbiaoshi;
                if (wxb){
                    //格式化GHS标签要素
                    var wxbD = {
                        "imgs":[],
                        "xhc":[]
                    };
                    var cons = wxb.GHSbiaoqianyaoshu;
                    for (key in cons){
                        if (key.trim().lastIndexOf('象形图') !== -1){
                            if (cons[key].trim().lastIndexOf('\\') !== -1){
                                var d = cons[key].trim().split('\\');
                                for (var i=0;i<d.length;i++){
                                    if (wxbD.imgs.length >0){
                                        var temp = [];
                                        for (var j=0;j<wxbD.imgs.length;j++){
                                            temp[wxbD.imgs[j]] = true;
                                        }
                                        if (!temp[d[i]]){
                                            wxbD.imgs.push(d[i]);
                                        }
                                    } else {
                                        wxbD.imgs.push(d[i]);
                                    }
                                }
                            }else if (cons[key].trim().lastIndexOf('\\') === -1 && cons[key].trim().lastIndexOf('/img') !== -1) {
                                if (wxbD.imgs.length >0){
                                    var temp = [];
                                    for (var j=0;j<wxbD.imgs.length;j++){
                                        temp[wxbD.imgs[j]] = true;
                                    }
                                    if (!temp[cons[key].trim()]){
                                        wxbD.imgs.push(cons[key].trim());
                                    }
                                } else {
                                    wxbD.imgs.push(cons[key].trim());
                                }
                            }
                        } else {  // 信号词
                            wxbD.xhc.push(cons[key].trim());
                        }
                    }
                    wxb.wxbD = wxbD;
                    if (!$.isEmptyObject(wxb.userInsert)){ //用户有输入再渲染
                        setTpl.oneTpl(wxb,$("#weixianbiao-tpl"),$("#weixianbiao-view"));
                    }
                }
                $(".weixianbiaoEdit").click(function () {
                    if (wxb){
                        if (!$.isEmptyObject(wxb.userInsert)){
                            var D = wxb.userInsert;
                            form.val("weixianbiao", {
                                "jinjiqing": D.jinjiqing,
                                "wulihe":D.wulihe,
                                "wxiru":D.wxiru,
                                "wshiru":D.wshiru,
                                "wpifujie":D.wpifujie,
                                "yanjing":D.yanjing,
                                "huanjinwei":D.huanjinwei
                            });
                        }
                        openView($("#weixianbiao-Edit"),'危险标识');
                    } else {
                        layer.msg("您还没有输入危险说明编码，请在顶部点击按钮添加！",{
                            icon:5
                        });
                    }
                });
                form.on('submit(weixianbiao)',function (data) {
                    layer.confirm("确认修改吗？",{
                        yes:function (index) {
                            isNull(data.field);
                            wxb.userInsert = data.field;
                            $.ajax({
                                url:'/weixian',
                                type:"post",
                                dataType:"json",
                                contentType: "application/json; charset=utf-8",
                                data:JSON.stringify(wxb),
                                success:function (msg) {
                                    if (msg.code === 0){
                                        layer.msg("修改成功",{
                                            icon: 1,
                                            time: 500,
                                            end:function () {
                                                setTpl.oneTpl(wxb,$("#weixianbiao-tpl"),$("#weixianbiao-view"));
                                            }
                                        })
                                    }else {
                                        layer.msg("修改失败,请稍后重试",{
                                            icon:2
                                        });
                                    }
                                },
                                error:function () {
                                    layer.msg("服务器异常,请稍后重试",{
                                        icon:2
                                    });
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            }

            //组成/成分
            ((function () {
                var zf = allData.zufen;
                if (zf){
                    setTpl.oneTpl(zf,$("#chengfenzu-tpl"),$("#chengfenzu-view"));
                }else {
                    zf = {};
                }
                $(".chengfenzuEdit").click(function () {
                    setTpl.oneTpl(zf,$("#chengfenzu-tpl-edit"),$("#chengfenzu-Edit-view"));
                    openView($("#chengfenzu-Edit-view"),'组成 / 成分信息');
                });
                //继续添加按钮
                $("#chengfenzu-Edit-view").delegate(".addItem","click",function () {
                    $("#tbody").append(createElement());
                });
                function createElement(){
                    var item = $(" <tr>\n" +
                        "                                        <td><input type=\"text\" name=\"zufen[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
                        "                                        <td><input type=\"text\" name=\"hanl[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
                        "                                        <td><input type=\"text\" name=\"CAS[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
                        "                                        <td><input type=\"text\" name=\"EC[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
                        "                                        <td><button type=\"button\" class=\"layui-btn  layui-btn-xs layui-btn-danger chengfenzu-del\" title=\"删除当前一条\">删除</button></td>\n" +
                        "                                    </tr>");
                    return item;
                }
                //删除
                $("#chengfenzu-Edit-view").delegate(".chengfenzu-del",'click',function () {
                    $(this).parents("tr").remove();
                });

                form.on("submit(chengfenzu)",function (data) {
                    layer.confirm("确认添加吗？",{
                        yes:function (index) {
                            var cfd = {
                                "zufen":[],
                                "hanl":[],
                                "CAS":[],
                                "EC":[],
                                "n":[]
                            };
                            for (key in data.field){
                                if (key.lastIndexOf("zufen") !== -1){
                                    cfd.zufen.push(data.field[key]);
                                }
                                if (key.lastIndexOf("hanl") !== -1) {
                                    cfd.hanl.push(data.field[key]);
                                }
                                if (key.lastIndexOf("CAS") !== -1){
                                    cfd.CAS.push(data.field[key]);
                                }
                                if (key.lastIndexOf("EC") !== -1){
                                    cfd.EC.push(data.field[key]);
                                }
                            }
                            cfd.n.push(cfd.zufen.length);  //法规个数
                            $.ajax({
                                url:'/chengFen',
                                type:"post",
                                dataType:"json",
                                contentType: "application/json; charset=utf-8",
                                data:JSON.stringify(cfd),
                                success:function (msg) {
                                    if (msg.code === 0){
                                        layer.msg("添加成功",{
                                            icon: 1,
                                            time: 500,
                                            end:function () {
                                                zf.zufen = cfd.zufen;
                                                zf.hanl = cfd.hanl;
                                                zf.CAS = cfd.CAS;
                                                zf.EC = cfd.EC;
                                                zf.n = cfd.n;
                                                setTpl.oneTpl(zf,$("#chengfenzu-tpl"),$("#chengfenzu-view"));
                                                //重新获取渲染数据
                                                location.reload();
                                                getAllData();
                                            }
                                        })
                                    }else {
                                        layer.msg("修改失败,请稍后重试",{
                                            icon:2
                                        });
                                    }
                                },
                                error:function (err) {
                                    layer.msg("服务器异常,请稍后重试",{
                                        icon:2
                                    });
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });

            })());

            //急救措施
            ((function () {
                var jjc = allData.jijiucuoshi;
                if (jjc){
                    var obj = {
                        "zuizhongyao":jjc.zuizhongyao.zuizhongyao,
                        "jinjiyi":jjc.jinjiyi.jinjiyi
                    };
                    initObj(jjc,obj);
                    setTpl.oneTpl(jjc,$("#jiJiuCuoShi-tpl"),$("#jiJiuCuoShi-view"));
                }else {
                    jjc = {
                        "pifujie": {},
                        "zuizhongyao": [],
                        "suction": {},
                        "eat": {},
                        "yanjinjie": {},
                        "yibanxing": {},
                        "jinjiyi": [],
                        "jijiuren": {}
                    };
                }
                $(".jiJiuEdit").click(function () {
                    setTpl.oneTpl(jjc,$("#jiJiuCuoShi-add-edit"),$("#jiJiuCuoShi-edit-view"));
                    openView($("#jiJiuCuoShi-edit-view"),'急救措施信息');
                });
                form.on('submit(jiJiuCuoShi)',function (data) {
                    layer.confirm("确认修改吗？",{
                        yes:function (index) {
                            isNull(data.field);
                            customAjax.http('/jijiucuoshi', data.field, function (res) {
                                if (res.code === 0) {
                                    layer.msg('修改成功', {
                                        icon: 1,
                                        time: 500,
                                        end:function () {
                                            for (key in data.field){
                                                jjc[key][key] = data.field[key];
                                            }
                                            var obj = {
                                                "zuizhongyao":jjc.zuizhongyao.zuizhongyao,
                                                "jinjiyi":jjc.jinjiyi.jinjiyi
                                            };
                                            initObj(jjc,obj);
                                            setTpl.oneTpl(jjc,$("#jiJiuCuoShi-tpl"),$("#jiJiuCuoShi-view"));
                                        }
                                    })
                                }else {
                                    layer.msg("修改失败,请稍后重试",{
                                        icon:2
                                    });
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false
                });

                function cus() {

                }
            })());

            //消防措施
            ((function () {
                var xf = allData.xiaofangcuoshi;
                if (xf){
                    var obj = {
                        "yuanyuci":xf.yuanyuci.yuanyuci,
                        "duixiaofang":xf.duixiaofang.duixiaofang
                    };
                    initObj(xf,obj);
                    setTpl.oneTpl( xf,$("#xiaoFangTpl"),$("#xiaofang-view"));
                }else {
                    xf = {
                        "heshi":{},
                        "buheshi":{},
                        "yuanyuci":[],
                        "duixiaofang":[]
                    }
                }

                $(".xiaoFangEdit").click(function () {
                    setTpl.oneTpl( xf,$("#xiaoFang-add-edit"),$("#xiaofang-view-edit"));
                    openView($("#xiaofang-view-edit"),'消防措施');
                });
                form.on('submit(xiaoFang)',function (data) {
                    layer.confirm("确认添加吗？",{
                        yes:function (index) {
                            isNull(data.field);
                            customAjax.http('/xiaofangcuoshi', data.field, function (res) {
                                if (res.code === 0) {
                                    layer.msg('添加成功', {
                                        icon: 1,
                                        time: 1000,
                                        end:function () {
                                            for (key in data.field){
                                                xf[key][key] = data.field[key];
                                            }
                                            var obj = {
                                                "yuanyuci":xf.yuanyuci.yuanyuci,
                                                "duixiaofang":xf.duixiaofang.duixiaofang
                                            };
                                            initObj(xf,obj);
                                            setTpl.oneTpl(xf,$("#xiaoFangTpl"),$("#xiaofang-view"));
                                        }
                                    })
                                }else {
                                    layer.msg("修改失败,请稍后重试",{
                                        icon:2
                                    });
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            })());

            //泄露应急处理
            ((function () {
                var xl = allData.xielouyingji;
                if (xl){
                    var obj = {
                        "zuoyexiao":xl.zuoyexiao.zuoyexiao,
                        "huanjinbao":xl.huanjinbao.huanjinbao,
                        "xielouhua":xl.xielouhua.xielouhua
                    };
                    initObj(xl,obj);
                    setTpl.oneTpl(xl,$("#xielou-tpl"),$("#xielou-view"));
                }else {
                    xl = {
                        "zuoyexiao":[],
                        "huanjinbao":[],
                        "xielouhua":[]
                    }
                }
                $(".xieLouEdit").click(function () {
                    setTpl.oneTpl(xl,$("#xieLou-add-edit"),$("#xielou-view-edit"));
                    openView($("#xielou-view-edit"),'泄露应急处理');
                });
                form.on('submit(xielou)',function (data) {
                    layer.confirm("确认添加吗？",{
                        yes:function (index) {
                            isNull(data.field);
                            customAjax.http('/xielouyingji', data.field, function (res) {
                                if (res.code === 0) {
                                    layer.msg('添加成功', {
                                        icon: 1,
                                        time: 1000,
                                        end:function () {
                                            for (key in data.field){
                                                xl[key][key] = data.field[key];
                                            }
                                            var obj = {
                                                "zuoyexiao":xl.zuoyexiao.zuoyexiao,
                                                "huanjinbao":xl.huanjinbao.huanjinbao,
                                                "xielouhua":xl.xielouhua.xielouhua
                                            };
                                            initObj(xl,obj);
                                            setTpl.oneTpl(xl,$("#xielou-tpl"),$("#xielou-view"));
                                        }
                                    })
                                }else {
                                    layer.msg("修改失败，稍后重试！",{
                                        icon:2
                                    })
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            })());

            //操作与存储
            ((function () {
                var cz = allData.caozhuoyucunchu;
                if (cz){
                    var obj = {
                        "caozuozhu":cz.caozuozhu.caozuozhu,
                        "cunchuzhu":cz.cunchuzhu.cunchuzhu
                    };
                    initObj(cz,obj);
                    setTpl.oneTpl(cz,$("#caoZuo-tpl"),$("#caoZuo-view"));
                }else {
                    cz = {
                        "caozuozhu":[],
                        "cunchuzhu":[]
                    }
                }
                $(".caoZuoEdit").click(function () {
                    setTpl.oneTpl(cz,$("#caoZuo-add-edit"),$("#caoZuo-view-edit"));
                    openView($("#caoZuo-view-edit"),'操作与存储');
                });
                form.on('submit(caozuo)',function (data) {
                    layer.confirm("确认添加吗？",{
                        btn:['确定','取消'],
                        yes:function (index) {
                            isNull(data.field);
                            customAjax.http('/caozhuoyucunchu', data.field, function (res) {
                                if (res.code === 0) {
                                    layer.msg('添加成功', {
                                        icon: 1,
                                        time: 1000,
                                        end:function () {
                                            for (key in data.field){
                                                cz[key][key] = data.field[key];
                                            }
                                            var obj = {
                                                "caozuozhu":cz.caozuozhu.caozuozhu,
                                                "cunchuzhu":cz.cunchuzhu.cunchuzhu
                                            };
                                            initObj(cz,obj);
                                            setTpl.oneTpl(cz,$("#caoZuo-tpl"),$("#caoZuo-view"));
                                        }
                                    })
                                }else {
                                    layer.msg("修改失败，稍后重试！",{
                                        icon:2
                                    })
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            })());

            //接触控制与人身保护
            ((function () {

                //继续添加按钮
                $("#jiechukongzhi").delegate('.zhiye-addItem','click',function () {
                    $(".zhiye-body").append(SzhiYecreateElement());
                });
                function SzhiYecreateElement(){
                    var item = $(" <tr>\n" +
                        "                                        <td>\n" +
                        "                                            <input type=\"text\" name=\"Jzufen[]\" lay-verify=\"required\"\n" +
                        "                                                   autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                        </td>\n" +
                        "                                        <td>\n" +
                        "                                            <input type=\"text\" name=\"biaozhunlai[]\" lay-verify=\"required\"\n" +
                        "                                                   autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                        </td>\n" +
                        "                                        <td colspan=\"2\">\n" +
                        "                                            <table border=\"1\" cellspacing=\"0\" width=\"100%\">\n" +
                        "                                                <tbody>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td width=\"130px\">\n" +
                        "                                                        <input type=\"text\" name=\"leixin[]\" lay-verify=\"required\"\n" +
                        "                                                                              autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                                    </td>\n" +
                        "                                                    <td>\n" +
                        "                                                        <input type=\"text\" name=\"biaozhunzhi[]\" lay-verify=\"required\"\n" +
                        "                                                               autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                <tr>\n" +
                        "                                                    <td>\n" +
                        "                                                        <input type=\"text\" name=\"leixin[]\" lay-verify=\"required\"\n" +
                        "                                                               autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                                    </td>\n" +
                        "                                                    <td>\n" +
                        "                                                        <input type=\"text\" name=\"biaozhunzhi[]\" lay-verify=\"required\"\n" +
                        "                                                               autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                                    </td>\n" +
                        "                                                </tr>\n" +
                        "                                                </tbody>\n" +
                        "                                            </table>\n" +
                        "                                        </td>\n" +
                        "                                        <td>\n" +
                        "                                            <input type=\"text\" name=\"beizhu[]\" lay-verify=\"required\" autocomplete=\"off\" class=\"layui-input\">\n" +
                        "                                        </td>\n" +
                        "                                        <td>\n" +
                        "                                            <button type=\"button\" class=\"layui-btn layui-btn-xs layui-btn-danger zhiye-del\">删除</button>\n" +
                        "                                        </td>\n" +
                        "                                    </tr>");
                    return item;
                }
                //删除
                $("#jiechukongzhi").delegate(".zhiye-del",'click',function () {
                    var that = this;
                    layer.confirm('确定删除吗？',{
                        yes:function (i) {
                            $(that).parents("tr").remove();
                            layer.close(i);
                        }
                    })

                });

                var kz = allData.jiechukongzhi;
                if (kz){
                    var obj = {
                        "jiancefang":kz.puto.jiancefang,
                        "gongchengkong":kz.puto.gongchengkong
                    };
                    initObj(kz,obj);
                    setTpl.oneTpl(kz,$("#jiecukong-tpl"),$("#jiecukong-view"));
                }else {
                    kz = {
                        "puto": {
                            "gongchengkong": "",
                            "huxixi": "",
                            "jiancefang": "",
                            "pifuhe": "",
                            "shengwuxian": "",
                            "shoubufang": "",
                            "yanjingfang": ""
                        },
                        "zhiyejie": {
                            "Jzufen": [],
                            "beizhu": [],
                            "biaozhunlai": [],
                            "biaozhunzhi": [],
                            "leixin": []
                        }
                    }
                }
                $(".jiecukongEdit").click(function () {
                    setTpl.oneTpl(kz,$("#jiecukong-add-edit"),$("#jiecukong-view-edit"));
                    openView($("#jiecukong-view-edit"),'接触控制与人身保护');
                });
                form.on("submit(jiechukong)",function (data) {
                    layer.confirm("确认添加吗？",{
                        btn:['确定','取消'],
                        yes:function (index) {
                            kz = {
                                "puto": {
                                    "gongchengkong": "",
                                    "huxixi": "",
                                    "jiancefang": "",
                                    "pifuhe": "",
                                    "shengwuxian": "",
                                    "shoubufang": "",
                                    "yanjingfang": ""
                                },
                                "zhiyejie": {
                                    "Jzufen": [],
                                    "beizhu": [],
                                    "biaozhunlai": [],
                                    "biaozhunzhi": [],
                                    "leixin": []
                                }
                            }
                            var arrS = {};//双个类别
                            var puto = {}; //普通数据
                            for (var key in data.field){
                                if (key.lastIndexOf('[') !== -1){
                                    arrS[key] = data.field[key];
                                }else {
                                    if (data.field[key] === ""){
                                        data.field[key] = "无资料";
                                    }
                                    puto[key] = data.field[key];
                                }
                            }
                            kz.puto = puto;

                            //判断直接接触是否为空
                            if ($.isEmptyObject(arrS)){
                                kz.zhiyejie = "无资料"
                            } else {
                                for (var key in arrS){
                                    if (key.lastIndexOf("Jzufen") !== -1){
                                        kz.zhiyejie.Jzufen.push(arrS[key]);
                                    }
                                    if (key.lastIndexOf("beizhu") !== -1) {
                                        kz.zhiyejie.beizhu.push(arrS[key]);
                                    }
                                    if (key.lastIndexOf("biaozhunlai") !== -1){
                                        kz.zhiyejie.biaozhunlai.push(arrS[key]);
                                    }
                                    if (key.lastIndexOf("leixin") !== -1){
                                        kz.zhiyejie.leixin.push(arrS[key]);
                                    }
                                    if (key.lastIndexOf("biaozhunzhi") !== -1){
                                        kz.zhiyejie.biaozhunzhi.push(arrS[key]);
                                    }
                                }
                            }
                            $.ajax({
                                type:"post",
                                url:'/jiechukongzhi',
                                dataType:"json",
                                contentType: "application/json; charset=utf-8",
                                data:JSON.stringify(kz),
                                success:function (msg) {
                                    if (msg.code === 0){
                                        layer.msg("修改成功",{
                                            icon: 1,
                                            time: 1000,
                                            end:function () {
                                                var obj = {
                                                    "jiancefang":kz.puto.jiancefang,
                                                    "gongchengkong":kz.puto.gongchengkong
                                                };
                                                initObj(kz,obj);
                                                setTpl.oneTpl(kz,$("#jiecukong-tpl"),$("#jiecukong-view"));
                                            }
                                        })
                                    }else {
                                        layer.msg("修改失败，稍后重试！", {
                                            icon: 2
                                        })
                                    }
                                },
                                error:function (err) {
                                    layer.msg("服务器繁忙,请稍后重试");
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            })());

            //物理和化学特性
            ((function () {
                var wht = allData.wuhuatexing;
                if (wht){
                    setTpl.oneTpl(wht,$("#wuhuate-tpl"),$("#wuhuate-view"));
                }else {
                    wht = {
                        "ziranwen": {},
                        "waiguanyu": {},
                        "fenjiewen": {},
                        "qiwei": {},
                        "xiangduizheng": {},
                        "yundongnian": {},
                        "zhengfasu": {},
                        "zhengqiya": {},
                        "xiangduimi": {},
                        "chushifei": {},
                        "rongdianning": {},
                        "qiweifa": {},
                        "phzhi": {},
                        "zhengxincun": {},
                        "kerongxing": {},
                        "yiranxing": {},
                        "baozashang": {},
                        "shandianbi": {}
                    }
                }
                $(".wuhuateEdit").click(function () {
                    setTpl.oneTpl(wht,$("#wuhuate-edit"),$("#wuhuate-view-edit"));
                    openView($("#wuhuate-view-edit"),'物理和化学特性');
                });
                form.on('submit(wuhuate)',function (data) {
                    layer.confirm("确认添加吗？",{
                        btn:['确定','取消'],
                        yes:function (index) {
                            isNull(data.field);
                            customAjax.http('/wuhuatexing', data.field, function (res) {
                                if (res.code === 0) {
                                    layer.msg('添加成功', {
                                        icon: 1,
                                        time: 1000,
                                        end: function () {
                                            for (key in data.field){
                                                wht[key][key] = data.field[key];
                                            }
                                            setTpl.oneTpl(wht,$("#wuhuate-tpl"),$("#wuhuate-view"));
                                        }
                                    })
                                } else {
                                    layer.msg("修改失败，稍后重试！", {
                                        icon: 2
                                    })
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            })());

            //稳定性和反应性
            ((function () {
                var wdx = allData.wendingying;
                if (wdx){
                    setTpl.oneTpl(wdx,$("#wendingxing-tpl"),$("#wendingxing-view"));
                } else {
                    wdx = {
                        "buxiangrong": {},
                        "huaxuewen": {},
                        "weixiande": {},
                        "yingbimian": {},
                        "fanyingxing": {},
                        "weixianfan": {}
                    }
                }
                $(".wendingxingEdit").click(function () {
                    setTpl.oneTpl(wdx,$("#wendingxing-Edit"),$("#wendingxing-view-edit"));
                    openView($("#wendingxing-view-edit"),'稳定性和反应性');
                });
                form.on('submit(wendingxing)',function (data) {
                    layer.confirm("确认添加吗？",{
                        btn:['确定','取消'],
                        yes:function (index) {
                            isNull(data.field);
                            customAjax.http('/wendingying', data.field, function (res) {
                                if (res.code === 0) {
                                    layer.msg('添加成功', {
                                        icon: 1,
                                        time: 1000,
                                        end:function () {
                                            for (key in data.field){
                                                wdx[key][key] = data.field[key];
                                            }
                                            setTpl.oneTpl(wdx,$("#wendingxing-tpl"),$("#wendingxing-view"));
                                        }
                                    })
                                }else {
                                    layer.msg("修改失败，稍后重试！", {
                                        icon: 2
                                    })
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            })());

            //毒理学资料
            ((function () {
                var dlx = allData.dulixue;
                var zf = allData.zufen;
                if (dlx && zf){
                    dlx.zhiaixing.zzufen = zf.zufen;
                    dlx.zhiaixing.zcas = zf.CAS;
                    setTpl.oneTpl(dlx,$("#dulixue-tpl"),$("#dulixue-view"));
                } else {
                    //提交的下面新建
                    //不存在，用来渲染的
                    dlx = {
                        "other":{
                            "fujiawei": "",
                            "pifufu": "",
                            "dancijie": "",
                            "yanzhongyan": "",
                            "huxizhi": "",
                            "pifuzhi": "",
                            "xiruwei": "",
                            "shengzhixi": "",
                            "shengzhidu": "",
                            "fanfujie": ""
                        },
                        "zhiaixing":{
                            "zzufen":[],
                            "zcas":[],
                            "iarc": [],
                            "ntp": []
                        },
                        "jixingdu":{
                            "jzufen": [],
                            "lc": [],
                            "jcas": [],
                            "jinkou": [],
                            "jinpi": []
                        }
                    };
                    //信息不存在，请求存在的法规和CAS
                    customAjax.http("/returnZhufenAndCas",{},function (msg) {
                        if (msg.code === 0){
                            dlx.zhiaixing.zzufen = msg.data.zufen;
                            dlx.zhiaixing.zcas = msg.data.cas;
                        }else {
                            layer.msg("没有相关组分或者CAS No.！！")
                        }
                    });
                }

                $(".dulixueEdit").click(function () {
                    console.log(dlx);
                    setTpl.oneTpl(dlx,$("#dulixue-edit"),$("#dulixue-view-Edit"));
                    openView($("#dulixue-view-Edit"),'毒理学资料');
                });

                //继续添加按钮
                $("#dulixue-content").delegate('.duli-addItem','click',function () {
                    $("#duli-tbody").append(createElementDu());
                });
                function createElementDu(){
                    var item = $(" <tr>\n" +
                        "                                        <td><input type=\"text\" name=\"jzufen[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
                        "                                        <td><input type=\"text\" name=\"jcas[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
                        "                                        <td><input type=\"text\" name=\"jinkou[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
                        "                                        <td><input type=\"text\" name=\"jinpi[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
                        "                                        <td><input type=\"text\" name=\"lc[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
                        "                                        <td><button type=\"button\" class=\"layui-btn  layui-btn-xs layui-btn-danger jixindu-del\" title=\"删除当前一条\">删除</button></td>\n" +
                        "                                    </tr>");
                    return item;
                }
                //删除
                $("#dulixue-content").delegate(".jixindu-del",'click',function () {
                    $(this).parents("tr").remove();
                });


                form.on('submit(dulixue)',function (data) {
                    layer.confirm("确认修改吗？",{
                        btn:['确定','取消'],
                        yes:function (index) {
                            var dlxAjax = {
                                "other":{}
                            };
                            var dlxTpl = {
                                "other":{}
                            };
                            //处理急性毒性
                            var jixingdu = {
                                "jzufen":[],
                                "jcas":[],
                                "jinkou":[],
                                "jinpi":[],
                                "lc":[]
                            };
                            for (key in data.field){
                                if (key.lastIndexOf("jzufen") !== -1){
                                    jixingdu.jzufen.push(data.field[key]);
                                }
                                if (key.lastIndexOf("jcas") !== -1) {
                                    jixingdu.jcas.push(data.field[key]);
                                }
                                if (key.lastIndexOf("jinkou") !== -1){
                                    jixingdu.jinkou.push(data.field[key]);
                                }
                                if (key.lastIndexOf("jinpi") !== -1){
                                    jixingdu.jinpi.push(data.field[key]);
                                }
                                if (key.lastIndexOf("lc") !== -1){
                                    jixingdu.lc.push(data.field[key]);
                                }
                            }

                            //处理致癌性
                            var zhiaixing = {
                                "iarc":[],
                                "ntp":[]
                            };
                            var zhiaixingTpl = {
                                "zcas":[],
                                "zzufen":[],
                                "iarc":[],
                                "ntp":[]
                            };
                            for (key in data.field){
                                if (key.lastIndexOf("zcas") !== -1){
                                    zhiaixingTpl.zcas.push(data.field[key]);
                                }
                                if (key.lastIndexOf("zzufen") !== -1) {
                                    zhiaixingTpl.zzufen.push(data.field[key]);
                                }
                                if (key.lastIndexOf("iarc") !== -1){
                                    if (data.field[key] === ""){
                                        data.field[key] = "未列入"
                                    }
                                    zhiaixing.iarc.push(data.field[key]);
                                    zhiaixingTpl.iarc.push(data.field[key]);
                                }
                                if (key.lastIndexOf("ntp") !== -1){
                                    if (data.field[key] === ""){
                                        data.field[key] = "未列入"
                                    }
                                    zhiaixing.ntp.push(data.field[key]);
                                    zhiaixingTpl.ntp.push(data.field[key]);
                                }
                            }
                            for (var key in data.field){
                                if (key.lastIndexOf('[') === -1){
                                    if (data.field[key] === "") {
                                        data.field[key] = "无资料"
                                    }
                                    dlxAjax.other[key] = data.field[key];
                                    dlxTpl.other[key] = data.field[key];
                                }
                            }
                            dlxAjax.zhiaixing = zhiaixing;
                            dlxAjax.jixingdu = jixingdu;
                            dlxTpl.jixingdu = jixingdu;
                            dlxTpl.zhiaixing = zhiaixingTpl;
                            $.ajax({
                                url:'/dulixue',
                                type:"post",
                                dataType:"json",
                                contentType: "application/json; charset=utf-8",
                                data:JSON.stringify(dlxAjax),
                                success:function (msg) {
                                    if (msg.code === 0){
                                        layer.msg("修改成功",{
                                            icon: 1,
                                            time: 1000,
                                            end:function () {
                                                dlx = dlxTpl;
                                                setTpl.oneTpl(dlxTpl,$("#dulixue-tpl"),$("#dulixue-view"));
                                            }
                                        })
                                    }else {
                                        layer.msg("修改失败，稍后重试！", {
                                            icon: 2
                                        })
                                    }
                                },
                                error:function (err) {
                                    layer.msg("服务器繁忙,请稍后重试");
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            })());

            //生态学资料
            ((function () {
                var st = allData.shengtaixue;
                if (st){
                    setTpl.oneTpl(st,$("#shengtaixue-tpl"),$("#shengtaixue-view"));
                } else {
                    st = {
                        "manxingshui": {},
                        "pbt": {},
                        "shengwufu": {},
                        "turangzhong": {},
                        "chijiuxing": {},
                        "jixingshui": {}
                    }
                }
                $(".shengtaixueEdit").click(function () {
                    setTpl.oneTpl(st,$("#shengtaixue-edit"),$("#shengtaixue-view-edit"));
                    openView($("#shengtaixue-view-edit"),'生态学资料');
                });
                form.on('submit(shengtaixue)',function (data) {
                    layer.confirm("确认添加吗？",{
                        btn:['确定','取消'],
                        yes:function (index) {
                            isNull(data.field);
                            customAjax.http('/shengtaixue', data.field, function (res) {
                                if (res.code === 0) {
                                    layer.msg('添加成功', {
                                        icon: 1,
                                        time: 1000,
                                        end:function () {
                                            for (key in data.field){
                                                st[key][key] = data.field[key];
                                            }
                                            setTpl.oneTpl(st,$("#shengtaixue-tpl"),$("#shengtaixue-view"));
                                        }
                                    })
                                }else {
                                    layer.msg("修改失败，稍后重试！", {
                                        icon: 2
                                    })
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            })());

            //废弃处置
            ((function () {
                var fq = allData.feiqichuzhi;
                if (fq){
                    setTpl.oneTpl(fq,$("#feiqichu-tpl"),$("#feiqichu-view"));
                }else {
                    fq = {
                        "feiqihua": {},
                        "feiqizhu": {},
                        "wuranbao": {}
                    }
                }
                $(".feiqichuEdit").click(function () {
                    setTpl.oneTpl(fq,$("#feiqichu-edit"),$("#feiqichu-view-edit"));
                    openView($("#feiqichu-view-edit"),'废弃处置');
                });
                form.on('submit(feiqichu)',function (data) {
                    layer.confirm("确认添加吗？",{
                        btn:['确定','取消'],
                        yes:function (index) {
                            isNull(data.field);
                            customAjax.http('/feiqichuzhi', data.field, function (res) {
                                if (res.code === 0) {
                                    layer.msg('添加成功', {
                                        icon: 1,
                                        time: 1000,
                                        end:function () {
                                            for (key in data.field){
                                                fq[key][key] = data.field[key];
                                            }
                                            setTpl.oneTpl(fq,$("#feiqichu-tpl"),$("#feiqichu-view"));
                                        }
                                    })
                                }else {
                                    layer.msg("修改失败，稍后重试！", {
                                        icon: 2
                                    })
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            })());

            //运输信息
            ysxx();
            function ysxx() {
                var ys = allData.yunshuxingxi;
                var ysbqD = {
                    "imgs":[]
                };
                if (ys){
                    var ysbq = allData.weixianbiaoshi.yunshubiaoqian;
                    for (key in ysbq){
                        if (ysbq[key].trim().lastIndexOf('\\') !== -1){
                            var data = ysbq[key].trim().split('\\');
                            for (var i=0;i<data.length;i++){
                                if (ysbqD.imgs.length >0){
                                    var temp = [];
                                    for (var j=0;j<ysbqD.imgs.length;j++){
                                        temp[ysbqD.imgs[j]] = true;
                                    }
                                    if (!temp[data[i]]){
                                        ysbqD.imgs.push(data[i]);
                                    }
                                } else {
                                    ysbqD.imgs.push(data[i]);
                                }
                            }
                        }else if (ysbq[key].trim().lastIndexOf('\\') === -1 && ysbq[key].trim().lastIndexOf('/img') !== -1) {
                            if (ysbqD.imgs.length >0){
                                var temp = [];
                                for (var j=0;j<ysbqD.imgs.length;j++){
                                    temp[ysbqD.imgs[j]] = true;
                                }
                                if (!temp[ysbq[key].trim()]){
                                    ysbqD.imgs.push(ysbq[key].trim());
                                }
                            } else {
                                ysbqD.imgs.push(ysbq[key].trim());
                            }
                        }
                    }
                    ys.ysbqD = ysbqD;
                    setTpl.oneTpl(ys,$("#yunshuxin-tpl"),$("#yunshuxin-view"));
                }else {
                    ys = {
                        "ysbqD":ysbqD,
                        "yunshuzhu": {},
                        "yunshuci": {},
                        "name": {},
                        "bianhao": {},
                        "baozhuanglei": {},
                        "haiyangwu": {}
                    }
                }
                $(".yunshuxinEdit").click(function () {
                    if (allData.weixianbiaoshi) {
                        var ysbq = allData.weixianbiaoshi.yunshubiaoqian;
                        for (key in ysbq){
                            if (ysbq[key].trim().lastIndexOf('\\') !== -1){
                                var data = ysbq[key].trim().split('\\');
                                for (var i=0;i<data.length;i++){
                                    if (ysbqD.imgs.length >0){
                                        var temp = [];
                                        for (var j=0;j<ysbqD.imgs.length;j++){
                                            temp[ysbqD.imgs[j]] = true;
                                        }
                                        if (!temp[data[i]]){
                                            ysbqD.imgs.push(data[i]);
                                        }
                                    } else {
                                        ysbqD.imgs.push(data[i]);
                                    }
                                }
                            }else if (ysbq[key].trim().lastIndexOf('\\') === -1 && ysbq[key].trim().lastIndexOf('/img') !== -1) {
                                if (ysbqD.imgs.length >0){
                                    var temp = [];
                                    for (var j=0;j<ysbqD.imgs.length;j++){
                                        temp[ysbqD.imgs[j]] = true;
                                    }
                                    if (!temp[ysbq[key].trim()]){
                                        ysbqD.imgs.push(ysbq[key].trim());
                                    }
                                } else {
                                    ysbqD.imgs.push(ysbq[key].trim());
                                }
                            }
                        }
                        ys.ysbqD = ysbqD;
                        setTpl.oneTpl(ys, $("#yunshuxin-add-tpl"), $("#yunshuxin-add-view"));
                        openView($("#yunshuxin-add-view"), '运输信息');
                    }else {
                        layer.msg("您还没有输入危险说明编码,缺少运输标签和标记，请在顶部点击按钮添加！",{
                            icon:5,
                            time:1000
                        });
                    }
                });
                form.on('submit(yunshuxin)',function (data) {
                    layer.confirm("确认添加吗？",{
                        btn:['确定','取消'],
                        yes:function (index) {
                            isNull(data.field);
                            customAjax.http('/yunshuxingxi', data.field, function (res) {
                                if (res.code === 0) {
                                    layer.msg('添加成功', {
                                        icon: 1,
                                        time: 500,
                                        end:function () {
                                            for (key in ys){
                                                if (key !== "ysbqD") {
                                                    ys[key][key] = data.field[key];
                                                }
                                            }
                                            setTpl.oneTpl(ys,$("#yunshuxin-tpl"),$("#yunshuxin-view"));
                                        }
                                    })
                                }else {
                                    layer.msg("修改失败，稍后重试！", {
                                        icon: 2
                                    })
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });
            }

            //法规信息
            ((function () {
                var fg = allData.zufen;
                if (fg && allData.zufen.zufenC0 && allData.zufen.zufen0){
                    wc(fg);
                    setTpl.oneTpl(fg,$("#faguixin-tpl"),$("#faguixin-view"));
                }else {
                    fg = {}
                }
                $(".faguixinEdit").click(function () {
                    if ($.isEmptyObject(fg)){
                        //如果为空 请求存在的法规数据
                        customAjax.http("/returnRuleAndZhufen","",function (msg) {
                            if (msg.code === 0){
                                for (key in msg.data[0]){
                                    fg[key] = msg.data[0][key];
                                }
                                setTpl.oneTpl(fg,$("#faguixin-add"),$("#faguixin-add-view"));
                            }else {
                                layer.msg("没有相关组分！！",{
                                    icon:5
                                })
                            }
                        });
                    }else {
                        setTpl.oneTpl(fg,$("#faguixin-add"),$("#faguixin-add-view"));
                    }
                    openView($("#faguixin-add-view"),'运输信息');
                });

                form.on("submit(faguixin)",function (data) {
                    var num = $("#faguixin-tbody").children().length;
                    layer.confirm("确认添加吗？",{
                        btn:['确定','取消'],
                        yes:function (index) {
                            console.log(data.field);
                            for (key in data.field){
                                if (data.field[key] !== "是" && data.field[key] !== "否") {
                                    layer.msg('含有除是否外其他字符，请检查后提交！',{
                                        icon:5
                                    });
                                    return;
                                }
                            }
                            var faguixin = {};
                            for (var i=0;i<num;i++){
                                faguixin['zufen'+i] = [];
                                faguixin['zufenC'+i] = [];
                            }
                            for (k1 in data.field){
                                var FkIndex = k1.indexOf('[');
                                var Fk = k1.substring(0,FkIndex);
                                for (k2 in faguixin){
                                    if (Fk === k2){
                                        faguixin[k2].push(data.field[k1])
                                    }
                                }
                            }
                            faguixin.n = [];
                            faguixin.n.push(fg.zufen.length); //长度
                            $.ajax({
                                url:'/Submitxuanzhe',
                                type:"post",
                                dataType:"json",
                                contentType: "application/json; charset=utf-8",
                                data:JSON.stringify(faguixin),
                                success:function (msg) {
                                    if (msg.code === 0){
                                        layer.msg("成功",{
                                            icon: 1,
                                            time: 500,
                                            end:function () {
                                                for (key in faguixin) {
                                                    if (key !== "n"){
                                                        fg[key] = faguixin[key];
                                                    }
                                                }
                                                wc(fg);
                                                console.log(fg);
                                                setTpl.oneTpl(fg,$("#faguixin-tpl"),$("#faguixin-view"));
                                            }
                                        })
                                    }else {
                                        layer.msg("修改失败，稍后重试！", {
                                            icon: 2
                                        })
                                    }
                                },
                                error:function (err) {
                                    layer.msg("服务器异常,请稍后重试",{
                                        icon: 2
                                    });
                                }
                            });
                            layer.close(index);
                            layer.closeAll('page');
                        }
                    });
                    return false;
                });

                //格式化 组分 (是否)
                function wc(fg) {
                    var tick = {};

                    //多少个组分就有多少个数组
                    var zufenD = {};
                    for (var j=0;j<fg.zufen.length;j++){
                        zufenD[j] = [];
                    }
                    for (key in fg){
                        if (key.lastIndexOf("zufen")!==-1&&key!=="zufen"&&key.lastIndexOf("zufenC")===-1&&key.lastIndexOf("zufenE")===-1){
                            tick[key] = fg[key];
                        }
                    }
                    for (key in tick){
                        var index = key.indexOf('n')+1;
                        var k = key.substring(index);
                        zufenD[k] = tick[key];
                    }
                    fg.zufenD = zufenD;

                    //中国法规
                    var tickC = {};
                    //多少个组分就有多少个数组
                    var zufenCD = {};
                    for (var i=0;i<fg.zufen.length;i++){
                        zufenCD[i] = [];
                    }
                    for (key in fg){
                        if (key.lastIndexOf("zufenC")!==-1){
                            tickC[key] = fg[key];
                        }
                    }
                    for (key in tickC){
                        var index = key.indexOf('C')+1;
                        var k = key.substring(index);
                        zufenCD[k] = tickC[key];
                    }
                    fg.zufenCD = zufenCD;
                }
            })());

        }
    //左边动态导航
    ((function () {
        var allPage = $(".page-style");
        for (var j=0;j<allPage.length;j++){
            allPage[j].num = j;
        }
        var allNavbtn = $(".left-bar");
        for (var i = 0; i < allNavbtn.length; i++) {
            allNavbtn[i].num = i;
            $(allNavbtn[i]).click(function () {
                var num = this.num;
                var height = 0;
                for(var i=0;i<num;i++){
                    height+=$(allPage[i]).height();
                }
                height+=num*10;
                $(".mainContent").animate({"scrollTop":height+"px"},500,function () {

                });
            });
        }
    })());



    //弹出层
    function openView(view,title){
        layer.open({
            type: 1,
            content: view,
            area: ['50%',"90%"],
            shift: 1,
            shade:0,
            maxmin: true,
            title: [title, 'font-size:22px;'],
            resize: false
        });
    }

    /**
     * //判断字符串
     * @param {父字符串}obj
     * @param {子字符串}childObj
     */
    function initObj(obj,childObj){
        var arrs = [];
        for (key in childObj){
            arrs = [];
            if (childObj[key]) {
                if (childObj[key].lastIndexOf("。") !== -1){
                    var info = childObj[key].split("。");
                    info.forEach(function (item,index) {
                        if (item){
                            arrs.push(item);
                        }
                    });
                    obj[key] = arrs;
                }else {
                    arrs.push(childObj[key]);
                    obj[key] = arrs;
                }
            }else {
                obj[key] = arrs;
            }
        }
    }

    //判断提交是否有空
    function isNull(data){
        for (key in data){
            if (data[key] === ""){
                data[key] = "无资料"
            }
        }
    }

    exports('dasisEdit',{});
});