//搜索功能
layui.define(['layer','table',"customAjax",'form','exportDoc'], function (exports) {
    var layer = layui.layer,
        table = layui.table,
        customAjax = layui.customAjax,
        form = layui.form,
        exportDoc = layui.exportDoc;

    function showTableInfo(options) {
        var index = layer.load(0);
        var tableIns = table.render({
            elem: options.node,
            url: options.url,
            text:{
                none: '暂无相关数据'
            },
            page:{
                limit:options.limit || 10
            },
            response: {
                statusName: 'code'
                , statusCode: 0
                , msgName: 'msg'
                , countName: 'count'
                , dataName: 'data'
            },
            cols: [
                options.col
            ]
        });
        layer.close(index);

        //行工具事件
        table.on('tool(test)',function (obj) {
            var data = obj.data;
            var layEvent = obj.event;
            if (layEvent === 'detail'){
                if (!options.href){
                    //是否需要向后端请求数据
                    if (options.http) {
                        customAjax.httpSelect(options.http+"?id=" + data.id,function (res) {
                            if (res.code === 0){
                                res.data.id = data.id;
                                var info = res.data;
                                form.val(options.filter,{
                                    "id":info.id,
                                    "bianhao":info.bianhao,
                                    "shuliang":info.shuliang,
                                    "jiancheyiju":info.jiancheyiju,
                                    "yangpingbiaoshi":info.yangpingbiaoshi,
                                    "yangpingchenfen":info.yangpingchenfen,
                                    "yangpingxingzhuang":info.yangpingxingzhuang,
                                    "jianguanren":info.jianguanren,
                                    "yewulei":info.yewulei,
                                    "jianceri":info.jianceri,
                                    "yangpinxin":info.yangpinxin,
                                    "jishuming":info.jishuming,
                                    "shuyu":info.shuyu,
                                    "fuhe":info.fuhe,
                                    "cnname":info.cnname,
                                    "usname":info.usname,
                                    "biename":info.biename,
                                    "casno":info.casno,
                                    "ecno":info.ecno,
                                    "fenzisi":info.fenzisi,
                                    "tuijianyongtu":info.tuijianyongtu,
                                    "xianzhiyongtu":info.xianzhiyongtu,
                                    "sqname":info.sqname,
                                    "sqadd":info.sqadd,
                                    "sqcode":info.sqcode,
                                    "sqtel":info.sqtel,
                                    "sqfax":info.sqfax,
                                    "sqyouxiang":info.sqyouxiang,
                                    "gysname":info.gysname,
                                    "gysadd":info.gysadd,
                                    "gyscode":info.gyscode,
                                    "gystel":info.gystel,
                                    "gysfax":info.gysfax,
                                    "gysyouxiang":info.gysyouxiang,
                                    "qiyetel":info.qiyetel
                                });
                                $(".Edit-btn").css("display","block");
                                $(".Submit-btn").css("display","none");
                            }
                        });
                        //测试中心信息
                    }else if (options.page === "testInfo"){
                        testInfo(data,options.filter)
                        //管理用户
                    }else if (options.page === "user"){
                        form.val(options.filter,{
                            "id":data.id
                        })
                        //法规
                    }else if (options.page === "rules"){
                        form.val(options.filter,{
                            "id":data.id,
                            "usname":data.usname,
                            "cnname":data.cnname,
                            "region":data.region
                        })
                    }
                    layer.open({
                        type: 1,
                        content: options.content,
                        area: [options.width,options.height],
                        shift: 1,
                        maxmin: true,
                        title: [options.title, 'font-size:22px;'],
                        resize: false
                    });
                } else {
                    //详情
                    localStorage.setItem('name',data.name);
                    // sessionStorage.getItem('name',data.name)
                    location.href = options.href;
                }
            } else if (layEvent === 'del'){
                layer.confirm('确定删除', function (index) {
                    obj.del();
                    layer.close(index);
                    //向服务端发送删除指令
                    customAjax.httpDel(options.delUrl+"?id=" + data.id,function (msg) {
                        if(msg.code === 0){
                            layer.msg("删除成功！",{
                                icon:6,
                                time:500,
                                end:function () {
                                    tableIns.reload(); //刷新表格
                                }
                            })
                        }
                    },tableIns);
                })
            }else if (layEvent === "export"){
                layer.confirm('确定导出吗？',{
                    yes:function (index) {
                        var d = {
                            id:data.id,
                            name:data.cnname
                        };
                        var i = layer.load(0);
                        customAjax.http('/daochu',d,function (msg) {
                            if (msg.code === 0){
                                var contentD = msg.data;
                                //有信息为空阻止导出
                                for (key in contentD){
                                    if (!contentD[key]){
                                        layer.msg('信息未填写完整,请到化学品管理填写！',{
                                            icon:5
                                        });
                                        layer.close(i);
                                        return;
                                    }
                                    if (!(contentD.zufen.zufenC0 && contentD.zufen.zufen0)) {
                                        layer.msg('法规管理未填写完整,请到化学品管理填写！',{
                                            icon:5
                                        });
                                        layer.close(i);
                                        return;
                                    }
                                }
                                layer.close(i);
                                exportDoc(contentD);
                            } else {
                                layer.msg("导出失败，请稍后重试！",{
                                    icon:5
                                });
                                layer.close(i);
                            }
                        });
                        layer.close(index);
                    }
                });
            }
        });
        return tableIns;
    }

    //检测中心信息
    function testInfo(data,filter){
        form.val(filter,{
            "jcname":data.jcname,
            "jctel":data.jctel,
            "jcfax":data.jcfax,
            "jccode":data.jccode,
            "jcadd":data.jcadd,
            "id":data.id
        })
    }

    exports("customTable",showTableInfo);
});
 