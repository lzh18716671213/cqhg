layui.define(['layer', 'form','jquery','element','customAjax','customTable','bar'],function (exports) {
    var  form = layui.form,
        layer = layui.layer,
        $ = layui.$,
        element = layui.element,
        customAjax = layui.customAjax,
        customTable = layui.customTable,
        bar = layui.bar;
    bar();


    //初始化页面
    var mainObj = {
        node:"#testInfo-demo",
        url:"/findTestCenter",
        limit:20,
        col:[//表头
            { field: 'jcname', title: '检测中心名称' }
            , { field: 'jctel', title: '检测中心电话' }
            , { field: 'jcfax', title: '检测中心传真' }
            , { field: 'jccode', title: '检测中心邮编' }
            , { field: 'jcadd', title: '检测中心地址' }
            , { title: '操作', toolbar: '#barDemo' }
        ],
        //弹出层参数
        content:$("#editTestInfo"),
        title:'修改检测中心信息',
        width:"760px",
        page:"testInfo",
        filter:"editTestInfo"
    };
    var tableIns = customTable(mainObj);

    //添加
    $(".my-btn").click(function () {
        $(".addInfo").css("display","block");
        $(".editInfo").css("display","none");
        layer.open({
            type: 1,
            content: $("#addTestInfo"),
            area: ['50%'],
            shift: 1,
            maxmin: true,
            title: ['添加检测中心信息', 'font-size:22px;'],
            resize: false,
            cancel:function (index) {
                $("#testInfo")[0].reset();
            }
        });

        form.on('submit(testInfo)',function (data) {
            customAjax.http('/insertTestCenter',data.field,function (res) {
                if (res.code === 0){
                    layer.msg('添加成功',{
                        icon:6,
                        time:1000,
                        end:function () {
                            layer.closeAll('page');
                            tableIns.reload();
                        }
                    })
                }else {
                    layer.msg('添加失败，请稍后重试！',{
                        icon:5,
                        time:1000
                    })
                }
            });
            return false;
        });

        form.verify({
            tel:[
                /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/,
                '电话格式错误！'
            ],
            fax:[
                /^(\d{3,4}-)?\d{7,8}$/,
                '传真号码格式错误！'

            ]
        })
    });


    //修改
    form.on('submit(editTestInfo)',function (data) {
        customAjax.http('/updateTestCenter',data.field,function (res) {
            if (res.code === 0){
                layer.msg("修改成功",{
                    icon: 1,
                    time: 1000,
                    end:function () {
                        layer.closeAll('page');
                        tableIns.reload();
                    }
                })
            }else {
                layer.msg("修改失败，请稍后重试",{
                    icon: 5,
                    time: 1000
                })
            }
        });
        return false;
    });

    exports('testCenterInfo',{})
});