layui.define(['layer','jquery','form','element','customTable','customWindows','bar'],function (exports) {
    var  form = layui.form,
        layer = layui.layer,
        $ = layui.$,
        element = layui.element,
        customTable = layui.customTable,
        customWindows = layui.customWindows,
        bar = layui.bar;
    bar();
    //初始化页面
    var mainObj = {
        node:'#loginLog-demo',
        url:'/findAllLog',
        limit:10,
        col:[//表头
            { field: 'username', title: '用户名' }
            , { field: 'type', title: '用户类型',templet:function (d) {
                    return d.type === "admin" ? "管理员" : "普通用户";
                }
            }
            , { field: 'status', title: '用户状态',templet:function (d) {
                    return d.status === 1 ? "正在使用" : "停止使用"
                } }
            , { field: 'createtime', title: '编辑时间' }
            , { field: 'miaoshu', title: '描述' }
        ]
    };
    customTable(mainObj);

//搜索
    $(".search-btn").click(function () {
        if ($(".search-input").val().length) {
            var searchObj = {
                node:'#search-demo',
                url:'/findLog?tick='+$(".search-input").val(),
                limit:20,
                col:[
                    { field: 'username', title: '用户名' }
                    , { field: 'type', title: '用户类型',templet:function (d) {
                            return d.type === "admin" ? "管理员" : "普通用户";
                        }
                    }
                    , { field: 'status', title: '用户状态',templet:function (d) {
                            return d.status === "1" ? "正在使用" : "停止使用"
                        } }
                    , { field: 'createtime', title: '编辑时间' }
                    , { field: 'miaoshu', title: '描述' }
                ]
            };
            //渲染数据
            customTable(searchObj);
            //使用弹窗显示搜索到的内容
            customWindows();
        } else {
            layer.msg('请输入搜索的内容')
        }

    });

    exports('loginLog',{});
});