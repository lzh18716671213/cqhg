layui.define(['layer','jquery','form','element','customTable','customWindows','bar'],function (exports) {
    var  form = layui.form,
        layer = layui.layer,
        $ = layui.$,
        element = layui.element,
        customTable = layui.customTable,
        customWindows = layui.customWindows,
        bar = layui.bar;
    bar();
    //初始化页面
    var mainObj = {
        node:'#exportLog-demo',
        url:'/findAllDcLog',
        limit:10,
        col:[//表头
            { field: 'username', title: '用户名' }
            , { field: 'createtime', title: '导出时间' }
            , { field: 'miaosu', title: '描述' }
        ]
    };
    customTable(mainObj);

//搜索
    $(".search-btn").click(function () {
        if ($(".search-input").val().length) {
            var searchObj = {
                node:'#search-demo',
                url:'/findDcLog?tick='+$(".search-input").val(),
                limit:20,
                col:[
                    { field: 'username', title: '用户名' }
                    , { field: 'createtime', title: '导出时间' }
                    , { field: 'miaosu', title: '描述' }
                ]
            };
            //渲染数据
            customTable(searchObj);
            //使用弹窗显示搜索到的内容
            customWindows();
        } else {
            layer.msg('请输入搜索的内容')
        }

    });

    exports('exportLog',{});
});