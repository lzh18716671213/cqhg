
layui.define(['setTpl'],function (exports) {
    var setTpl = layui.setTpl;
    function exportView(contentD){
        //封面
        ((function () {
            contentD.diyibufen.CN.testCenter = contentD.testCenter;
            var ysx = contentD.yunshuxingxi;
            var ysxD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(ysx,ysxD);
            //封面的组分
            var zufen = contentD.zufen.zufen;
            var hanl = contentD.zufen.hanl;
            var a = [];
            for (var i=0;i<zufen.length;i++){
                var S = zufen[i]+"："+hanl[i];
                a.push(S);
            }
            var S1 = a.join(";");
            contentD.diyibufen.CN.zf = S1;
            contentD.diyibufen.CN.ysx = ysxD.infoC;
            setTpl.oneTpl(contentD.diyibufen.CN,$("#export-home-page-tpl-SDS"),$("#export-home-page-view-SDS"));
            setTpl.oneTpl(contentD.diyibufen.CN,$("#export-home-page-tpl-SDS-E"),$("#export-home-page-view-SDS-E"));
            setTpl.oneTpl(contentD.diyibufen.CN,$("#export-home-page-tpl-GHS"),$("#export-home-page-view-GHS"));
        })());

        //GHS中文
        //GHS英文   ,包含再第二部分

        ((function () {
            var E = contentD.diyibufen.EN;
            setTpl.oneTpl(contentD.diyibufen.CN,$("#deliver-tpl"),$("#deliver-info"));
            contentD.diyibufen.CN.E = E;
            setTpl.oneTpl(contentD.diyibufen.CN,$("#deliver-tpl-E"),$("#deliver-info-E"));
        })());

        //第二部分
        ((function () {
            var wxb = contentD.weixianbiaoshi;
            var wxbD = {
                "infoE":{},
                "infoC":{}
            };
            //处理中英文
            jsons(wxb,wxbD);
            //处理图片
            var GHSys = {
                "imgs":[],
                "xhc":[]
            };
            var cons = wxbD.infoC.GHSbiaoqianyaoshu;
            for (key in cons){
                if (key.trim().lastIndexOf('象形图') !== -1){
                    if (cons[key].trim().lastIndexOf('\\') !== -1){
                        var d = cons[key].trim().split('\\');
                        for (var i=0;i<d.length;i++){
                            if (GHSys.imgs.length >0){
                                var temp = [];
                                for (var j=0;j<GHSys.imgs.length;j++){
                                    temp[GHSys.imgs[j]] = true;
                                }
                                if (!temp[d[i]]){
                                    GHSys.imgs.push(d[i]);
                                }
                            } else {
                                GHSys.imgs.push(d[i]);
                            }
                        }
                    }else if (cons[key].trim().lastIndexOf('\\') === -1 && cons[key].trim().lastIndexOf('/img') !== -1) {
                        if (GHSys.imgs.length >0){
                            var temp = [];
                            for (var j=0;j<GHSys.imgs.length;j++){
                                temp[GHSys.imgs[j]] = true;
                            }
                            if (!temp[cons[key].trim()]){
                                GHSys.imgs.push(cons[key].trim());
                            }
                        } else {
                            GHSys.imgs.push(cons[key].trim());
                        }
                    }
                } else {  // 信号词
                    GHSys.xhc.push(cons[key].trim());
                }
            }
            wxbD.infoC.GHSys = GHSys;
            wxbD.infoE.GHSys = GHSys;
            setTpl.oneTpl(wxbD.infoC,$("#weixianbioa-tpl"),$("#weixianbioa-info"));
            setTpl.oneTpl(wxbD.infoE,$("#weixianbioa-tpl-E"),$("#weixianbioa-info-E"));

            wxbD.infoC.bianhao = contentD.diyibufen.CN.bianhao;
            wxbD.infoC.gysname = contentD.diyibufen.CN.gysname;
            wxbD.infoC.gysadd = contentD.diyibufen.CN.gysadd;
            wxbD.infoC.gyscode = contentD.diyibufen.CN.gyscode;
            wxbD.infoC.gystel = contentD.diyibufen.CN.gystel;
            wxbD.infoC.yushubianhao = contentD.yunshuxingxi.bianhao.bianhao;
            wxbD.infoC.name = contentD.yunshuxingxi.name.name;
            wxbD.infoC.qiyetel = contentD.diyibufen.CN.qiyetel;
            wxbD.infoE.bianhao = contentD.diyibufen.CN.bianhao;
            wxbD.infoE.gysname = contentD.diyibufen.EN.GysnameE;
            wxbD.infoE.gysadd = contentD.diyibufen.EN.GysaddE;
            wxbD.infoE.gyscode = contentD.diyibufen.CN.gyscode;
            wxbD.infoE.gystel = contentD.diyibufen.CN.gystel;
            wxbD.infoE.yushubianhaoE = contentD.yunshuxingxi.bianhao.bianhaoE;
            wxbD.infoE.nameE = contentD.yunshuxingxi.name.nameE;
            wxbD.infoE.hxpName = contentD.diyibufen.CN.usname;
            wxbD.infoE.qiyetel = contentD.diyibufen.CN.qiyetel;
            //GHS的组分(中文)
            var zufen = contentD.zufen.zufen;
            var zufenE = contentD.zufen.zufenE;
            var hanl = contentD.zufen.hanl;
            var a = [];
            var aE = [];
            for (var i=0;i<zufen.length;i++){
                var S = zufen[i]+"："+hanl[i];
                a.push(S);
            }
            var S1 = a.join(";");
            //GHS的组分(英文)
            for (var i=0;i<zufenE.length;i++){
                var S = zufenE[i]+"："+hanl[i];
                aE.push(S);
            }
            var S2 = aE.join(";");
            wxbD.infoE.zf = S2;
            wxbD.infoC.zf = S1;
            //H编码的说明
            var H = contentD.weixianbiaoshi.weixianyingshuo;
            var HE = [],
                HC = [];
            for (var key in H){
                if (key.lastIndexOf('E') !== -1){
                    HE.push(H[key]);
                } else {
                    HC.push(H[key]);
                }
            }
            wxbD.infoC.HC = HC.join('，');
            wxbD.infoE.HE = HE.join('，');
            //GHS中文
            setTpl.oneTpl(wxbD.infoC,$("#export-SHS-tpl"),$("#export-SHS-view"));
            // //GHS英文
            setTpl.oneTpl(wxbD.infoE,$("#export-SHSE-tpl"),$("#export-SHSE-view"));
        })());

        //第三部分
        setTpl.oneTpl(contentD.zufen,$("#chenfenzu-tpl"),$("#chenfenzu-info"));
        setTpl.oneTpl(contentD.zufen,$("#chenfenzu-tpl-E"),$("#chenfenzu-info-E"));

        //第四部分
        ((function () {
            var jjcs = contentD.jijiucuoshi;
            var jjcsD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(jjcs,jjcsD);
            var arr1 = initObj(jjcsD.infoC.zuizhongyao.zuizhongyao);
            if (arr1){
                jjcsD.infoC.zuizhongyao = arr1;
            }
            var arr2 = initObj(jjcsD.infoC.jinjiyi.jinjiyi);
            if (arr2){
                jjcsD.infoC.jinjiyi = arr2;
            }
            var arr3 = initObjE(jjcsD.infoE.zuizhongyao.zuizhongyaoE);
            if (arr3){
                jjcsD.infoE.zuizhongyao = arr3;
            }
            var arr4 = initObjE(jjcsD.infoE.jinjiyi.jinjiyiE);
            if (arr4){
                jjcsD.infoE.jinjiyi = arr4;
            }

            setTpl.oneTpl(jjcsD.infoC,$("#jijiucuo-tpl"),$("#jijiucuo-info"));
            setTpl.oneTpl(jjcsD.infoE,$("#jijiucuo-tpl-E"),$("#jijiucuo-info-E"));
        })());

        //第五部分（消防措施）
        ((function () {
            var xfcs = contentD.xiaofangcuoshi;
            var xfcsD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(xfcs,xfcsD);
            var arr3 = initObj(xfcsD.infoC.yuanyuci.yuanyuci);
            if (arr3){
                xfcsD.infoC.yuanyuci = arr3;
            }
            var arr4 = initObj(xfcsD.infoC.duixiaofang.duixiaofang);
            if (arr4){
                xfcsD.infoC.duixiaofang = arr4;
            }

            var arr5 = initObjE(xfcsD.infoE.yuanyuci.yuanyuciE);
            if (arr5){
                xfcsD.infoE.yuanyuci = arr5;
            }
            var arr6 = initObjE(xfcsD.infoE.duixiaofang.duixiaofangE);
            if (arr6){
                xfcsD.infoE.duixiaofang = arr6;
            }
            setTpl.oneTpl(xfcsD.infoC,$("#xiaofangcuo-tpl"),$("#xiaofangcuo-info"));
            setTpl.oneTpl(xfcsD.infoE,$("#xiaofangcuo-tpl-E"),$("#xiaofangcuo-info-E"));
        })());

        //第六部分（泄露应急处理）
        ((function () {
            var xly = contentD.xielouyingji;
            var xlyD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(xly,xlyD);
            var arr5 = initObj(xlyD.infoC.huanjinbao.huanjinbao);
            if (arr5){
                xlyD.infoC.huanjinbao = arr5;
            }
            var arr6 = initObj(xlyD.infoC.xielouhua.xielouhua);
            if (arr6){
                xlyD.infoC.xielouhua = arr6;
            }
            var arr7 = initObj(xlyD.infoC.zuoyexiao.zuoyexiao);
            if (arr7){
                xlyD.infoC.zuoyexiao = arr7;
            }


            var arr8 = initObjE(xlyD.infoE.huanjinbao.huanjinbaoE);
            if (arr8){
                xlyD.infoE.huanjinbao = arr8;
            }
            var arr9 = initObjE(xlyD.infoE.xielouhua.xielouhuaE);
            if (arr9){
                xlyD.infoE.xielouhua = arr9;
            }
            var arr10 = initObjE(xlyD.infoE.zuoyexiao.zuoyexiaoE);
            if (arr10){
                xlyD.infoE.zuoyexiao = arr10;
            }
            setTpl.oneTpl( xlyD.infoE,$("#xielouyin-tpl-E"),$("#xielouyin-info-E"));
            setTpl.oneTpl( xlyD.infoC,$("#xielouyin-tpl"),$("#xielouyin-info"));
        })());

        //第七部分（操作与存储）
        ((function () {
            var czy = contentD.caozhuoyucunchu;
            var czyD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(czy,czyD);
            var arr8 = initObj(czyD.infoC.caozuozhu.caozuozhu);
            if (arr8){
                czyD.infoC.caozuozhu = arr8;
            }
            var arr9 = initObj(czyD.infoC.cunchuzhu.cunchuzhu);
            if (arr9){
                czyD.infoC.cunchuzhu = arr9;
            }

            var arr11 = initObjE(czyD.infoE.caozuozhu.caozuozhuE);
            if (arr11){
                czyD.infoE.caozuozhu = arr11;
            }
            var arr12 = initObjE(czyD.infoE.cunchuzhu.cunchuzhuE);
            if (arr12){
                czyD.infoE.cunchuzhu = arr12;
            }
            setTpl.oneTpl(czyD.infoC,$("#caozuoyu-tpl"),$("#caozuoyu-info"));
            setTpl.oneTpl(czyD.infoE,$("#caozuoyu-tpl-E"),$("#caozuoyu-info-E"));
        })());

        //第八部分（接触控制/个人防护）
        ((function () {
            var jck = contentD.jiechukongzhi;
            var jckD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(jck,jckD);
            var arr1 = initObj(jckD.infoC.puto.jiancefang);
            if (arr1){
                jckD.infoC.jiancefang = arr1;
            }
            var arr2 = initObj(jckD.infoC.puto.gongchengkong);
            if (arr2){
                jckD.infoC.gongchengkong = arr2;
            }

            var arr3 = initObjE(jckD.infoE.puto.jiancefangE);
            if (arr3){
                jckD.infoE.jiancefang = arr3;
            }
            var arr4 = initObjE(jckD.infoE.puto.gongchengkongE);
            if (arr4){
                jckD.infoE.gongchengkong = arr4;
            }
            setTpl.oneTpl(jckD.infoC,$("#jiechukong-tpl"),$("#jiechukong-info"));
            setTpl.oneTpl(jckD.infoE,$("#jiechukong-tpl-E"),$("#jiechukong-info-E"));
        })());

        //第九部分（物化特性）
        ((function () {
            var wht = contentD.wuhuatexing;
            var whtD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(wht,whtD);
            whtD.infoC.baozashang = initObj(whtD.infoC.baozashang.baozashang);
            whtD.infoE.baozashang = initObjE(whtD.infoE.baozashang.baozashangE);
            setTpl.oneTpl(whtD.infoC,$("#wuhuate-tpl"),$("#wuhuate-info"));
            setTpl.oneTpl(whtD.infoE,$("#wuhuate-tpl-E"),$("#wuhuate-info-E"));
        })());

        //第十部分（稳定性和反应性）
        ((function () {
            var wdx = contentD.wendingying;
            var wdxD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(wdx,wdxD);
            setTpl.oneTpl(wdxD.infoC,$("#wendingxing-tpl"),$("#wendingxing-info"));
            setTpl.oneTpl(wdxD.infoE,$("#wendingxing-tpl-E"),$("#wendingxing-info-E"));
        })());

        //第十一部分
        ((function () {
            var dlx = contentD.dulixue;
            var zzf = contentD.zufen.zufen;
            var zzfE = contentD.zufen.zufenE;
            var cas = contentD.zufen.CAS;
            dlx.zhiaixing.zzufen = zzf;
            dlx.zhiaixing.zzufenE = zzfE;
            var dlxD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(dlx,dlxD);
            dlxD.infoC.zhiaixing.cas = cas;
            dlxD.infoE.zhiaixing.cas = cas;
            dlxD.infoE.jixingdu.jcas = dlx.jixingdu.jcas;
            setTpl.oneTpl(dlxD.infoC,$("#dulixue-tpl"),$("#dulixue-info"));
            setTpl.oneTpl(dlxD.infoE,$("#dulixue-tpl-E"),$("#dulixue-info-E"));
        })());

        //第十二部分
        ((function () {
            var stx = contentD.shengtaixue;
            var stxD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(stx,stxD);
            setTpl.oneTpl(stxD.infoC,$("#shengtaixue-tpl"),$("#shengtaixue-info"));
            setTpl.oneTpl(stxD.infoE,$("#shengtaixue-tpl-E"),$("#shengtaixue-info-E"));
        })());

        //第十三部分
        ((function () {
            var fqc = contentD.feiqichuzhi;
            var fqcD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(fqc,fqcD);
            setTpl.oneTpl(fqcD.infoC,$("#feiqichu-tpl"),$("#feiqichu-info"));
            setTpl.oneTpl(fqcD.infoE,$("#feiqichu-tpl-E"),$("#feiqichu-info-E"));
        })());

        //第十四部分
        ((function () {
            var ysx = contentD.yunshuxingxi;
            var ysxD = {
                "infoE":{
                    "imgs":[]
                },
                "infoC":{
                    "imgs":[]
                }
            };
            var HandLeibie = {
                "weixianyingshuo":contentD.weixianbiaoshi.weixianyingshuo,
                "GHSweixianxingleibie":contentD.weixianbiaoshi.GHSweixianxingleibie
            };
            var HandLeibieD = {
                "infoE":{},
                "infoC":{}
            };
            jsons(HandLeibie,HandLeibieD);
            //标签（图片）
            var ysbq = contentD.weixianbiaoshi.yunshubiaoqian;
            for (key in ysbq){
                if (ysbq[key].trim().lastIndexOf('\\') !== -1){
                    var data = ysbq[key].trim().split('\\');
                    for (var i=0;i<data.length;i++){
                        if (ysxD.infoC.imgs.length >0){
                            var temp = [];
                            for (var j=0;j<ysxD.infoC.imgs.length;j++){
                                temp[ysxD.infoC.imgs[j]] = true;
                            }
                            if (!temp[data[i]]){
                                ysxD.infoC.imgs.push(data[i]);
                                ysxD.infoE.imgs.push(data[i]);
                            }
                        } else {
                            ysxD.infoC.imgs.push(data[i]);
                            ysxD.infoE.imgs.push(data[i]);
                        }
                    }
                }else if (ysbq[key].trim().lastIndexOf('\\') === -1 && ysbq[key].trim().lastIndexOf('/img') !== -1) {
                    if (ysxD.infoC.imgs.length >0){
                        var temp = [];
                        for (var j=0;j<ysxD.infoC.imgs.length;j++){
                            temp[ysxD.infoC.imgs[j]] = true;
                        }
                        if (!temp[ysbq[key].trim()]){
                            ysxD.infoC.imgs.push(ysbq[key].trim());
                            ysxD.infoE.imgs.push(ysbq[key].trim());
                        }
                    } else {
                        ysxD.infoC.imgs.push(ysbq[key].trim());
                        ysxD.infoE.imgs.push(ysbq[key].trim());
                    }
                }
            }

            jsons(ysx,ysxD);
            HandLeibieD.infoC.imgs = ysxD.infoC.imgs;
            HandLeibieD.infoE.imgs = ysxD.infoE.imgs;
            setTpl.oneTpl(ysxD.infoC,$("#yunshuxin-tpl"),$("#yunshuxin-info"));
            setTpl.oneTpl(ysxD.infoE,$("#yunshuxin-tpl-E"),$("#yunshuxin-info-E"));

            //GHS附加
            var H = [],
                HE = [],
                zhonglei = [],
                zhongleiE = [],
                leibieE = [],
                leibie = [];
            for (var key in HandLeibieD.infoC.GHSweixianxingleibie) {
                zhonglei.push(key);
                leibie.push(HandLeibieD.infoC.GHSweixianxingleibie[key]);
            }
            for (var key in HandLeibieD.infoC.weixianyingshuo) {
                H.push(key);
            }
            for (var key in HandLeibieD.infoE.GHSweixianxingleibie) {
                zhongleiE.push(key);
                leibieE.push(HandLeibieD.infoE.GHSweixianxingleibie[key]);
            }
            for (var key in HandLeibieD.infoE.weixianyingshuo) {
                HE.push(key);
            }
            HandLeibieD.infoC.H = H;
            HandLeibieD.infoC.zhonglei = zhonglei;
            HandLeibieD.infoC.leibie = leibie;
            HandLeibieD.infoE.HE = HE;
            HandLeibieD.infoE.zhongleiE = zhongleiE;
            HandLeibieD.infoE.leibieE = leibieE;
            setTpl.oneTpl(HandLeibieD.infoC,$("#GHS-xulun-tpl"),$("#GHS-xulun"));
        })());


        //第十五部分
        ((function () {
            var fgx = contentD.zufen;
            var tick = {};
            var zufenD = {};
            for (key in fgx){
                if (key.lastIndexOf("zufen")!==-1&&key!=="zufen"&&key.lastIndexOf("zufenC")===-1&&key.lastIndexOf("zufenE")===-1){
                    tick[key] = fgx[key];
                }
            }
            for (key in tick){
                var index = key.indexOf('n')+1;
                var k = key.substring(index);
                zufenD[k] = tick[key];
            }
            contentD.zufen.zufenD = zufenD;

            //中国法规
            var tickC = {};
            var zufenCD = {};
            for (key in fgx){
                if (key.lastIndexOf("zufenC")!==-1){
                    tickC[key] = fgx[key];
                }
            }
            for (key in tickC){
                var index = key.indexOf('C')+1;
                var k = key.substring(index);
                zufenCD[k] = tickC[key];
            }
            contentD.zufen.zufenD = zufenD;
            contentD.zufen.zufenCD = zufenCD;
            setTpl.oneTpl(contentD.zufen,$("#fagui-tpl"),$("#fagui-info"));
            setTpl.oneTpl(contentD.zufen,$("#fagui-tpl-E"),$("#fagui-info-E"));
        })());

        //16
        setTpl.oneTpl(contentD.zufen,$("#qita-tpl"),$("#qita-info"));
        setTpl.oneTpl(contentD.zufen,$("#qita-tpl-E"),$("#qita-info-E"));


        //导出
        //导出
        layer.confirm('请选择导出的文档类型', {
            btn: ['SDS设计报告','GHS标签']
        }, function(index){
            $.ajax({type:'get', url:'/Daochuleixing?name=SDS安全数据单导出'});
            $("#export-content-E").wordExport("SDS英文");
            $("#export-content").wordExport("SDS中文");
            // $("#export-home-page-view-SDS").wordExport("封面");
            // $("#export-zhuyi").wordExport("注意事项");
            layer.close(index);
        }, function(index){
            $.ajax({type:'get', url:'/Daochuleixing?name=GHS标签导出'});
            $("#export-ghs").wordExport("GHS文档");
            // $("#export-home-page-view-GHS").wordExport("封面");
            // $("#export-SHS-view").wordExport("GHS中文");
            // $("#GHS-xulun").wordExport("绪论");
            // $("#export-SHSE-view").wordExport("GHS英文");
            layer.close(index)
        });
        //  $("#export-content-E").wordExport("SDS英文");
        //  $("#export-content").wordExport("SDS中文");
        //  $("#export-home-page-view").wordExport("封面");
        //  $("#export-SHS-view").wordExport("GHS中文");
        //  $("#export-SHSE-view").wordExport("GHS英文");
        //  $("#export-zhuyi").wordExport("注意事项");
    }


    /**
     *
     * @param obj
     * @param newObj
     */
    function jsons(obj,newObj) {
        for (key in obj){
            newObj.infoE[key] = {};
            newObj.infoC[key] = {};
            for (k in obj[key]){
                if (k.lastIndexOf("E") !== -1) {
                    newObj.infoE[key][k] = obj[key][k];
                }else {
                    newObj.infoC[key][k] = obj[key][k];
                }
            }
        }
    }

    /**
     *拆分字符串
     * @param data
     * @returns {*}
     */
    function initObj(data){
        var arrs = [];
        if (data.lastIndexOf("。") !== -1){
            var info = data.split("。");
            info.forEach(function (item,index) {
                if (item){
                    arrs.push(item);
                }
            });
            return arrs;
        }else {
            arrs.push(data);
            return arrs;
        }
    }



    //差英文
    function initObjE(data){
        var arrs = [];
        if (data.lastIndexOf(".") !== -1){
            var info = data.split(".");
            info.forEach(function (item,index) {
                if (item){
                    arrs.push(item);
                }
            });
            return arrs;
        }else {
            arrs.push(data);
            return arrs;
        }
    }

    exports('exportDoc',exportView);
});