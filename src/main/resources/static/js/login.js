layui.define(['form','customAjax','layer','jquery'],function (exports) {
    var form = layui.form,
        customAjax = layui.customAjax,
    $ = layui.$;
    form.verify({
        username: function (value) {
            if (value.length === 0) {
                return '用户名不能为空';
            }
        },
        password: function (value) {
            if (value.length === 0) {
                return '密码不能为空';
            }
        }
    });

    //登录
    form.on('submit()',function (data) {
        customAjax.http('/userlogin',data.field,function (res) {
            console.log(res);
            if (res.code === 0){
                if (res.data.status === 0){
                    layer.msg("您已被停用，请联系管理员！",{
                        icon:2
                    })
                }else {
                    location.href = "/main";
                }
           } else {
               layer.msg("您密码错误请重新确认！",{
                   icon:2,
                   end:function () {
                       location.reload();
                   }
               })
           }
       });
       return false;
    });


    exports('login',{});
});