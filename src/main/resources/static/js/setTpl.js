layui.define(['laytpl','jquery'],function (exports) {
    var laytpl = layui.laytpl,
        $ = layui.$;
   var tpl = {
     oneTpl:function (msg,gettpl,view) {
         var data = {
             "list":[
             ]
         };
         data.list = [];
         data.list.push(msg);
         var getTpl = gettpl.html();
         laytpl(getTpl).render(data,function (html) {
             view.html(html);
         });
     },
      manyTpl:function () {
          
      } 
   };

    exports('setTpl',tpl);
});