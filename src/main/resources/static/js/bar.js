layui.define(['jquery','layer','form','customAjax'],function (exports) {
    var $ = layui.$,
        customAjax = layui.customAjax,
        layer = layui.layer,
    form = layui.form;
    function bar(){
        $(".updatePass").click(function () {
            layer.open({
                type: 1,
                content: $("#topUpdatePass"),
                area: ['50%'],
                shift: 1,
                maxmin: true,
                title: ['添加用户', 'font-size:22px;'],
                resize: false
            });
            form.on('submit(topUpdatePass)',function (data) {
                if (data.field.passwordNewA === data.field.passwordNewB){
                    var datas = {};
                    datas["passwordNew"] = data.field.passwordNewA;
                    datas["passwordOld"] = data.field.passwordOld;
                    customAjax.http('/updatePass',datas,function (res) {
                        console.log(res);
                        if (res.code === 0){
                            layer.msg("修改成功,请重新登录",{
                                icon:1,
                                time:1000,
                                end:function () {
                                    layer.closeAll('page');
                                    location.href = '/';
                                }
                            })
                        }else {
                            layer.msg("修改失败，请重新确认密码！",{
                                icon:2,
                                time:1000
                            })
                        }
                    });
                }else {
                    layer.msg("两次密码不同请重新确认",{
                        icon:2,
                        time:2000
                    })
                }
                return false;
            })
        });

        $(".logout").click(function () {
            $.ajax({
                type:'get',
                url:'/logout',
                dataType:'json',
                success:function (msg) {
                    if (msg.code === 0){
                        location.href = '/';
                    }
                },
                error:function (err) {

                }
            })
        });
    }

    form.verify({
        username: [
            /^[A-Za-z]{1}[A-Za-z0-9]{3,15}$/
            , '必须是以字母开头，必须包含字母数字，4到16位'
        ],
        password: [
            /^[\S]{6,12}$/
            , '密码必须6到12位，且不能出现空格'
        ]
    });

    exports('bar',bar);
});