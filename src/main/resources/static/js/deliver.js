layui.define(['layer', 'form', 'jquery','element','customWindows','customTable','customAjax','bar'], function (exports) {
    var layer = layui.layer,
        form = layui.form,
        $ = layui.$,
        element = layui.element,
        customTable = layui.customTable,
        customWindows = layui.customWindows,
        customAjax = layui.customAjax,
    bar = layui.bar;
    bar();


    //回车阻止默认事件
    $(document).keydown(function (event) {
        var e = event || window.event;
        if (e.keyCode === 13){
            e.preventDefault();
        }
    });

    //初始化页面
    var mainObj = {
        node:"#deliver-demo",
        url:"/findAllDeliver",
        limit:20,
        col:[//表头
            { field: 'cnname', title: '产品名称' }
            , { field: 'sqname', title: '申请单位' }
            , { field: 'sqtel', title: '申请单位电话' }
            , { field: 'gysname', title: '供应商名称' }
            , { field: 'gystel', title: '供应商联系电话' }
            , { field: 'createtime', title: '送样时间' }
            , { title: '操作', toolbar: '#barDemo' }
        ],
        delUrl:"/deleteDeliver",
        //弹出层参数
        content:$("#insert-content"),
        title:'送样详情',
        height:"90%",
        width:"90%",
        http:"/xiangQing",
        filter:"editInfo"
    };
    var tableIns = customTable(mainObj);

    //搜索
    $(".search-btn").click(function () {
        if ($(".search-input").val().length) {
            var searchObj = {
                node:"#search-demo",
                url:"/findDeliver?tick="+$(".search-input").val(),
                limit:20,
                col:[//表头
                    { field: 'cnname', title: '产品名称' }
                    , { field: 'sqname', title: '申请单位' }
                    , { field: 'sqtel', title: '申请单位电话' }
                    , { field: 'gysname', title: '供应商名称' }
                    , { field: 'gystel', title: '供应商联系电话' }
                    , { field: 'createtime', title: '送样时间' }
                    , { title: '操作', toolbar: '#barDemo' }
                ],
                delUrl:"/deleteDeliver",
                //弹出层参数
                content:$("#insert-content"),
                title:'送样详情',
                height:"90%",
                width:"90%",
                http:"/xiangQing",
                filter:"editInfo"
            };
            //渲染数据
            customTable(searchObj);
            //使用弹窗显示
            customWindows();
        } else {
            layer.msg('请输入搜索的内容')
        }
    });

    //点击添加送样信息(第二层)
    $(".my-btn").click(function (e) {
        $(".Edit-btn").css("display","none");
        $(".Submit-btn").css("display","block");
        form.val('editInfo',{
            "id":'',
            "bianhao":'',
            "shuliang":'',
            "jiancheyiju":'',
            "yangpingbiaoshi":'',
            "yangpingchenfen":'',
            "yangpingxingzhuang":'',
            "jianguanren":'',
            "yewulei":'',
            "jianceri":'',
            "yangpinxin":'',
            "jishuming":'',
            "shuyu":'',
            "fuhe":'',
            "cnname":'',
            "usname":'',
            "biename":'',
            "casno":'',
            "ecno":'',
            "fenzisi":'',
            "tuijianyongtu":'',
            "xianzhiyongtu":'',
            "sqname":'',
            "sqadd":'',
            "sqcode":'',
            "sqtel":'',
            "sqfax":'',
            "sqyouxiang":'',
            "gysname":'',
            "gysadd":'',
            "gyscode":'',
            "gystel":'',
            "gysfax":'',
            "gysyouxiang":'',
            "qiyetel":''
        });
        layer.open({
            type: 1,
            content: $("#insert-content"),
            area: ['90%', '90%'],
            shift: 1,
            maxmin: true,
            title: ['输入公司送样信息', 'font-size:22px;'],
            resize: false,
            cancel:function (index) {
                $("#addDeliverInfo")[0].reset();
            },
            end:function () {
                $("#addDeliverInfo")[0].reset();
            }
        });
    });

    //表单提交送样信息
    form.on('submit(formSubmit)', function (data) {
        console.log(data);
        if (data.field) {
            var index = layer.load(0);
            customAjax.http("/submitDeliver",data.field,function (res) {
                if (res.code === 0) {
                    form.val('editInfo',{
                        "id":info.id,
                        "bianhao":info.bianhao,
                        "shuliang":info.shuliang,
                        "jiancheyiju":info.jiancheyiju,
                        "yangpingbiaoshi":info.yangpingbiaoshi,
                        "yangpingchenfen":info.yangpingchenfen,
                        "yangpingxingzhuang":info.yangpingxingzhuang,
                        "jianguanren":info.jianguanren,
                        "yewulei":info.yewulei,
                        "jianceri":info.jianceri,
                        "yangpinxin":info.yangpinxin,
                        "jishuming":info.jishuming,
                        "shuyu":info.shuyu,
                        "fuhe":info.fuhe,
                        "cnname":info.cnname,
                        "usname":info.usname,
                        "biename":info.biename,
                        "casno":info.casno,
                        "ecno":info.ecno,
                        "fenzisi":info.fenzisi,
                        "tuijianyongtu":info.tuijianyongtu,
                        "xianzhiyongtu":info.xianzhiyongtu,
                        "sqname":info.sqname,
                        "sqadd":info.sqadd,
                        "sqcode":info.sqcode,
                        "sqtel":info.sqtel,
                        "sqfax":info.sqfax,
                        "sqyouxiang":info.sqyouxiang,
                        "gysname":info.gysname,
                        "gysadd":info.gysadd,
                        "gyscode":info.gyscode,
                        "gystel":info.gystel,
                        "gysfax":info.gysfax,
                        "gysyouxiang":info.gysyouxiang,
                        "qiyetel":info.qiyetel
                    });
                    layer.confirm(
                        "你添加的送样信息已经存在，请点击查看"
                        ,{btn: ['查看','取消']}
                        ,function (index) {
                            //进入查看页面
                            layer.closeAll('page');
                            layer.open({
                                type: 1,
                                content: $("#insert-content"),
                                area: ['90%', '90%'],
                                shift: 1,
                                maxmin: true,
                                title: ['公司送样信息详情', 'font-size:22px;'],
                                resize: false
                            });
                            layer.close(index);
                        });
                    layer.close(index);
                } else {
                    layer.close(index);
                    //信息不存在请输入
                    layer.open({
                        type: 1,
                        content: $("#insert-H"),
                        area: ['50%', '50%'],
                        shift: 1,
                        closeBtn:0,
                        title: ['请输入输入危险性说明编号', 'font-size:22px;'],
                        resize: false,
                        cancel:function (index) {
                            $("#addInsertH")[0].reset();
                        }
                    });
                }
            });
        }
        return false;
    });

    //H开头的编码  ------添加
    $(".formDemo-H-add").click(function () {
        var index = $(this).parents("#addInsertH").children("#H-content").children().length;
        $("#H-content").append(createElement(index+1));
        form.render();
    });
    function createElement(index){
        var item = $(" <div class=\"layui-form-item\">\n" +
            "                            <label class=\"layui-form-label\"><span class=\"layui-icon layui-icon-close close-btn\" title=\"删除一条\"></span>编号1"+index+"</label>\n" +
            "                            <div class=\"layui-input-block\">\n" +
            "                                <select name=\"H[]\" lay-search lay-verify=\"required\">\n" +
            "                                    <option value=\"\">请搜索危险性说明编号</option>\n" +
            "                                    <option value=\"1\">H200</option>\n" +
            "                                    <option value=\"2\">H201</option>\n" +
            "                                    <option value=\"3\">H202</option>\n" +
            "                                    <option value=\"4\">H203</option>\n" +
            "                                    <option value=\"5\">H204</option>\n" +
            "                                    <option value=\"6\">H205</option>\n" +
            "                                    <option value=\"7\">无(类别：爆炸物,危险分类：第1.6项)</option>\n" +
            "                                    <option value=\"8\">H220</option>\n" +
            "                                    <option value=\"9\">H220H232</option>\n" +
            "                                    <option value=\"10\">H220H230</option>\n" +
            "                                    <option value=\"11\">H220H231</option>\n" +
            "                                    <option value=\"12\">H221(危险分类：1B,信号词：危险)</option>\n" +
            "                                    <option value=\"13\">H221(危险分类：2,信号词：警告)</option>\n" +
            "                                    <option value=\"14\">H222H229</option>\n" +
            "                                    <option value=\"15\">H223H229</option>\n" +
            "                                    <option value=\"16\">H229</option>\n" +
            "                                    <option value=\"17\">H270</option>\n" +
            "                                    <option value=\"18\">H280(危险分类：压缩气体)</option>\n" +
            "                                    <option value=\"19\">H280(危险分类：液化气体)</option>\n" +
            "                                    <option value=\"20\">H281</option>\n" +
            "                                    <option value=\"21\">H280(危险分类：溶解气体)</option>\n" +
            "                                    <option value=\"22\">H224</option>\n" +
            "                                    <option value=\"23\">H225</option>\n" +
            "                                    <option value=\"24\">H226</option>\n" +
            "                                    <option value=\"25\">H227</option>\n" +
            "                                    <option value=\"26\">H228(危险分类：1,信号词：危险)</option>\n" +
            "                                    <option value=\"27\">H228(危险分类：2,信号词：警告)</option>\n" +
            "                                    <option value=\"28\">H240(危险类别：自反应物质和混合物)</option>\n" +
            "                                    <option value=\"29\">H241(危险类别：自反应物质和混合物)</option>\n" +
            "                                    <option value=\"30\">H242(危险类别：自反应物质和混合物,危险分类：C型和D型,信号词：危险)</option>\n" +
            "                                    <option value=\"31\">H242(危险类别：自反应物质和混合物,危险分类：E型和F型,信号词：警告)</option>\n" +
            "                                    <option value=\"32\">无(危险类别：自反应物质和混合物,危险分类：G型)</option>\n" +
            "                                    <option value=\"33\">H250(危险类别：发火液体)</option>\n" +
            "                                    <option value=\"34\">H250(危险类别：发火固体)</option>\n" +
            "                                    <option value=\"35\">H251</option>\n" +
            "                                    <option value=\"36\">H252</option>\n" +
            "                                    <option value=\"37\">H260</option>\n" +
            "                                    <option value=\"38\">H261(危险分类：2,信号词：危险)</option>\n" +
            "                                    <option value=\"39\">H261(危险分类：3,信号词：警告)</option>\n" +
            "                                    <option value=\"40\">H271(危险类别：氧化性液体)</option>\n" +
            "                                    <option value=\"41\">H272(危险分类：2,信号词：危险)</option>\n" +
            "                                    <option value=\"42\">H272(危险分类：3,信号词：警告)</option>\n" +
            "                                    <option value=\"43\">H271(危险类别：氧化性固体)</option>\n" +
            "                                    <option value=\"44\">H272(危险分类：2,信号词：危险)</option>\n" +
            "                                    <option value=\"45\">H272(危险分类：3,信号词：警告)</option>\n" +
            "                                    <option value=\"46\">H240(危险类别：有机过氧化物)</option>\n" +
            "                                    <option value=\"47\">H241(危险类别：有机过氧化物)</option>\n" +
            "                                    <option value=\"48\">H242(危险类别：有机过氧化物,危险分类：C型和D型,信号词：危险)</option>\n" +
            "                                    <option value=\"49\">H242(危险类别：有机过氧化物,危险分类：E型和F型,信号词：警告)</option>\n" +
            "                                    <option value=\"50\">无(危险类别：有机过氧化物,危险分类：G型)</option>\n" +
            "                                    <option value=\"51\">H290</option>\n" +
            "                                    <option value=\"52\">H206</option>\n" +
            "                                    <option value=\"53\">H207(危险分类：2,信号词：危险)</option>\n" +
            "                                    <option value=\"54\">H207(危险分类：3,信号词：警告)</option>\n" +
            "                                    <option value=\"55\">H208</option>\n" +
            "                                    <option value=\"56\">H300+H310</option>\n" +
            "                                    <option value=\"57\">H300+H330</option>\n" +
            "                                    <option value=\"58\">H310+H330</option>\n" +
            "                                    <option value=\"59\">H300+H310+H330</option>\n" +
            "                                    <option value=\"60\">H301+H311</option>\n" +
            "                                    <option value=\"61\">H301+H331</option>\n" +
            "                                    <option value=\"62\">H311+H331</option>\n" +
            "                                    <option value=\"63\">H301+H311+H331</option>\n" +
            "                                    <option value=\"64\">H302+H312</option>\n" +
            "                                    <option value=\"65\">H302+H332</option>\n" +
            "                                    <option value=\"66\">H312+H332</option>\n" +
            "                                    <option value=\"67\">H302+H312+H332</option>\n" +
            "                                    <option value=\"68\">H303+H313</option>\n" +
            "                                    <option value=\"69\">H303+H333</option>\n" +
            "                                    <option value=\"70\">H313+H333</option>\n" +
            "                                    <option value=\"71\">H303+H313+H333</option>\n" +
            "                                    <option value=\"72\">H315+H320</option>\n" +
            "                                    <option value=\"73\">H314</option>\n" +
            "                                    <option value=\"74\">H315</option>\n" +
            "                                    <option value=\"75\">H316</option>\n" +
            "                                    <option value=\"76\">H318</option>\n" +
            "                                    <option value=\"77\">H319</option>\n" +
            "                                    <option value=\"78\">H320</option>\n" +
            "                                    <option value=\"79\">H334(危险分类：1)</option>\n" +
            "                                    <option value=\"80\">H334(危险分类：1Aa)</option>\n" +
            "                                    <option value=\"81\">H334(危险分类：1Ba)</option>\n" +
            "                                    <option value=\"82\">H317(危险分类：1)</option>\n" +
            "                                    <option value=\"83\">H317(危险分类：1Aa)</option>\n" +
            "                                    <option value=\"84\">H317(危险分类：1Ba)</option>\n" +
            "                                    <option value=\"85\">H340</option>\n" +
            "                                    <option value=\"86\">H341</option>\n" +
            "                                    <option value=\"87\">H350</option>\n" +
            "                                    <option value=\"88\">H351</option>\n" +
            "                                    <option value=\"89\">H360</option>\n" +
            "                                    <option value=\"90\">H361</option>\n" +
            "                                    <option value=\"91\">H362</option>\n" +
            "                                    <option value=\"92\">H370</option>\n" +
            "                                    <option value=\"93\">H371</option>\n" +
            "                                    <option value=\"94\">H335H336</option>\n" +
            "                                    <option value=\"95\">H372</option>\n" +
            "                                    <option value=\"96\">H373</option>\n" +
            "                                    <option value=\"97\">H304</option>\n" +
            "                                    <option value=\"98\">H305</option>\n" +
            "                                    <option value=\"99\">H400</option>\n" +
            "                                    <option value=\"100\">H401</option>\n" +
            "                                    <option value=\"101\">H402</option>\n" +
            "                                    <option value=\"102\">H410</option>\n" +
            "                                    <option value=\"103\">H411</option>\n" +
            "                                    <option value=\"104\">H412</option>\n" +
            "                                    <option value=\"105\">H413</option>\n" +
            "                                    <option value=\"106\">H420</option>\n" +
            "                                </select>\n" +
            "                            </div>\n" +
            "                        </div>");
        return item;
    }


    //删除
    $("#insert-H").delegate(".close-btn","click",function () {
        $(this).parents(".layui-form-item").remove();
    });

    form.on('submit(formDemo-H)', function (data) {

        if (data.field) {
            var d = {};
            var ds = [];
            for(k in data.field){
                ds.push(data.field[k]);
            }

            d["data"] = ds;
            var index = layer.load(0);

            $.ajax({
                type:'post',
                url:'/H1',
                dataType:"json",
                contentType: "application/json; charset=utf-8",
                data:JSON.stringify(d),
                success:function (msg) {
                    if (msg.code === 0){
                        localStorage.setItem("autoInfo",JSON.stringify(msg.data));
                        localStorage.setItem("name",msg.data[0].name);
                        localStorage.setItem("yunshubiaoqian",JSON.stringify(msg.data[0].yunshubiaoqian));
                        location.href = '/insertDasic';
                        layer.close(index);
                    }
                },
                error:function (err) {
                    layer.msg("服务器异常,请稍后重试");
                }
            })
        }
        return false;
    });

    //修改信息
    form.on('submit(formEdit)',function (data) {
        customAjax.http('/updateDeliver',data.field,function (res) {
            if (res.code === 0){
                layer.msg("修改成功",
                    {
                        icon: 1,
                        time:1000,
                        end:function () {
                            layer.closeAll('page');
                            tableIns.reload();
                        }
                    });
            }else {
                layer.msg("修改失败稍后重试！",
                    {
                        icon: 2,
                        time:1000,
                        end:function () {
                            layer.closeAll('page');
                            tableIns.reload();
                        }
                    });
            }
        });
        return false;
    });


    //自定义表单验证规则
    form.verify({
        customEmail:function (value) {
            if (value){
                if (!(/^[a-z0-9]{1}[a-z0-9_-]{1,}@[a-z0-9]{1,}(\.[a-z]{2,})*\.[a-z]{2,}$/.test(value))){
                    return "邮箱格式错误"
                }
            }
        }
    });

    exports('deliver', {})
});