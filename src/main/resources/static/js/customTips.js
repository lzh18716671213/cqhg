
layui.define(['jquery', 'layer'], function (exports) {
    var $ = layui.$,
        layer = layui.layer;

    $(".doc-content").delegate(".tip-input", 'propertychange input', function () {
        $(".tip-content").stop().slideUp(50);
        var that = $(this);
        if (that.val().length < 1) {
            $(".tip-content").stop().slideUp(50).html("");
        } else {
            $.ajax({
                type: 'post',
                url: "/lianxiang",
                dataType: 'json',
                data: {"val": that.val(),"zdName":that.attr('name'),"conName":that.parents(".page-style").attr("id")},
                success: function (msg) {
                    if (msg.code === 0) {
                        if (msg.data.length >0){
                            that.parent().parent().find(".tip-content").html("");
                            //渲染  数组
                            for (var i = 0; i < msg.data.length; i++) {
                                $(".tip-content").append(createNode(msg.data[i]));
                            }
                            that.parent().parent().find(".tip-content").stop().slideDown(50);
                        } else {
                            that.parent().parent().find(".tip-content").html("");
                            $(".tip-content").append(createNode("暂无"));
                            that.parent().parent().find(".tip-content").stop().slideDown(50);
                        }
                    } else {
                        that.parent().parent().find(".tip-content").html("");
                        $(".tip-content").append(createNode("暂无"));
                        that.parent().parent().find(".tip-content").stop().slideDown(50);
                    }
                },
                error: function (err) {
                    layer.msg("服务器异常，请稍后重试！")
                }
            });
        }

    });
    $(".doc-content").delegate(".tip-item", "click", function () {
        $(this).parent().parent().find(".tip-input").val($(this).text());
        $(".tip-content").stop().slideUp(50).html("");
    });

    function createNode(data){
        var item = $("<div class=\"tip-item\" style=\"cursor:pointer\">"+data+"</div>");
        return item;
    }

    //回车阻止默认事件
    $(document).keydown(function (event) {
        var e = event || window.event;
        if (e.keyCode === 13){
            e.preventDefault();
            $(".tip-content").stop().slideUp(50).html("");
        }
    });

    exports('customTips', {});
});