layui.define(['jquery','layer','form','element','customAjax','setTpl'],function (exports) {
    var  form = layui.form,
        layer = layui.layer,
        $ = layui.$,
        element = layui.element,
        customAjax = layui.customAjax,
        setTpl = layui.setTpl;

    //刷新清除数据
    window.onbeforeunload = function() {
        //用户点击浏览器右上角关闭按钮或是按alt+F4关闭
        if(event.clientX>document.body.clientWidth&&event.clientY<0||event.altKey){
            // 点关闭按钮
            localStorage.clear();
        }
        //用户点击任务栏，右键关闭。s或是按alt+F4关闭
        else if(event.clientY > document.body.clientHeight || event.altKey) {
            // 确定要退出本页吗;
            localStorage.clear();
        }
        else {
            //刷新页面
            localStorage.clear();
        }
    };

    //左边动态导航
    var allPage = $(".page-style");
    for (var j=0;j<allPage.length;j++){
        allPage[j].num = j;
    }
    var allNavbtn = $(".left-bar");
    for (var i = 0; i < allNavbtn.length; i++) {
        allNavbtn[i].num = i;
        $(allNavbtn[i]).click(function () {
            var num = this.num;
            var height = 0;
            for(var i=0;i<num;i++){
                height+=$(allPage[i]).height();
            }
            height+=num*10;
            $(".mainContent").animate({"scrollTop":height+"px"},500,function () {

            });
        });
    }

    //危险标识
    $(".weixianbiaoAdd").click(function () {
        if (getData('weixianbiao')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#weixianbiao-add"),'危险标识');
    });
    form.on('submit(weixianbiao)',function (data) {
        layer.confirm("确认添加吗？",{
            yes:function (index) {
                var autoInfo = JSON.parse(localStorage.getItem("autoInfo"));
                isNull(data.field);
                autoInfo[0].userInsert=data.field;
                $.ajax({
                    url:'/weixian',
                    type:"post",
                    dataType:"json",
                    contentType: "application/json; charset=utf-8",
                    data:JSON.stringify(autoInfo[0]),
                    success:function (msg) {
                        if (msg.code === 0){
                            layer.msg("添加成功",{
                                icon: 1,
                                time: 1000,
                                end:function () {
                                    localStorage.setItem("weixianbiao",JSON.stringify(data.field));
                                    //格式化GHS标签要素
                                    var wxbD = {
                                        "imgs":[],
                                        "xhc":[]
                                    };
                                    var cons = autoInfo[0].GHSbiaoqianyaoshu;
                                    for (key in cons){
                                        if (key.trim().lastIndexOf('象形图') !== -1){
                                            if (cons[key].trim().lastIndexOf('\\') !== -1){
                                                var d = cons[key].trim().split('\\');
                                                for (var i=0;i<d.length;i++){
                                                    if (wxbD.imgs.length >0){
                                                        var temp = [];
                                                        for (var j=0;j<wxbD.imgs.length;j++){
                                                            temp[wxbD.imgs[j]] = true;
                                                        }
                                                        if (!temp[d[i]]){
                                                            wxbD.imgs.push(d[i]);
                                                        }
                                                    } else {
                                                        wxbD.imgs.push(d[i]);
                                                    }
                                                }
                                            }else if (cons[key].trim().lastIndexOf('\\') === -1 && cons[key].trim().lastIndexOf('/img') !== -1) {
                                                if (wxbD.imgs.length >0){
                                                    var temp = [];
                                                    for (var j=0;j<wxbD.imgs.length;j++){
                                                        temp[wxbD.imgs[j]] = true;
                                                    }
                                                    if (!temp[cons[key].trim()]){
                                                        wxbD.imgs.push(cons[key].trim());
                                                    }
                                                } else {
                                                    wxbD.imgs.push(cons[key].trim());
                                                }
                                            }
                                        } else {  // 信号词
                                            wxbD.xhc.push(cons[key].trim());
                                        }
                                    }
                                    autoInfo[0].wxbD = wxbD;
                                    setTpl.oneTpl(autoInfo[0],$("#weixianbiao-tpl"),$("#weixianbiao-view"));
                                }
                            })
                        }
                    },
                    error:function (err) {
                        layer.msg("服务器异常,请稍后重试");
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });



    //组成/成分
    $(".chengfenzuAdd").click(function () {
        if (getData('chengfenzu')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#chengfenzu-add"),'组成 / 成分信息');
    });

    //继续添加按钮
    $(".addItem").click(function () {
        $("#tbody").append(createElement());
    });
    function createElement(){
        var item = $(" <tr>\n" +
            "                                        <td><input type=\"text\" name=\"zufen[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "                                        <td><input type=\"text\" name=\"hanl[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "                                        <td><input type=\"text\" name=\"CAS[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "                                        <td><input type=\"text\" name=\"EC[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "                                        <td><button type=\"button\" class=\"layui-btn  layui-btn-xs layui-btn-danger chengfenzu-del\" title=\"删除当前一条\">删除</button></td>\n" +
            "                                    </tr>");
        return item;
    }
    //删除
    $("#tbody").delegate(".chengfenzu-del",'click',function () {
        $(this).parents("tr").remove();
    });
    form.on("submit(chengfenzu)",function (data) {
        layer.confirm("确认添加吗？",{
            yes:function (index) {
                var cfd = {
                    "name":[],
                    "zufen":[],
                    "hanl":[],
                    "CAS":[],
                    "EC":[],
                    "n":[]
                };
                for (key in data.field){
                    if (key.lastIndexOf("zufen") !== -1){
                        cfd.zufen.push(data.field[key]);
                    }
                    if (key.lastIndexOf("hanl") !== -1) {
                        cfd.hanl.push(data.field[key]);
                    }
                    if (key.lastIndexOf("CAS") !== -1){
                        cfd.CAS.push(data.field[key]);
                    }
                    if (key.lastIndexOf("EC") !== -1){
                        cfd.EC.push(data.field[key]);
                    }
                }
                cfd.n.push(cfd.zufen.length);  //法规个数
                $.ajax({
                    url:'/chengFen',
                    type:"post",
                    dataType:"json",
                    contentType: "application/json; charset=utf-8",
                    data:JSON.stringify(cfd),
                    success:function (msg) {
                        if (msg.code === 0){
                            layer.msg("添加成功",{
                                icon: 1,
                                time: 1000,
                                end:function () {
                                    localStorage.setItem("chengfenzu",JSON.stringify(cfd));
                                    setTpl.oneTpl(cfd,$("#chengfenzu-tpl"),$("#chengfenzu-view"));
                                }
                            })
                        }
                    },
                    error:function (err) {
                        layer.msg("服务器异常,请稍后重试");
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });



    //急救措施
    $(".jiJiuAdd").click(function () {
        if (getData('jiJiuCuoShi')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#jiJiuCuoShi-add-edit"),'急救措施信息');
    });
    form.on('submit(jiJiuCuoShi)',function (data) {
        layer.confirm("确认添加吗？",{
            yes:function (index) {
                isNull(data.field);
                customAjax.http('/jijiucuoshi', data.field, function (res) {
                    if (res.code === 0) {
                        layer.msg('添加成功', {
                            icon: 1,
                            time: 1000,
                            end:function () {
                                var arr1 = initObj(data.field.zuizhongyao);
                                if (arr1){
                                    data.field.zuizhongyao = arr1;
                                }
                                var arr2 = initObj(data.field.jinjiyi);
                                if (arr2){
                                    data.field.jinjiyi = arr2;
                                }
                                localStorage.setItem("jiJiuCuoShi",JSON.stringify(data.field));
                                setTpl.oneTpl(data.field,$("#jiJiuCuoShi-tpl"),$("#jiJiuCuoShi-view"));
                            }
                        })
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false
    });


    //消防措施
    $(".xiaoFangAdd").click(function () {
        if (getData('xiaoFang')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#xiaoFang-add-edit"),'消防措施');
    });
    form.on('submit(xiaoFang)',function (data) {
        layer.confirm("确认添加吗？",{
            yes:function (index) {
                isNull(data.field);
                customAjax.http('/xiaofangcuoshi', data.field, function (res) {
                    if (res.code === 0) {
                        layer.msg('添加成功', {
                            icon: 1,
                            time: 1000,
                            end:function () {
                                var arr1 = initObj(data.field.yuanyuci);
                                if (arr1){
                                    data.field.yuanyuci = arr1;
                                }
                                var arr2 = initObj(data.field.duixiaofang);
                                if (arr2){
                                    data.field.duixiaofang = arr2;
                                }
                                localStorage.setItem("xiaoFang",JSON.stringify(data.field));
                                setTpl.oneTpl(data.field,$("#xiaoFangTpl"),$("#xiaofang-view"));
                            }
                        })
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //泄露应急处理
    $(".xieLouAdd").click(function () {
        if (getData('xielou')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#xieLou-add-edit"),'泄露应急处理');
    });
    form.on('submit(xielou)',function (data) {
        layer.confirm("确认添加吗？",{
            yes:function (index) {
                isNull(data.field);
                customAjax.http('/xielouyingji', data.field, function (res) {
                    if (res.code === 0) {
                        layer.msg('添加成功', {
                            icon: 1,
                            time: 1000,
                            end:function () {
                                var arr1 = initObj(data.field.zuoyexiao);
                                if (arr1){
                                    data.field.zuoyexiao = arr1;
                                }
                                var arr2 = initObj(data.field.huanjinbao);
                                if (arr2){
                                    data.field.huanjinbao = arr2;
                                }
                                var arr3 = initObj(data.field.xielouhua);
                                if (arr3){
                                    data.field.xielouhua = arr3;
                                }
                                localStorage.setItem("xielou",JSON.stringify(data.field));
                                setTpl.oneTpl(data.field,$("#xielou-tpl"),$("#xielou-view"));
                            }
                        })
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //操作与存储
    $(".caoZuo-add").click(function () {
        if (getData('caozuo')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#caoZuo-add-edit"),'操作与存储');
    });
    form.on('submit(caozuo)',function (data) {
        layer.confirm("确认添加吗？",{
            btn:['确定','取消'],
            yes:function (index) {
                isNull(data.field);
                customAjax.http('/caozhuoyucunchu', data.field, function (res) {
                    if (res.code === 0) {
                        layer.msg('添加成功', {
                            icon: 1,
                            time: 1000,
                            end:function () {
                                var arr1 = initObj(data.field.caozuozhu);
                                if (arr1){
                                    data.field.caozuozhu = arr1;
                                }
                                var arr2 = initObj(data.field.cunchuzhu);
                                if (arr2){
                                    data.field.cunchuzhu = arr2;
                                }
                                localStorage.setItem("caozuo",JSON.stringify(data.field));
                                setTpl.oneTpl(data.field,$("#caoZuo-tpl"),$("#caoZuo-view"));
                            }
                        })
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //接触控制与人身保护

    //继续添加按钮
    $(".zhiye-addItem").click(function () {
        $("#zhiye-body").append(SzhiYecreateElement());
    });
    function SzhiYecreateElement(){
        var item = $(" <tr>\n" +
            "                                        <td>\n" +
            "                                            <input type=\"text\" name=\"Jzufen[]\" lay-verify=\"required\"\n" +
            "                                                   autocomplete=\"off\" class=\"layui-input\">\n" +
            "                                        </td>\n" +
            "                                        <td>\n" +
            "                                            <input type=\"text\" name=\"biaozhunlai[]\" lay-verify=\"required\"\n" +
            "                                                   autocomplete=\"off\" class=\"layui-input\">\n" +
            "                                        </td>\n" +
            "                                        <td colspan=\"2\">\n" +
            "                                            <table border=\"1\" cellspacing=\"0\" width=\"100%\">\n" +
            "                                                <tbody>\n" +
            "                                                <tr>\n" +
            "                                                    <td width=\"130px\">\n" +
            "                                                        <input type=\"text\" name=\"leixin[]\" lay-verify=\"required\"\n" +
            "                                                                              autocomplete=\"off\" class=\"layui-input\">\n" +
            "                                                    </td>\n" +
            "                                                    <td>\n" +
            "                                                        <input type=\"text\" name=\"biaozhunzhi[]\" lay-verify=\"required\"\n" +
            "                                                               autocomplete=\"off\" class=\"layui-input\">\n" +
            "                                                    </td>\n" +
            "                                                </tr>\n" +
            "                                                <tr>\n" +
            "                                                    <td>\n" +
            "                                                        <input type=\"text\" name=\"leixin[]\" lay-verify=\"required\"\n" +
            "                                                               autocomplete=\"off\" class=\"layui-input\">\n" +
            "                                                    </td>\n" +
            "                                                    <td>\n" +
            "                                                        <input type=\"text\" name=\"biaozhunzhi[]\" lay-verify=\"required\"\n" +
            "                                                               autocomplete=\"off\" class=\"layui-input\">\n" +
            "                                                    </td>\n" +
            "                                                </tr>\n" +
            "                                                </tbody>\n" +
            "                                            </table>\n" +
            "                                        </td>\n" +
            "                                        <td>\n" +
            "                                            <input type=\"text\" name=\"beizhu[]\" lay-verify=\"required\" autocomplete=\"off\" class=\"layui-input\">\n" +
            "                                        </td>\n" +
            "                                        <td>\n" +
            "                                            <button type=\"button\" class=\"layui-btn layui-btn-xs layui-btn-danger zhiye-del\">删除</button>\n" +
            "                                        </td>\n" +
            "                                    </tr>");
        return item;
    }
    //删除
    $("#zhiye-body").delegate(".zhiye-del",'click',function () {
        var that = this;
        layer.confirm('确定删除吗？',{
            yes:function (i) {
                $(that).parents("tr").remove();
                layer.close(i);
            }
        })

    });

    $(".jiecukongAdd").click(function () {
        if (getData('jiechukong')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#jiecukong-add"),'接触控制与人身保护');
    });

    form.on("submit(jiechukong)",function (data) {
        layer.confirm("确认添加吗？",{
            btn:['确定','取消'],
            yes:function (index) {
                var arrS = {};//双个类别
                var puto = {}; //普通数据
                var allData = {
                    "zhiyejie":{
                        "Jzufen":[],
                        "beizhu":[],
                        "biaozhunlai":[],
                        "leixin":[],
                        "biaozhunzhi":[]
                    }
                };
                for (var key in data.field){
                    if (key.lastIndexOf('[') !== -1){
                        arrS[key] = data.field[key];
                    }else {
                        if (data.field[key] === ""){
                            data.field[key] = "无资料";
                        }
                        puto[key] = data.field[key];
                    }
                }
                allData.puto = puto;

                //判断直接接触是否为空
                if ($.isEmptyObject(arrS)){
                    allData.zhiyejie = "无资料"
                } else {
                    for (var key in arrS){
                        if (key.lastIndexOf("Jzufen") !== -1){
                            allData.zhiyejie.Jzufen.push(arrS[key]);
                        }
                        if (key.lastIndexOf("beizhu") !== -1) {
                            allData.zhiyejie.beizhu.push(arrS[key]);
                        }
                        if (key.lastIndexOf("biaozhunlai") !== -1){
                            allData.zhiyejie.biaozhunlai.push(arrS[key]);
                        }
                        if (key.lastIndexOf("leixin") !== -1){
                            allData.zhiyejie.leixin.push(arrS[key]);
                        }
                        if (key.lastIndexOf("biaozhunzhi") !== -1){
                            allData.zhiyejie.biaozhunzhi.push(arrS[key]);
                        }
                    }
                }

                var arr1 = initObj(allData.puto.jiancefang);
                if (arr1){
                    allData.puto.jiancefang = arr1;
                }
                var arr2 = initObj(allData.puto.gongchengkong);
                if (arr2) {
                    allData.puto.gongchengkong = arr2;
                }
                $.ajax({
                    type:"post",
                    url:'/jiechukongzhi',
                    dataType:"json",
                    contentType: "application/json; charset=utf-8",
                    data:JSON.stringify(allData),
                    success:function (msg) {
                        if (msg.code === 0){
                            layer.msg("添加成功",{
                                icon: 1,
                                time: 1000,
                                end:function () {
                                    localStorage.setItem("jiechukong",JSON.stringify(data.field));
                                    setTpl.oneTpl(allData,$("#jiecukong-tpl"),$("#jiecukong-view"));
                                }
                            })
                        }else {
                            layer.msg("修改失败，稍后重试！", {
                                icon: 2
                            })
                        }
                    },
                    error:function (err) {
                        layer.msg("服务器繁忙,请稍后重试");
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //物理和化学特性
    $(".wuhuateAdd").click(function () {
        if (getData('wuhuate')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#wuhuate-add"),'物理和化学特性');
    });
    form.on('submit(wuhuate)',function (data) {
        layer.confirm("确认添加吗？",{
            btn:['确定','取消'],
            yes:function (index) {
                isNull(data.field);
                customAjax.http('/wuhuatexing', data.field, function (res) {
                    if (res.code === 0) {
                        layer.msg('添加成功', {
                            icon: 1,
                            time: 1000,
                            end:function () {
                                localStorage.setItem("wuhuate",JSON.stringify(data.field));
                                setTpl.oneTpl(data.field,$("#wuhuate-tpl"),$("#wuhuate-view"));
                            }
                        })
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //稳定性和反应性
    $(".wendingxingAdd").click(function () {
        if (getData('wendingxing')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#wendingxing-add"),'稳定性和反应性');
    });
    form.on('submit(wendingxing)',function (data) {
        layer.confirm("确认添加吗？",{
            btn:['确定','取消'],
            yes:function (index) {
                isNull(data.field);
                customAjax.http('/wendingying', data.field, function (res) {
                    if (res.code === 0) {
                        layer.msg('添加成功', {
                            icon: 1,
                            time: 1000,
                            end:function () {
                                localStorage.setItem("wendingxing",JSON.stringify(data.field));
                                setTpl.oneTpl(data.field,$("#wendingxing-tpl"),$("#wendingxing-view"));
                            }
                        })
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //毒理学资料
    //继续添加按钮
    $("#dulixue-content").delegate('.duli-addItem','click',function () {
        $("#duli-tbody").append(createElementDu());
    });
    function createElementDu(){
        var item = $(" <tr>\n" +
            "                                        <td><input type=\"text\" name=\"jzufen[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "                                        <td><input type=\"text\" name=\"jcas[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "                                        <td><input type=\"text\" name=\"jinkou[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "                                        <td><input type=\"text\" name=\"jinpi[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "                                        <td><input type=\"text\" name=\"lc[]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "                                        <td><button type=\"button\" class=\"layui-btn  layui-btn-xs layui-btn-danger jixindu-del\" title=\"删除当前一条\">删除</button></td>\n" +
            "                                    </tr>");
        return item;
    }
    //删除
    $("#dulixue-content").delegate(".jixindu-del",'click',function () {
        $(this).parents("tr").remove();
    });
    $(".dulixueAdd").click(function () {
        if (!localStorage.getItem('chengfenzu') ){
            layer.msg("请先录入成分！",{
                icon: 6,
                time: 1000
            });
            return;
        }
        if (getData('dulixue')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        //请求存在的法规和CAS
        customAjax.http("/returnZhufenAndCas",{},function (msg) {
            if (msg.code === 0){
                setTpl.oneTpl(msg.data,$("#dulixueAdd-tpl"),$("#dulixue-add-view"));
                openView($("#dulixue-add-view"),'毒理学资料');
            }else {
                layer.msg("没有相关组分或者CAS No.！！")
            }
        });
    });
    form.on('submit(dulixue)',function (data) {
        layer.confirm("确认添加吗？",{
            btn:['确定','取消'],
            yes:function (index) {
                var dlx = {
                    "other":{}
                };
                var dlxTpl = {
                    "other":{}
                };
                //处理急性毒性
                var jixingdu = {
                    "jzufen":[],
                    "jcas":[],
                    "jinkou":[],
                    "jinpi":[],
                    "lc":[]
                };
                for (key in data.field){
                    if (key.lastIndexOf("jzufen") !== -1){
                        jixingdu.jzufen.push(data.field[key]);
                    }
                    if (key.lastIndexOf("jcas") !== -1) {
                        jixingdu.jcas.push(data.field[key]);
                    }
                    if (key.lastIndexOf("jinkou") !== -1){
                        jixingdu.jinkou.push(data.field[key]);
                    }
                    if (key.lastIndexOf("jinpi") !== -1){
                        jixingdu.jinpi.push(data.field[key]);
                    }
                    if (key.lastIndexOf("lc") !== -1){
                        jixingdu.lc.push(data.field[key]);
                    }
                }

                //处理致癌性
                var zhiaixing = {
                    "iarc":[],
                    "ntp":[]
                };
                var zhiaixingTpl = {
                    "zcas":[],
                    "zzufen":[],
                    "iarc":[],
                    "ntp":[]
                };
                for (key in data.field){
                    if (key.lastIndexOf("zcas") !== -1){
                        zhiaixingTpl.zcas.push(data.field[key]);
                    }
                    if (key.lastIndexOf("zzufen") !== -1) {
                        zhiaixingTpl.zzufen.push(data.field[key]);
                    }
                    if (key.lastIndexOf("iarc") !== -1){
                        if (data.field[key] === ""){
                            data.field[key] = "未列入"
                        }
                        zhiaixing.iarc.push(data.field[key]);
                        zhiaixingTpl.iarc.push(data.field[key]);
                    }
                    if (key.lastIndexOf("ntp") !== -1){
                        if (data.field[key] === ""){
                            data.field[key] = "未列入"
                        }
                        zhiaixing.ntp.push(data.field[key]);
                        zhiaixingTpl.ntp.push(data.field[key]);
                    }
                }
                for (var key in data.field){
                    if (key.lastIndexOf('[') === -1){
                        if (data.field[key] === "") {
                            data.field[key] = "无资料"
                        }
                        dlx.other[key] = data.field[key];
                        dlxTpl.other[key] = data.field[key];
                    }
                }
                dlx.zhiaixing = zhiaixing;
                dlx.jixingdu = jixingdu;
                dlxTpl.jixingdu = jixingdu;
                dlxTpl.zhiaixing = zhiaixingTpl;
                $.ajax({
                    url:'/dulixue',
                    type:"post",
                    dataType:"json",
                    contentType: "application/json; charset=utf-8",
                    data:JSON.stringify(dlx),
                    success:function (msg) {
                        if (msg.code === 0){
                            layer.msg("添加成功",{
                                icon: 1,
                                time: 1000,
                                end:function () {
                                    localStorage.setItem("dulixue",JSON.stringify(dlx));
                                    setTpl.oneTpl(dlxTpl,$("#dulixue-tpl"),$("#dulixue-view"));
                                }
                            })
                        }
                    },
                    error:function (err) {
                        layer.msg("服务器繁忙,请稍后重试");
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //生态学资料
    $(".shengtaixueAdd").click(function () {
        if (getData('shengtaixue')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#shengtaixue-add"),'生态学资料');
    });
    form.on('submit(shengtaixue)',function (data) {

        layer.confirm("确认添加吗？",{
            btn:['确定','取消'],
            yes:function (index) {
                isNull(data.field);
                customAjax.http('/shengtaixue', data.field, function (res) {
                    if (res.code === 0) {
                        layer.msg('添加成功', {
                            icon: 1,
                            time: 1000,
                            end:function () {
                                localStorage.setItem("shengtaixue",JSON.stringify(data.field));
                                setTpl.oneTpl(data.field,$("#shengtaixue-tpl"),$("#shengtaixue-view"));
                            }
                        })
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //废弃处置
    $(".feiqichuAdd").click(function () {
        if (getData('feiqichu')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        openView($("#feiqichu-add"),'废弃处置');
    });
    form.on('submit(feiqichu)',function (data) {
        layer.confirm("确认添加吗？",{
            btn:['确定','取消'],
            yes:function (index) {
                isNull(data.field);
                customAjax.http('/feiqichuzhi', data.field, function (res) {
                    if (res.code === 0) {
                        layer.msg('添加成功', {
                            icon: 1,
                            time: 1000,
                            end:function () {
                                localStorage.setItem("feiqichu",JSON.stringify(data.field));
                                setTpl.oneTpl(data.field,$("#feiqichu-tpl"),$("#feiqichu-view"));
                            }
                        })
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });




    //运输信息
    $(".yunshuxinAdd").click(function () {
        if (getData('yunshuxin')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        var ysbqD = {
            "imgs":[]
        };
        var ysbq = JSON.parse(localStorage.getItem("yunshubiaoqian"));
        for (key in ysbq){
            if (ysbq[key].trim().lastIndexOf('\\') !== -1){
                var data = ysbq[key].trim().split('\\');
                for (var i=0;i<data.length;i++){
                    if (ysbqD.imgs.length >0){
                        var temp = [];
                        for (var j=0;j<ysbqD.imgs.length;j++){
                            temp[ysbqD.imgs[j]] = true;
                        }
                        if (!temp[data[i]]){
                            ysbqD.imgs.push(data[i]);
                        }
                    } else {
                        ysbqD.imgs.push(data[i]);
                    }
                }
            }else if (ysbq[key].trim().lastIndexOf('\\') === -1 && ysbq[key].trim().lastIndexOf('/img') !== -1) {
                if (ysbqD.imgs.length >0){
                    var temp = [];
                    for (var j=0;j<ysbqD.imgs.length;j++){
                        temp[ysbqD.imgs[j]] = true;
                    }
                    if (!temp[ysbq[key].trim()]){
                        ysbqD.imgs.push(ysbq[key].trim());
                    }
                } else {
                    ysbqD.imgs.push(ysbq[key].trim());
                }
            }
        }
        setTpl.oneTpl(ysbqD,$("#yunshuxin-add-tpl"),$("#yunshuxin-add-view"));
        openView($("#yunshuxin-add-view"),'运输信息');
        localStorage.setItem("ysbqD",JSON.stringify(ysbqD));
    });
    form.on('submit(yunshuxin)',function (data) {
        layer.confirm("确认添加吗？",{
            btn:['确定','取消'],
            yes:function (index) {
                isNull(data.field);
                customAjax.http('/yunshuxingxi', data.field, function (res) {
                    if (res.code === 0) {
                        layer.msg('添加成功', {
                            icon: 1,
                            time: 1000,
                            end:function () {
                                data.field.ysbqD = JSON.parse(localStorage.getItem('ysbqD'));
                                setTpl.oneTpl(data.field,$("#yunshuxin-tpl"),$("#yunshuxin-view"));
                                localStorage.setItem("yunshuxin",JSON.stringify(data.field));
                            }
                        })
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //法规信息
    $(".faguixinAdd").click(function () {
        if (!localStorage.getItem('chengfenzu') ){
            layer.msg("请先录入成分！",{
                icon: 6,
                time: 1000
            });
            return;
        }
        if (getData('faguixin')){
            layer.msg("信息已经存在，请勿重复添加",{
                icon: 6,
                time: 1000
            });
            return;
        }
        //请求存在的法规
        customAjax.http("/returnRuleAndZhufen",{name:localStorage.getItem('name')},function (msg) {
            if (msg.code === 0){
                setTpl.oneTpl(msg.data[0],$("#faguixin-add"),$("#faguixin-add-view"));
                localStorage.setItem('title',JSON.stringify(msg.data[0]));
                openView($("#faguixin-add-view"),'法规信息');
            }else {
                layer.msg("没有相关组分！！")
            }
        })
    });
    form.on("submit(faguixin)",function (data) {
        var num = $("#faguixin-tbody").children().length;
        layer.confirm("确认添加吗？",{
            btn:['确定','取消'],
            yes:function (index) {
                for (key in data.field){
                    if (data.field[key] !== "是" && data.field[key] !== "否") {
                        layer.msg('含有除是否外其他字符，请检查后提交！',{
                            icon:5
                        });
                        return;
                    }
                }
                var faguixin = {};
                for (var i=0;i<num;i++){
                    faguixin['zufen'+i] = [];
                    faguixin['zufenC'+i] = [];
                }
                for (k1 in data.field){
                    var FkIndex = k1.indexOf('[');
                    var Fk = k1.substring(0,FkIndex);
                    for (k2 in faguixin){
                        if (Fk === k2){
                            faguixin[k2].push(data.field[k1])
                        }
                    }
                }
                var title = JSON.parse(localStorage.getItem('title'));
                faguixin.n = [];
                faguixin.n.push(title.zufen.length); //长度
                $.ajax({
                    url:'/Submitxuanzhe',
                    type:"post",
                    dataType:"json",
                    contentType: "application/json; charset=utf-8",
                    data:JSON.stringify(faguixin),
                    success:function (msg) {
                        if (msg.code === 0){
                            layer.msg("成功",{
                                icon: 1,
                                time: 1000,
                                end:function () {
                                    faguixin.title = title;
                                    console.log(faguixin);
                                    localStorage.setItem("faguixin",JSON.stringify(faguixin));
                                    setTpl.oneTpl(faguixin,$("#faguixin-tpl"),$("#faguixin-view"));
                                }
                            })
                        }
                    },
                    error:function (err) {
                        layer.msg("服务器异常,请稍后重试");
                    }
                });
                layer.close(index);
                layer.closeAll('page');
            }
        });
        return false;
    });


    //弹出层
    function openView(view,title){
        layer.open({
            type: 1,
            content: view,
            area: ['50%',"90%"],
            shift: 1,
            maxmin: true,
            title: [title, 'font-size:22px;'],
            resize: false
        });
    }
    //获取数据
    function getData(key){
        var data = JSON.parse(localStorage.getItem(key));
        if (data){
            return 1;
        }
    }
    //判断字符串
    function initObj(data){
        var arrs = [];
        if (data.lastIndexOf("。") !== -1){
            var info = data.split("。");
            info.forEach(function (item,index) {
                if (item){
                    arrs.push(item);
                }
            });
            return arrs;
        }else {
            return 0;
        }
    }
    //判断提交是否有空
    function isNull(data){
        for (key in data){
            if (data[key] === ""){
                data[key] = "无资料"
            }
        }
    }

    exports('insertDasic',{});
});