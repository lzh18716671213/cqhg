layui.define(['layer', 'form','jquery','element','customAjax','customTable','bar'],function (exports) {
    var  form = layui.form,
        layer = layui.layer,
        $ = layui.$,
        element = layui.element,
        customAjax = layui.customAjax,
        customTable = layui.customTable,
        bar = layui.bar;
    bar();

    //初始化页面
    var mainObj = {
        node:"#RulesF-demo-China",
        url:"/findRules",
        limit:20,
        col:[//表头
            { field: 'usname', title: '法规英文名称' }
            , { field: 'cnname', title: '法规中文名称' }
            ,{ field:'region',title:'所属地区',templet:function (d) {
                    return d.region === "china" ? "中国化学品管理名录" : "国际化学品名录"
                }}
            ,{ title: '操作', toolbar: '#barDemo' }
        ],
        //弹出层参数
        content:$("#editRulesF"),
        title:'修改法规信息',
        width:"760px",
        page:"rules",
        filter:"editRulesF"
    };
    var tableIns = customTable(mainObj);



    //修改
    form.on('submit(rulesEdit)',function (data) {
        customAjax.http('/updateRules',data.field,function (res) {
            if (res.code === 0){
                layer.msg('修改成功!',{
                    icon:6,
                    time:1000,
                    end:function () {
                        layer.closeAll('page');
                        tableIns.reload();
                    }
                })
            }else {
                layer.msg("修改失败，请稍后重试！",{
                    icon:5,
                    time:1000
                })
            }

        });
        return false;
    });

    //添加
    $(".my-btn").click(function () {
        layer.open({
            type: 1,
            content: $("#addRulesF"),
            area: ['50%'],
            shift: 1,
            maxmin: true,
            title: ['添加法律法规', 'font-size:22px;'],
            resize: false,
            cancel:function (index) {
                $("#testInfo")[0].reset();
            }
        });

        form.on('submit(rules)',function (data) {
            customAjax.http('/insertRules',data.field,function (res) {
                if (res.code === 0){
                    layer.msg('添加成功!',{
                        icon:6,
                        time:1000,
                        end:function () {
                            layer.closeAll('page');
                            tableIns.reload();
                        }
                    })
                }else {
                    layer.msg("添加失败，请稍后重试！",{
                        icon:5,
                        time:1000
                    })
                }
            });
            return false;
        });
    });

    exports('rulesF',{})
});