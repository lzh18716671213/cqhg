layui.define(['layer','jquery','form','bar','element','customAjax','customTable','customWindows'],function (exports) {
    var  form = layui.form,
        layer = layui.layer,
        $ = layui.$,
        element = layui.element,
        bar = layui.bar,
        customAjax = layui.customAjax,
        customTable = layui.customTable,
        customWindows = layui.customWindows;
        bar();

    //初始化页面
    var mainObj = {
        node:'#userM-demo',
        url:'/findAllUser',
        limit:20,
        col:[//表头
            { field: 'username', title: '用户名' }
            , { field: 'createtime', title: '编辑时间' }
            , { field: 'type', title: '用户类型',templet:"#typeTpl"}
            , { field: 'status', title: '状态',templet:"#statusTpl"}
            , { title: '操作', toolbar: '#barDemo' }
        ],
        //弹出层参数
        content:$("#updatePass"),
        title:'修改密码',
        height:"320px",
        width:"720px",
        page:'user',
        filter:'updatePass'
    };
   var tableInsHome = customTable(mainObj);
    status(tableInsHome);
    usertype(tableInsHome);
    updatePass(tableInsHome);

    //搜索
    $(".search-btn").click(function () {
        if ($(".search-input").val().length) {
            var searchObj = {
                node:'#search-demo',
                url:'/searchUser?tick='+$(".search-input").val(),
                limit:20,
                col:[
                    { field: 'username', title: '用户名' }
                    , { field: 'createtime', title: '编辑时间' }
                    , { field: 'type', title: '用户类型',templet:"#typeTpl"}
                    , { field: 'status', title: '状态',templet:"#statusTpl"}
                    , { title: '操作', toolbar: '#barDemo' }
                ],
                //弹出层参数
                content:$("#updatePass"),
                title:'修改密码',
                height:"320px",
                width:"720px",
                page:'user',
                filter:'updatePass'
            };
            //渲染数据
            var tableInsSearch = customTable(searchObj);
            status(tableInsSearch);
            usertype(tableInsSearch);
            updatePass(tableInsSearch);
            //使用弹窗显示搜索到的内容
            layer.open({
                type: 1,
                content:$("#search-info"),
                area: ['80%', '80%'],
                shift: 1,
                maxmin: true,
                title: ['搜索详情', 'font-size:22px;'],
                cancel:function () { //点击关闭时，切回home页的实例
                    status(tableInsHome);
                    usertype(tableInsHome);
                    updatePass(tableInsHome);
                }
            });
        } else {
            layer.msg('请输入搜索的内容')
        }
    });

    //添加用户
    $(".my-btn").click(function () {
        layer.open({
            type: 1,
            content: $("#addUser"),
            area: ['50%'],
            shift: 1,
            maxmin: true,
            title: ['添加用户', 'font-size:22px;'],
            resize: false,
            cancel:function (index) {
                $("#User")[0].reset();
            }
        });
    });

    /**
     * 注册
     */
    form.on('submit(addUser)',function (data) {
        if (data.field.password === data.field.password1){
            data.field['time'] = newTime();
            customAjax.http('/registerUser',data.field,function (res) {
                if (res.code === 0){
                    layer.msg("添加成功",{
                        icon:6,
                        time:1000,
                        end:function () {
                            layer.closeAll('page');
                            tableInsHome.reload();
                        }
                    })
                }else {
                    layer.msg("您的用户名已被占用！",{
                        icon:5,
                        time:1000
                    })
                }
            });
        }else {
            layer.msg("两次密码不同请重新确认",{
                icon:5,
                time:1000
            })
        }
        return false;
    });

    form.verify({
        username:[
            /^[A-Za-z]{1}[A-Za-z0-9]{3,15}$/
            ,'必须是以字母开头，必须包含字母数字，4到16位'
        ],
        password: [
            /^[\S]{6,12}$/
            ,'密码必须6到12位，且不能出现空格'
        ]
    });

    //时间：
    function newTime(){
        var D = new Date();
        var Y = D.getFullYear();
        var M = D.getMonth()+1;
        var day = D.getDate();
        return Y+"-"+M+"-"+day;
    }

    /**
     * 修改密码方法
     * @param tableIns
     */
    function updatePass(tableIns){
        form.on('submit(updatePass)',function (data) {
            if (data.field.passwordNewA === data.field.passwordNewB){
                var datas = {};
                datas["id"] = data.field.id;
                datas["passwordNew"] = data.field.passwordNewA;
                datas["passwordOld"] = data.field.passwordOld;
                console.log(datas);
                customAjax.http('/rePassword',datas,function (res) {
                    if (res.code === 0){
                        layer.msg("修改成功",{
                            icon:6,
                            time:1000,
                            end:function () {
                                layer.closeAll('page');
                                tableIns.reload();
                            }
                        })
                    }else {
                        layer.msg("修改失败，请重新确认密码！",{
                            icon:5,
                            time:1000
                        })
                    }
                });
            }else {
                layer.msg("两次密码不同请重新确认",{
                    icon:5,
                    time:1000
                })
            }
            return false;
        });
    }


    /**
     *处理用户状态的修改
     * @param {Object}tableIns
     */
    function status(tableIns){
        form.on('switch(status)', function (data) {
            var id = $(data.elem).attr("switch_id");
            if (data.elem.checked) {
                customAjax.httpDel('/updateStatus?status='+1+"&id="+id,function (res) {
                    if (res.code === 0){
                        layer.msg("开启成功！", {
                            icon: 6,
                            time: 100,
                            end: function () {
                               tableIns.reload();
                            }
                        })
                    }else {
                        layer.msg("开启失败，请稍后重试！", {
                            icon: 5,
                            time: 100,
                            end: function () {
                                tableIns.reload();
                            }
                        });
                    }
                },tableIns)
            }else {
                customAjax.httpDel('/updateStatus?status=' + 0+"&id="+id, function (res) {
                    if (res.code === 0) {
                        layer.msg("停用成功！", {
                            icon: 6,
                            time: 100,
                            end: function () {
                                tableIns.reload();
                            }
                        })
                    }else {
                        layer.msg("停用失败，请稍后重试！", {
                            icon: 5,
                            time: 100,
                            end: function () {
                                tableIns.reload();
                            }
                        });
                    }
                },tableIns)
            }
        });
    }


    /**
     * 处理用户类型
     * @param {Object}tableIns
     */
    function usertype(tableIns){
        form.on('switch(type)',function (data) {
            var id = $(data.elem).attr("switch_id");
            if (data.elem.checked) {
                customAjax.httpDel('/updateType?type=admin&id='+id,function (res) {
                    if (res.code === 0){
                        layer.msg("开启成功！", {
                            icon: 6,
                            time: 100,
                            end: function () {
                                tableIns.reload();
                            }
                        })
                    }else {
                        layer.msg("开启失败，请稍后重试！", {
                            icon: 5,
                            time: 100,
                            end: function () {
                                tableIns.reload();
                            }
                        })
                    }
                },tableIns)
            }else {
                customAjax.httpDel('/updateType?type=user&id='+id, function (res) {
                    if (res.code === 0) {
                        layer.msg("停用成功！", {
                            icon: 6,
                            time: 100,
                            end: function () {
                                tableIns.reload();
                            }
                        })
                    }else {
                        layer.msg("停用失败，请稍后重试！", {
                            icon: 5,
                            time: 100,
                            end: function () {
                                tableIns.reload();
                            }
                        })
                    }
                },tableIns)
            }
        });
    }


    exports('userM',{});
});

