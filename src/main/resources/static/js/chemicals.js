layui.define(['layer','jquery', 'element','customTable','customWindows','bar'], function (exports) {
    var layer = layui.layer,
        $ = layui.$,
        element = layui.element,
        customTable = layui.customTable,
        customWindows = layui.customWindows,
        bar = layui.bar;
    bar();

    var mainObj = {
        node:"#chemicals-demo",
        url:"/findAllChemicals",
        limit:20,
        col:[//表头
            { field: 'name', title: '产品中文名称' }
            , { field: 'usname', title: '产品英文名称' }
            , { title: '操作', toolbar: '#barDemo' }
        ],
        href:"/dasisEdit"
    };
    customTable(mainObj);

    $(".search-btn").click(function () {
        if ($(".search-input").val().length) {
            var obj = {
                node:"#search-demo",
                url:"/findChemical?tick="+$(".search-input").val(),
                limit:10,
                col:[//表头
                    { field: 'name', title: '产品中文名称' }
                    , { field: 'usname', title: '产品英文名称' }
                    , { title: '操作', toolbar: '#barDemo' }
                ],
                href:"/dasisEdit"
            };
            //渲染数据
            customTable(obj);
            //弹窗
            customWindows();
        } else {
            layer.msg('请输入搜索的内容')
        }
    });
    exports('chemicals', {});
});