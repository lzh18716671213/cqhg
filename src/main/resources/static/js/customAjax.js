layui.define(['jquery','layer'],function (exports) {
   var $ = layui.$,
       layer = layui.layer;
   var utils = {
       /**
        * 需要向后台发送数据的操作
        * @param {string}url
        * @param {object/string}data
        * @param {function}callback
        */
       http:function (url,data,callback) {
           $.ajax({
               url:url,
               type:"post",
               dataType:"json",
               data:data,
               success:function (msg) {
                   callback(msg);
               },
               error:function (err) {
                   layer.msg("服务器繁忙,请稍后重试！",{
                       icon:5,
                       time:100
                   });
               }
           })
       },
       /**
        * 删除指令
        * @param {String|接口}url
        * @param {function/回掉函数}callback
        * @param {Object}tableIns
        */
       httpDel:function (url,callback,tableIns) {
           $.ajax({
               url:url,
               type:"get",
               success:function (msg) {
                   callback(msg);
               },
               error:function () {
                   layer.msg("服务器繁忙,请稍后重试！",{
                       icon:5,
                       time:100,
                       end:function () {
                           tableIns.reload();
                       }
                   });
               }
           })
       },
       /**
        * 查询数据指令
        * @param url
        * @param callback
        */
       httpSelect:function (url,callback) {
           $.ajax({
               url:url,
               type:"get",
               dataType:"json",
               success:function (msg) {
                       callback(msg);
               },
               error:function () {
                   layer.msg("服务器繁忙,请稍后重试！",{
                       icon:5,
                       time:100
                   });
               }
           })
       }
   };

    exports("customAjax",utils);
});